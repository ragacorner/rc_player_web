<?php

// use Illuminate\Support\Facades\URL;

//GUEST PAGE
Auth::routes(['verify' => true]);
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');
Route::get('get/regencies/{id}', 'HomeController@getPlaces')->name('getPlaces');
// Route::get('get/subdistricts/{id}', 'LandingController@getPlaces')->name('getSub');

// BOOKING AT PLACE FOR GUEST
Route::post('booking/ots/', 'GuestController@bookingonplace')->name('booking.onplace');
Route::get('booking/ots/payment/{code}', 'GuestController@payment')->name('booking.onplace.payment');

// PLAYER PAGE - Booking
Route::get('home', 'HomeController@home')->name('home');
Route::group(['middleware'=>['auth','verified']], function()
{
    // SETUP Page -  ONLY FOR NEW COMERS
    Route::get('setup', 'SetupProfileController@create')->name('setup.profile.create');
    Route::post('setup', 'SetupProfileController@store')->name('setup.profile.store');

    // Route::get('boards', 'BoardController@index')->name('boards.index');

    Route::group(['prefix'=>'{player}'], function()
    {
        // Route::get('', 'ActivityController@index')->name('player.activities.index');

        Route::get('my-games', 'GamesController@index')->name('player.games.index');

        Route::get('profile', 'ProfileController@show')->name('player.profile.show');
        Route::get('profile/edit', 'ProfileController@edit')->name('player.profile.edit');
        Route::put('profile/update', 'ProfileController@update')->name('player.profile.update');

        // Route::get('booklogs', 'BooklogController@index')->name('player.booklogs.index');
        Route::get('booklogs/invoice/{place}', 'BooklogController@invoice')->name('player.booklogs.invoice');
    });

    // BOOKING PAGE
    Route::get('booking/{place_slug}/create', 'BookingController@create')->name('booking.create');
    Route::post('booking/{place_slug}/store', 'BookingController@store')->name('booking.store');
    Route::get('booking/{booking_code}/change-time', 'BookingController@change_time')->name('booking.change_time');
    Route::post('booking/{booking_code}/update-time', 'BookingController@update_time')->name('booking.update_time');
    Route::delete('booking/{booking_id}/cancel/', 'BookingController@cancel')->name('booking.cancel');
    
});

// SEARCHING PAGE
Route::get('/locale', 'LandingController@setreg')->name('searching.setreg');
Route::post('/getlocale', 'LandingController@getreg')->name('searching.getreg');
Route::get('/', 'LandingController@landing')->name('searching.landing');
Route::get('search/reg', 'SearchingController@regency')->name('searching.regency');
Route::get('search/plc/{place_slug}', 'SearchingController@place')->name('searching.place');

// PLACE PAGE
Route::get('places={place_slug}', 'PlaceController@show')->name('place.show');

// LAWS PAGE
Route::get('syarat-ketentuan', 'LandingController@termsOfUse')->name('syarat.ketentuan');
Route::get('kebijakan-privasi', 'LandingController@privacy')->name('kebijakan.privasi');

// TEMPORARY PAGE
// Route::get('/', 'VisitorController@index');
// Route::get('/coming-soon', 'VisitorController@comingsoon');
// Route::post('/coming-soon/message', 'VisitorController@store')->name('visitor.message.store');
// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

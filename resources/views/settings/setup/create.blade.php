@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('css/separate/pages/others.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/separate/vendor/bootstrap-daterangepicker.min.css') }}">
@endsection

@section('content')

	<div class="">
		<div class="col-md-6">
			
		</div>
		<div class="col-md-5">
            <div class="row">
                <div class="card">
                    <div class="card-block">
                        <h5 class="steps-numeric-title with-border">Lengkapi Profile Kamu</h5> 
                        
                        <!-- <div class="add-customers-screen-user">
                            <i class="font-icon font-icon-user"></i>
                        </div> -->
        
                        {!! Form::open(['url' => route('setup.profile.store'), 'method' => 'post', 'files'=>'true', 'id'=>'']) !!}
                            {{ csrf_field() }}
                            
                            <div class="card-block">
                                @include('settings.setup._form')
                            </div>
                            
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
       
			<!-- <div class="box-typical-full-height">
                <div class="card">
					<div class="add-customers-screen-in">
					</div>
				</div>
			</div> -->
		</div>
	</div>
@endsection

@section('js')
    <script src="{{ asset('js/get-places.js') }}"></script>
	<script src="{{ asset('js/lib/input-mask/jquery.mask.min.js') }}"></script>
	
	<script type="text/javascript" src="{{ asset('js/lib/moment/moment-with-locales.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/lib/eonasdan-bootstrap-datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
	<script src="{{ asset('js/lib/daterangepicker/daterangepicker.js') }}"></script>
	
    <script type="text/javascript">
		$('#daterange3').daterangepicker({
			singleDatePicker: true,
			showDropdowns: true
		});
        $(document).ready(function() {
            $('#phone-with-code-area-mask-input').mask('000000000000', {placeholder: "____________"});
        });
	</script>

@endsection

   
<div class="row">

    <!-- <input type="hidden" name="status" value="inactivated"> -->
    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
    <input type="hidden" name="status" value="">

    <div class="col-md-12">
        <!-- NAMA -->
        <div class="form-group row">
            <label class="col-lg-4 form-label"><small class="">Nama Lengkap </small></label>
            <div class="col-lg-7 pull-left"><b>{{ Auth::user()->name }}</b></div>
        </div> 

        <!-- PHOTO -->
        <div class="form-group row">
            <label class="col-lg-4 form-label"><small class="">Foto</small></label>
            <div class="col-lg-7">
                <!-- <img src="" alt="" class=""> -->
                <div class="form-control-wrapper form-control-icon-left">
                    {!! Form::file('photo', null, ['class'=>'form-control']) !!}
                </div>
                @if ($errors->has('photo'))
                    <small class="text-danger">{{ $errors->first('photo') }}</small>
                @endif
            </div>
        </div>

        <!-- NAMA -->
        <!-- <div class="form-group row">
            <label class="col-lg-4 form-label"><small class="">Nama Lengkap <span class="color-red">*</span></small></label>
            <div class="col-lg-7">
                {!! Form::text('name', Auth::user()->name, ['class'=>'form-control','placeholder'=>'','required']) !!}
                
                @if ($errors->has('name'))
                    <small class="text-danger">{{ $errors->first('name') }}</small>
                @endif
            </div>
            <div class="col-lg-1">
                
            </div>
        </div> --> 

        <!-- TANGGAL LAHIR -->
        <div class="form-group row">
            <label class="col-lg-4 form-label"><small class="">Tanggal Lahir <span class="color-red">*</span></small></label>
            <div class="col-lg-7">
                <input id="daterange3" type="text" name="birthday" value="{{Carbon\Carbon::now()->subYears(10)->format('m/d/Y')}}" class="form-control">
                
                @if ($errors->has('birthday'))
                    <small class="text-danger">{{ $errors->first('birthday') }}</small>
                @endif
            </div>
            <div class="col-lg-1"></div>
        </div>      

        <!-- GENDER -->
        <div class="form-group row">
            <label class="col-lg-4 form-label"><small class="">Jenis Kelamin <span class="color-red">*</span></small></label>
            <div class="col-lg-7">
                <select name="gender" class="select2" required>
                    <option value="male" selected>Pria</option>
                    <option value="female">Wanita</option>
                </select>
                @if ($errors->has('gender'))
                    <small class="text-danger">{{ $errors->first('gender') }}</small>
                @endif
            </div>
            <div class="col-lg-1">
                
            </div>
        </div>

        <!-- PEKERJAAN -->
        <!-- <div class="form-group row">
            <label class="col-lg-4 form-label"><small class="">Pekerjaan <span class="color-red">*</span></small></label>
            <div class="col-lg-7">
                <select name="occupation" class="select2" required>
                    <option value="smp" selected>Pelajar SMP</option>
                    <option value="sma">Pelajar SMA</option>
                    <option value="mahasiswa">Mahasiswa</option>
                    <option value="karyawan">Karyawan</option>
                    <option value="wiraswasta">Wiraswasta</option>
                </select>
                @if ($errors->has('occupation'))
                    <small class="text-danger">{{ $errors->first('occupation') }}</small>
                @endif
            </div>
            <div class="col-lg-1">
                
            </div>
        </div> -->

        <!-- PEKERJAAN -->
        <div class="form-group row">
            <label class="col-lg-4 form-label"><small class="">Olahraga Favorit <span class="color-red">*</span></small></label>
            <div class="col-lg-7">
                <select name="category" class="select2-photo" required>
                    
                    @foreach($composer_categories as $cat)

                        @php $count_place = $cat->places->count(); @endphp

                        @if($count_place > 0)
                            <option data-photo="{{ asset('img/others/categories/'.$cat->name.'.png') }}" 
                                value="{{ $cat->name }}">{{ $cat->display_name }}
                            </option>
                        @endif
                    @endforeach
                </select>
                @if ($errors->has('category'))
                    <small class="text-danger">Harap diisi dengan benar</small>
                @endif
            </div>
        </div>

        <div class="form-group row">
        <label class="col-sm-4 form-control-label"><small class="">No. HP <span class="color-red">*</span></small></label>
        @if ($errors->has('phone'))
            <div class="col-sm-7">
                {!! Form::text('phone', null, ['class'=>'form-control','id'=>'phone-with-code-area-mask-input', 'placeholder'=>'Masukan nomor handphone', '']) !!}
                <small class="text-danger">{{ $errors->first('phone') }}</small>
            </div>
        @else
            <div class="col-sm-7">
                {!! Form::text('phone', null, ['class'=>'form-control','id'=>'phone-with-code-area-mask-input', 'required']) !!}
                
            </div>
        @endif
    </div>

        <!-- ALAMAT -->
        <div class="form-group row">
            <label class="col-lg-4 form-label">
                <small class="">Domisili Sekarang 
                    <span class="color-red">*</span>
                    <!-- <a href="#"
                        class=""
                        title="Pemberitahuan"
                        data-container="body"
                        data-toggle="popover"
                        data-placement="bottom"
                        data-content="Kabupaten/Kota berdasarkan alamat tempat olahraga yang mendaftar pada kami. Kami ucapkan terima kasih atas bantuannya untuk mengenalkan Ragacorner kepada tempat olahraga di daerahmu :)">
                        <i class="font-icon-question"></i>
                    </a> -->
                </small>
            </label>
            <div class="col-lg-7">
                <select name="regency_id" class="select2" required>
                    <option value="" selected>-- Pilih Kota/Kabupaten --</option>
                    
                    @foreach($composer_provincies as $prov)
                        <optgroup label="{{$prov->display_name}}">
                            
                            @foreach($composer_regencies as $reg) 
                                @if($prov->id == $reg->province_id)
                                    <option value="{{ $reg->id }}">{{ $reg->display_name }}</option> 
                                @endif
                            @endforeach
                            
                        </optgroup>
                    @endforeach
                </select>
                @if ($errors->has('regency_id'))
                    <small class="text-danger">{{ $errors->first('regency_id') }}</small>
                @endif
            </div>
            <div class="col-lg-1"></div>
        </div>
        <div class="form-group row">
            <label class="col-lg-4 form-label"></label>
            <div class="col-lg-7">
                <select name="subdistrict_id" class="select2" required>
                    <option>-- Pilih Kecamatan --</option>
                </select>
                @if ($errors->has('subdistrict_id'))
                    <small class="text-danger">{{ $errors->first('subdistrict_id') }}</small>
                @endif
            </div>
            <div class="col-lg-1">
                
            </div>
        </div>
        <div class="form-group row">
            <label class="col-lg-4 form-label"></label>
            <div class="col-lg-7">
                {!! Form::text('area', null, ['class'=>'form-control','placeholder'=>'Nama Kelurahan', 'required']) !!}
                @if ($errors->has('area'))
                    <small class="text-danger">{{ $errors->first('area') }}</small>
                @else
                    <!-- <small class="text-muted">Nama Daerah</small> -->
                @endif
            </div>
            <div class="col-lg-1">
                
            </div>
        </div>
        
        <div class="form-group row">
            <div class="col-md-4"></div>
            <div class="col-md-7">
                <button type="submit" name="button" class="btn btn-success btn-block">
                    <span class="font-icon glyphicon glyphicon-ok"></span> 
                    Selesai
                </button>
            </div>
        </div>
    </div>
</div>

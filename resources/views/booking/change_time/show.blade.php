@extends('layouts.app')

@section('css')
@endsection

@section('content')

    @include('booking.change_time.triger')
    
    <!-- <div class="p-t-md"></div> -->
    <div class="row">
            
        <div class="col-md-3">
			@include('places.placeinfo')
        </div>
        <div class="col-md-6">
            @include('booking.change_time.alert')

            <div class="">
                @if($hari_diminta <= $batas_hari && $hari_diminta >= $composer_today)
                    @if(($request_jam <= $composer_now_hour) && ($hari_diminta->format('d-m-Y') == $composer_now_date))
                        <div class="card">
                            <div class="card-block">
                                <div class="text-center">
                                    <h3 class="with-border">Maaf, sekarang jam {{ $composer_now_hour_minute }} </h3>
                                    <h5>Kamu hanya diperbolehkan merubah jadwal booking pada jam berikutnya</h5>
                                </div>
                            </div>
                        </div>
                    @else
                        @if($jam_diminta->format('i') != '00' )
                            <div class="text-center">
                                <h1 class="with-border">Pemberitahuan!</h1>
                                <h3>Harap gunakan waktu pas (contoh: 09:00, 10:00, 11:00)</h3>
                            </div>
                        @else
                            
                            <div id="">
                                @forelse($fields as $field)
                                    @if($jam_diminta < $field->place->open)
                                        <div class="text-center">
                                            <h1 class="with-border">Maaf :)</h1>
                                            <h3>Keliatannya masih belum ada yang buka pada waktu itu</h3>
                                        </div>
                                    @else
                                        @include('booking.change_time.list')
                                    @endif
                                @empty
                                    <div class="card-block text-center">
                                        <h1 class="with-border">Oops!</h1>
                                        <h3>Keliatannya sudah terbooking semua lapangan didaerah kamu</h3>
                                        
                                    </div>
                                @endforelse
                                
                                @foreach($booked_fields as $booked)
                                    @include('searching.inc.place.bookedlist')
                                @endforeach
                            </div>                      
                        @endif                    
                    @endif
                @else
                    <div class="text-center">
                        <h1 class="with-border">Maaf :)</h1>
                        <h3>Batas pemesanan di 
                        <i>Ragacorner</i> 
                        hanya bisa dilakukan mulai 
                        <b>hari ini</b> 
                        sampai 
                        <b>1 bulan</b>
                        ke depan.</h3>
                    </div>
                @endif
            </div>
            
        </div>
		<div class="col-md-3">

            <div class="card">
                <div class="card-block">
                    <h6 class="with-border">Info Booking</h6>
                    @php
                        $tanggal_invoice = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $booking->updated_at)->format('d M Y');
                        $tanggal_main = Carbon\Carbon::createFromFormat('Y-m-d', $booking->playing_date);
                        $mulai = Carbon\Carbon::createFromFormat('H:i:s', $booking->playing_time);
                        $selesai = Carbon\Carbon::createFromFormat('H:i:s', $booking->playing_time)->addHour($booking->duration)->format('H:i');
                    @endphp

                    <article class="panel">
                        
                        <div id="coll{{ $booking->id }}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="{{ $booking->id }}">
                            <div class="panel-collapse-in">
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <header class="title"><b>No Booking:</b></header>
                                        <p class="no-margin">
                                            {{ $booking->code }} 
                                        </p>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="pull-right">
                                            <header class="title"><b>Pembayaran:</b></header>
                                            <p class="">
                                            @if($booking->paid_half == 'false' && $booking->paid_full == 'false')
                                                <span class="label label-warning">Belum Dibayar</span>
                                            @elseif($booking->paid_half == 'true' && $booking->paid_full == 'false')
                                                <span class="label label-primary">DP Terbayar</span>
                                            @else
                                                <span class="label label-success">Lunas</span>
                                            @endif
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <header class="title"><b>Bermain {{ $booking->category->display_name }} Pada:</b></header>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>
                                            Lapang {{ $booking->field->name }}<br>
                                            {{ $tanggal_main->format('l, d M Y') }}<br>{{ $mulai->format('H:i') }} - {{ $selesai }}
                                        </p>
                                    </div>
                                    <div class="col-md-12">
                                        @if($booking->paid_half == "false")
                                            <div class="form-group text-center">
                                                <a  href="{{ route('player.booklogs.index', Auth::user()->profile->username) }}"
                                                    class="btn btn-primary-outline btn-rounded">
                                                    Kembali ke Booklogs
                                                </a>
                                            </div>
                                        </div>
                                        @endif
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
		</div>
    </div>

@endsection

@section('js')

@endsection
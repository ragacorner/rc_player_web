<div class="alert alert-twitter alert-close alert-dismissible fade in" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>

    <strong>Kami tampilkan lapang kosong untuk bermain pada</strong><br/>
    Tanggal <b>{{ $hari_diminta->format('d-m-Y') }}</b>, 
    pukul <b>{{ $jam_diminta->format('H:i') }}</b>,
    bermain selama <b>{{ $request_duration }}</b> Jam

    <i class="font-icon font-icon-lamp"></i>

    <!-- <a data-toggle="collapse"
        data-parent="#accordion"
        href="#change_triger"
        aria-expanded="true"
        aria-controls="collapseOne"
        class="btn btn-sm btn-warning">
        Cari Waktu Lain
    </a> -->
</div>
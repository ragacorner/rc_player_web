
<div class="row">
    <header class="page-content-header">   
        <section class="widget-triger widget-accordion no-border" id="accordion" role="tablist" aria-multiselectable="true">
            <div id="change_triger" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="card-block">
                    <h5 class="with-border text-center">
                        Ubah Jadwal Booking #{{ $booking->code }}
                    </h5><div class="m-t-lg m-b-lg"></div>
                    <form method="get" action="{{ route('booking.change_time', $booking->code) }}" id="form-comment" class="">
                        @csrf
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class="form-control-wrapper">
                                        <div class="input-group input-append date" id="datePicker">
                                            <input type="text" class="form-control" name="hari" value="{{ $hari_diminta->format('d-m-Y') }}" required/>
                                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                        @if ($errors->has('hari'))
                                            <div class="form-tooltip-error">{{ $errors->first('hari') }}</div>
                                        @endif
                                    </div>
                                    <small class="text-muted">Pilih Tanggal Bermain</small>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <fieldset class="form-group {{ $errors->has('jam') ? 'form-group-error' : '' }}">
                                    <div class="form-control-wrapper">
                                        <select class="select2" name="jam">
                                            @for($jam = 9; $jam <= 23; $jam++)
                                                @if($jam == $jam_diminta->format('H') )
                                                    @if($jam < 10)
                                                        <option value="{{$jam}}" selected>{{ '0'.$jam.':00' }}</option>
                                                    @else
                                                        <option value="{{$jam}}" selected>{{ $jam.':00' }}</option>
                                                    @endif
                                                @else
                                                    @if($jam < 10)
                                                        <option value="{{$jam}}">{{ '0'.$jam.':00' }}</option>
                                                    @else
                                                        <option value="{{$jam}}">{{ $jam.':00' }}</option>
                                                    @endif
                                                @endif
                                            @endfor
                                        </select>
                                        @if ($errors->has('jam'))
                                            <div class="form-tooltip-error">{{ $errors->first('jam') }}</div>
                                        @endif
                                    </div>
                                    <small class="text-muted">Pilih Jam Bermain</small>
                                </fieldset>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class="form-control-wrapper">
                                        <input id="duration" class="form-control" type="text" value="{{ $request_duration }}" placeholder="Durasi" name="duration" required>

                                        @if ($errors->has('duration'))
                                            <div class="form-tooltip-error">Durasi diisi dengan angka, minimal 1 jam, maksimal 10 jam</div>
                                        @endif
                                    </div>
                                    <small class="text-muted">Pilih Durasi Bermain Dalam Jam</small>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <input  type="submit" 
                                        class="btn btn-{{ $booking->category->color }}-outline btn-block" 
                                        name="submit" 
                                        value="Cari Waktu Kosong">
                            </div>
                        </div>
                        <!-- <div class="row">
                        </div> -->
                    </form>
                </div>
            </div>
        </section>
    </header>
</div>

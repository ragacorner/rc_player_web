<div class="card">
    <article class="profile-post">
        <div class="profile-post-content">
            <div class="tbl profile-post-card">
                <div class="">
                    <div class="col-md-8">
                        <div class="tbl-cell tbl-cell-photo">
                            <a href="#">
                                <img src="{{ asset('img/others/categories/'.$category->name.'.png') }}" alt="">
                            </a>
                        </div>
                        <div class="tbl-cell">
                            <p class=""><b id="">Lapang ++ {{ ucwords($field->name) }}</b></p>
                            <p>Jenis Rumput <b>{{ ucwords($field->grass_type) }}</b></p>
                            
                            @if($jam_diminta->format('H') >= '18')
                                <p class="">Harga /Jam <b>IDR {{ number_format($field->priceNight) }}</b></p>
                            @else
                                <p class="">Harga /Jam <b>IDR {{ number_format($field->priceDayLight) }}</b></p>
                            @endif

                            <p>Luas <b>{{ $field->width }} x {{ $field->length }}</b></p>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="tbl-cell tbl-cell-btns">
                            {!! Form::open(['url' => route('booking.update_time', $booking->code), 'method' => 'post', 'class'=>'m-t-md']) !!}
                                
                                {!! Form::hidden('field_id', $field->id) !!}
                                {!! Form::hidden('duration', $request_duration) !!}
                                {!! Form::hidden('jam', $jam_diminta->format('H')) !!}
                                {!! Form::hidden('hari', $hari_diminta->format('d-m-Y')) !!}

                                <button type="submit" class="btn btn-success btn-rounded">
                                Ubah<br> <small>Waktu dan Lapang</small>
                                </button>
 
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</div>
<div class="card">
    <article class="profile-post">
        <div class="profile-post-content">
            <div class="tbl profile-post-card">
                <div class="">
                    <div class="col-md-8">
                        <div class="tbl-cell tbl-cell-photo">
                            <a href="#">
                                <img src="{{ asset('img/others/categories/'.$category->name.'.png') }}" alt="">
                            </a>
                        </div>
                        <div class="tbl-cell">
                            <p class=""><b id="">Lapang {{ ucwords($booked->name) }}</b></p>
                            <p>Jenis Rumput <b>{{ ucwords($booked->grass_type) }}</b></p>
                            
                            @if($jam_diminta->format('H') >= '18')
                                <p class="">Harga /Jam <b>IDR {{ number_format($booked->priceNight) }}</b></p>
                            @else
                                <p class="">Harga /Jam <b>IDR {{ number_format($booked->priceDayLight) }}</b></p>
                            @endif

                            <p>Luas <b>{{ $booked->width }} x {{ $booked->length }}</b></p>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="tbl-cell tbl-cell-btns">
                            <div class="form-group {{ $errors->has('duration') ? 'form-group-error' : '' }}">
                                <small>Durasi dalam jam</small>
                                <input disabled id="" class="form-control" type="text" value="-" placeholder="Durasi" name="" required>
                                @if ($errors->has('duration'))
                                    <div class="form-tooltip-error">{{ $errors->first('duration') }}</div>
                                @endif
                            </div>
                            <button disabled type="button" class="btn btn-danger btn-block">Booked!</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</div>
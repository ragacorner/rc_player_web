<div class="card">
    <article class="profile-post">
        <div class="profile-post-content">
            <div class="tbl profile-post-card">
                <div class="">
                    <div class="col-md-6">
                        <div class="tbl-cell tbl-cell-photo">
                            <a href="#">
                                <img src="{{ asset('img/others/categories/'.$category->name.'.png') }}" alt="">
                            </a>
                        </div>
                        <div class="tbl-cell">
                            <p class=""><b id="">Lapang {{ ucwords($field->name) }}</b></p>
                            <p>Jenis Rumput <b>{{ ucwords($field->grass_type) }}</b></p>
                            
                            @if($jam_diminta->format('H') >= '18')
                                <p class="">Harga /Jam <b>IDR {{ number_format($field->priceNight) }}</b></p>
                            @else
                                <p class="">Harga /Jam <b>IDR {{ number_format($field->priceDayLight) }}</b></p>
                            @endif

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="tbl-cell tbl-cell-btns">
                            
                            @for($i = 9; $i <= 23; $i++)
                                @php
                                    $jam    = Carbon\Carbon::createFromFormat('H', $i)->toTimeString();
                                    $book   = App\Models\BookedField::where('field_id', $field->id)
                                            ->where('playing_date', $hari_diminta->format('Y-m-d'))
                                            ->where('playing_time', $jam)
                                            ->get();

                                    $conv = "";
                                    foreach($book as $b){
                                        $conv   = Carbon\Carbon::createFromFormat('H:i:s', $b->playing_time)->format('H');
                                    }
                                @endphp

                                
                                
                                @if($i == $conv)
                                        <span class="label label-danger">{{ $conv }}:00</span>  
                                @else
                                    <span class="label label-success">{{ $i }}:00</span>  
                                @endif
                                <!-- <span class="label label-success">{{ $i.':00' }}</span> -->
                            @endfor
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</div>
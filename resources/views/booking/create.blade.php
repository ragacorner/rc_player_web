@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/separate/vendor/bootstrap-touchspin.min.css') }}">
    <!-- <link rel="stylesheet" href="{{ asset('css/separate/vendor/select2.min.css') }}"> -->
@endsection

@section('content')
    <div class="card">
        <div class="card-block">
            <h5 class="with-border">
                Info Booking Tempat Olahraga
            </h5>
            
            <div class="col-md-7">
                <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Kategory</label>
                    <div class="col-sm-8">
                        <p><b>: {{ ucfirst(Request::cookie('request_category')) }}</b></p>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Tempat</label>
                    <div class="col-sm-8">
                        <p>
                            <a href="{{ route('place.show', $field->place->slug) }}" target="_blank">
                                <b>: {{ $place->name }}</b>
                            </a>
                            (Lapang <b>{{ $field->name }}: {{ucwords($field->grass_type)}}</b>)
                        </p>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Alamat</label>
                    <div class="col-sm-8">
                        <p><b>: {{ $place->address }}, {{ $place->subdistrict->display_name }}, {{ $place->regency->display_name }}</b></p>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Waktu Bermain</label>
                    <div class="col-sm-8">
                        <p><b>:</b> Pukul <b>{{ $jam_diminta->format('H:i') }}</b>, Tanggal <b> {{ $hari_diminta->format('d M Y') }}</b></p>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Lama Bermain</label>
                    <div class="col-sm-8">
                        <p><b>:</b> <b>{{ $duration }} Jam</b></p>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Fasilitas</label>
                    <div class="col-sm-8">
                        <p>
                        @foreach($facilities->where('price','free') as $facility)
                            - <b>{{ $facility->item }}</b><br>
                        @endforeach
                        </p>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Harga Lapang /Jam</label>
                    <div class="col-sm-4">
                        <p>
                            @if($jam_diminta->format('H') >= '18')
                                <b>: Rp {{ number_format($field->priceNight) }}</b>
                            @else
                                <b>: Rp {{ number_format($field->priceDayLight) }}</b>
                            @endif
                        </p>
                    </div>
                </div>
                
            </div>
            <div class="col-lg-5">
                <div class="card">
                    <div class="card-block">
                        {!! Form::open(['url' => route('booking.store', [$place->id, $field->id]), 'method' => 'POST', 'class'=>'']) !!}
                            {{ csrf_field() }}

                            {!! Form::hidden('place_id', $place->id) !!}
                            {!! Form::hidden('field_id', $field->id) !!}
                            {!! Form::hidden('duration', $duration) !!}
                            {!! Form::hidden('ip_address', Request::getClientIp()) !!}
                            @if($jam_diminta->format('H') >= '18')
                                {!! Form::hidden('price_field', $field->priceNight) !!}
                            @else
                                {!! Form::hidden('price_field', $field->priceDayLight) !!}
                            @endif

                            <div class="form-group row">
                                <label class="col-sm-4 form-control-label">Nama Pemesan</label>
                                <div class="col-sm-8">
                                    <div class="form-control-wrapper form-control-icon-left">
                                        {!! Form::text('player_id', Auth::user()->name, ['class'=>'form-control', 'placeholder'=>'lengkap sesuai kartu identitas', 'readonly']) !!}
                                        <i class="font-icon fa fa-user color-green"></i>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-sm-4 form-control-label">Harga DP (Rp)</label>
                                <div class="col-sm-8">
                                    <div class="form-control-wrapper form-control-icon-left">
                                        @if($jam_diminta->format('H') >= '18')
                                            <input type="text" class="form-control" value="{{ number_format($field->priceNight / 2) }}" readonly>
                                        @else
                                            <input type="text" class="form-control" value="{{ number_format($field->priceDayLight / 2) }}" readonly>
                                        @endif
                                        <i class="font-icon fa fa-money color-green"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-4 form-control-label">Jenis Permainan</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <select class="select2" name="game_type" required>
                                            <option value="regular" selected>Regular (Biasa/Langganan)</option>
                                            <option value="sparing" disabled>Sparring {{ ucfirst(Request::cookie('request_category')) }} (Latih Tanding)</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-4 form-control-label">Tipe Pembayaran</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <select class="select2" name="payment" required>
                                            <option value="">-- Pilih Cara Bayar --</option>
                                            <option value="onthespot" selected>Bayar Di Tempat (On The Spot)</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-8">
                                    <small class="text-muted m-b-md">
                                        Dengan mengklik tombol Bayar, kamu bersedia membayar harga DP lapangan dalam waktu maksimal 1 jam dimulai dari tombol tersebut diklik.
                                    </small>
                                    <button type="submit" name="button" class="btn btn-success btn-block">
                                        Bayar
                                        <span class="font-icon font-icon-arrow-right"></span>
                                    </button>
                                    <a href="{{ route('home') }}" class="btn btn-success-outline btn-block">
                                        <span class="font-icon font-icon-answer"></span>
                                        Kembali Ke Awal
                                    </a>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('js')
<script src="{{ asset('js/lib/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('js/lib/input-mask/jquery.mask.min.js') }}"></script>
    <script src="{{ asset('js/lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>
    <script type="text/javascript">
        
        $(document).ready(function(){
            // $("input[id='demo4']").TouchSpin();
			// $("input[id='demo_vertical']").TouchSpin({
			// 	verticalbuttons: true
			// });
            $("input[id='demo1']").TouchSpin({
				min: 1,
				max: 10,
				step: 1,
				decimals: 0,
				boostat: 5,
				maxboostedstep: 10,
				postfix: 'Jam'
			});
        });
        $(document).ready(function() {
            $('#phone-with-code-area-mask-input').mask('0000-0000-0000', {placeholder: "____-____-____"});
        });
        $('[data-countdown]').each(function() {
            var $this = $(this), finalDate = $(this).data('countdown');
            $this.countdown(finalDate, function(event) {
                $this.html(event.strftime('%D days %H:%M:%S'));
            });
        });
    </script>
@endsection

<div class="row">
    <h4 class="text-center with-border">Suggestions</h4>
    <div class="card-user-grid">
        @if($hari_diminta <= $batas_hari && $hari_diminta >= $composer_today)
            @if(($request_jam < $composer_now_hour) && ($hari_diminta->format('d-m-Y') == $composer_date))
                <div class="text-center">
                    <h1 class="with-border">Maaf, sekarang jam {{ $now->format('H:i') }} </h1>
                    <h3>Waktu kamu sudah lewat :)</h3>
                </div>
            @else
                @if($jam_diminta->format('i') != '00' )
                    <div class="text-center">
                        <h1 class="with-border">Pemberitahuan!</h1>
                        <h3>Harap gunakan waktu pas (contoh: 09:00, 10:00, 11:00)</h3>
                    </div>
                @else
                    @forelse($places as $place)
                        @if($jam_diminta < $place->open)
                            <div class="text-center">
                                <h1 class="with-border">Maaf :)</h1>
                                <h3>Keliatannya masih belum ada yang buka pada waktu itu</h3>
                            </div>
                        @else

                            @include('layouts.booking-layout.item')
                        
                        @endif
                    @empty
                        <div class="text-center">
                            <h1 class="with-border">Oops!</h1>
                            <h3>Keliatannya sudah terbooking semua lapangan didaerah kamu</h3>
                        </div>
                    @endforelse
                    
                @endif                    
            @endif
        @else
            <div class="text-center">
                <h1 class="with-border">Maaf :)</h1>
                <h3>Batas pemesanan di 
                <i>ragacorner.com</i> 
                hanya bisa dilakukan mulai 
                <b>hari ini</b> 
                sampai 
                <b>1 bulan</b>
                ke depan.</h3>
            </div>
        @endif
    </div>
</div>
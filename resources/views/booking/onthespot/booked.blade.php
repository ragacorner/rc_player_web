@extends('layouts.app')

@section('css')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-block">
                    <h5 class="with-border">
                        Booking Details
                    </h5>
                    <div class="alert alert-warning alert-no-border alert-close alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        Segera Lakukan Pembayaran
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <!-- <div class="col-md-2"></div> -->
                            <div class="col-md-12">
                                <p>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <b>Kode Booking:</b>
                                        </div>
                                        <div class="col-md-8">
                                            {{ $code }}
                                        </div>
                                    </div>
                                </p>
                                <p>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <b>Nama Pemain:</b>
                                        </div>
                                        <div class="col-md-8">
                                            @php
                                                $pemain = App\Models\PlayerOffline::where('code', $code)->first();
                                            @endphp

                                            @if(Auth::guest())
                                                {{ ucwords($pemain->name) }}
                                            @else
                                                {{ Auth::user()->name }}
                                            @endif
                                        </div>
                                    </div>
                                </p>
                                <p>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <b>Nama Tempat:</b>
                                        </div>
                                        <div class="col-md-8">
                                            {{ $place->name }} - ({{ $category }})
                                        </div>
                                    </div>
                                </p>
                                <p>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <b>Bermain Tanggal:</b>
                                        </div>
                                        <div class="col-md-8">
                                            {{ $playing_date }}
                                        </div>
                                    </div>
                                </p>
                                <p>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <b>Bermain Pukul:</b>
                                        </div>
                                        <div class="col-md-8">
                                            {{ $playing_time }}, selama {{ $duration }} Jam
                                        </div>
                                    </div>
                                </p>
                                <p>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <b>Harga Lapang:</b>
                                        </div>
                                        <div class="col-md-8">
                                            @if($playing_time >= '18:00')
                                                Rp {{ number_format($field->priceNight) }} (Harga Malam)
                                            @else
                                                Rp {{ number_format($field->priceDayLight) }} (Harga Siang)
                                            @endif
                                        </div>
                                    </div>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@extends('auth._sublayout')

@section('content')
    <div class="col-md-7">
        <div class="p-t-lg m-t-lg text-center">
            {!! Html::image(asset('img/others/reset_password.png'), null, ['class'=>'','style'=>'height:560px']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="box-typical-full-height">
            <div class="add-customers-screen tbl">
                <div class="add-customers-screen-in">
                    <form method="POST" action="{{ route('password.email') }}" class="sign-box">
                        @csrf
                        
                        <header class="sign-title">{{ __('Atur Ulang Password') }}</header>
                        <div class="form-group">
                            <input type="text" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="E-Mail"/>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-warning btn-block">
                                {{ __('Kirim Reset Link') }}
                            </button>
                            
                            <div class="text-block bs">
                                <p class="sign-note">atau <a href="{{ route('login') }}">Masuk</a></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

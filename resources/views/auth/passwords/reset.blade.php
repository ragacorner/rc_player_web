@extends('auth._sublayout')

@section('content')
    <div class="col-md-7">
        <div class="p-t-lg m-t-lg text-center">
            {!! Html::image(asset('img/others/reset_password.png'), null, ['class'=>'','style'=>'height:560px']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="box-typical-full-height">
            <div class="add-customers-screen tbl">
                <div class="add-customers-screen-in">
                    <form method="POST" action="{{ route('password.update') }}" class="sign-box">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <header class="sign-title">{{ __('Atur Ulang Password') }}</header>

                        <div class="form-group">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" placeholder="Alamat E-Mail" required autofocus>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                            

                        <div class="form-group">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Konfirmasi Password" required>
                            <!-- <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                            </div> -->
                        </div>

                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-warning btn-block">
                                {{ __('Atur Ulang') }}
                            </button>
                        </div>

                        <!-- <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div> -->
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection

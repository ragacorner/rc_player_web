@extends('auth._sublayout')

@section('content')
<!-- <div class="">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verify Your Email Address') }}</div>

                <div class="card-body">
                </div> -->
<div class="col-md-7">
    {{-- <div class="p-t-lg m-t-lg text-center">
        {!! Html::image(asset('img/others/email.png'), null, ['class'=>'image-big']) !!}
    </div> --}}
</div>
<div class="col-md-4">
    <div class="box-typical-full-height">
        <div class="add-customers-screen tbl">
            <div class="add-customers-screen-in">
                <div class="sign-box">
                    <header class="sign-title">{{ __('Verifikasi Alamat Email Kamu') }}</header>
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Link verifikasi yang baru telah dikirim ke email kamu.') }}
                        </div>
                    @endif

                    {{ __('Sebelum melanjutkan, silahkan cek email kamu untuk link verifikasi. Email akan terkirim dalam waktu kurang dari 5 menit.') }}
                    {{ __('Kalo kamu tidak mendapatkan email') }}, <a href="{{ route('verification.resend') }}">{{ __('Klik disini untuk link yang baru') }}</a>.
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

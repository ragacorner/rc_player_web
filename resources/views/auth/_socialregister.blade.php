<section class="contacts-page-section text-center">
    <div class="m-b-md">
        Atau login dengan: 
    </div>
    
    <a href="{{ url('/auth/facebook') }}" class="contact" title="Facebook">
        <i class="font-icon font-icon-facebook"></i>
    </a>
    <a href="{{ url('/auth/twitter') }}" class="contact" title="Twitter">
        <i class="font-icon font-icon-twitter"></i>
    </a>
    <a href="{{ url('/auth/google') }}" class="contact m-r-0" title="Google">
        <i class="font-icon font-icon-google-plus"></i>
    </a>
</section>

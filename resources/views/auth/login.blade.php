@extends('auth._sublayout')

@section('content')
    <div class="col-md-7">
        {{-- <div class="image-big">
        {!! Html::image(asset('img/others/running2.png'), null, ['class'=>'','style'=>'']) !!}
        </div> --}}
    </div>
    <div class="col-md-4">
        <div class="box-typical-full-height">
            <div class="add-customers-screen tbl">
                <div class="add-customers-screen-in">
                    <form class="sign-box"  method="POST" action="{{ route('login') }}">
                        @csrf

                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-icon alert-close alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{ $message }}
                            </div>
                        @endif

                        <div class="">
                            <div class="pull-left fontawesome-icon-list">
                                <a href="{{ url('/') }}" class=""><i class="fa fa-arrow-left"></i></a>
                            </div>
                            <div class="sign-avatar">
                                <img src="img/others/avatar-sign.png" alt="">
                            </div>
                        </div>
                        <header class="sign-title">{{ __('Masuk') }}</header>
                        <div class="form-group">
                            <div class="form-control-wrapper form-control-icon-left">
                                <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="{{ __('E-Mail atau No.HP') }}" required autofocus/>
                                <i class="font-icon font-icon-mail"></i>
                            </div>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <div class="form-control-wrapper form-control-icon-left">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="{{ __('Password') }}" required>
                                <i class="font-icon font-icon-lock"></i>
                            </div>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <div class="float-right reset">
                                <a href="{{ route('password.request') }}">Atur Ulang Password</a>
                            </div>
                        </div>
                        
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-success btn-block">
                                <span class="font-icon glyphicon glyphicon-log-in"></span>
                                {{ __('Masuk') }}
                            </button>

                            {{-- @include('auth._socialregister') --}}
                            
                            <div class="text-block bs">
                                <p class="sign-note">Belum punya akun? <a href="{{ route('register') }}">Daftar</a></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@extends('auth._sublayout')

@section('content')
    <div class="col-md-7">
        <div class="">
            {{-- <div class="image-big">
            {!! Html::image(asset('img/others/sports.png'), null, ['class'=>'image-big']) !!}
            </div> --}}
        </div>
    </div>
    <div class="col-md-4">
        <div class="box-typical-full-height">
            <div class="add-customers-screen tbl">
                <div class="add-customers-screen-in">
                <form class="sign-box"  method="POST" action="{{ route('register') }}">
                    @csrf

                    <div class="pull-left fontawesome-icon-list">
                        <a href="{{ url('/') }}" class=""><i class="fa fa-arrow-left"></i></a>
                    </div>
                    <div class="sign-avatar">
                        <div class="sign-avatar no-photo">&plus;</div>
                    </div>

                    <header class="sign-title">{{ __('Pendaftaran Pemain') }}</header>
                    
                    <div class="form-group">
                        <div class="form-control-wrapper form-control-icon-left">
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Nama Lengkap" required autofocus/>
                            <i class="font-icon font-icon-user"></i>
                        </div>
                        @if ($errors->has('name'))
                            <small class="text-danger">{{ $errors->first('name') }}</small>
                        @endif
                    </div>
                    <div class="form-group">
                        <div class="form-control-wrapper form-control-icon-left">
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-Mail" required/>
                            <i class="font-icon font-icon-mail"></i>
                        </div>
                        @if ($errors->has('email'))
                            <small class="text-danger">{{ $errors->first('email') }}</small>
                        @endif
                    </div>
                    <div class="form-group">
                        <div class="form-control-wrapper form-control-icon-left">
                            <input type="password" class="form-control" name="password" placeholder="Password" required/>
                            <i class="font-icon font-icon-lock"></i>
                        </div>
                        @if ($errors->has('password'))
                            <small class="text-danger">{{ $errors->first('password') }}</small>
                        @endif
                    </div>
                    <div class="form-group">
                        <div class="form-control-wrapper form-control-icon-left">
                            <input type="password" class="form-control" name="password_confirmation" placeholder="Ulangi password" required/>
                            <i class="font-icon font-icon-lock"></i>
                        </div>
                    </div>

                    <div class="form-group">
                        <small class="text-muted m-b-md">
                            Dengan mengklik tombol Daftar, Kamu menyetujui
                            <a href="{{ route('syarat.ketentuan') }}">Syarat dan Ketentuan,</a> serta
                            <a href="{{ route('kebijakan.privasi') }}">Kebijakan Privasi</a>.
                            Kamu akan menerima Email dari Ragacorner dan dapat menolaknya kapan saja.
                        </small>
                        <button type="submit" class="btn btn-success btn-block sign-up">
                            <span class="font-icon glyphicon glyphicon-plus-sign"></span> Daftar
                        </button>

                        {{-- @include('auth._socialregister') --}}
                        
                        <div class="text-block bs">
                            <p class="sign-note">Sudah punya akun? <a href="{{ route('login') }}">Masuk</a></p>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
@endsection

<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Coming Soon | Ragacorner</title>

	<link href="img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.png" rel="icon" type="image/png">
	<link href="img/favicon.ico" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

    <link rel="stylesheet" href="{{ asset('css/separate/pages/prices.min.css') }}">
    <!-- <link rel="stylesheet" href="{{ asset('css/separate/vendor/pnotify.min.css') }}"> -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
    
    <link rel="stylesheet" href="{{ asset('css/separate/pages/widgets.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/lib/font-awesome/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/lib/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
</head>
<body>
    @include('player.layouts.header')
    <div class="page-center">
        <div class="page-center-in">
            <div class="container-fluid">

                @include('player.layouts.flash_message')
            
				<div class="box-typical-center">
					<div class="box-typical-center-in prices-page">
						<header class="prices-page-title">SELAMAT DATANG DI RAGACORNER :)</header>
                        {{ Request::getClientIp() }}
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <p class="prices-page-subtitle">Bermula dari usulan dan permintaan para pencinta  olahraga dalam memberikan layanan teknologi, <b>ragacorner</b> berdiri untuk memberikan layanan kepada mereka. <br>Salam Olahraga!</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <section class="widget widget-time aquamarine">
                                    <header class="widget-header-dark with-btn">
                                        <!-- We are coming soon in... -->
                                        Kami akan segera tiba dalam waktu...
                                    </header>
                                    <div class="widget-time-content">
                                        <div id="demo"></div>
                                    </div>
                                </section><!--.widget-time-->
                                <form action="#" method="post">
                                @csrf
                                    <div class="form-group">
                                        <textarea name="visitor_message" id="" cols="10" rows="7" class="form-control" placeholder="Beri pesan, saran, atau masukan untuk kami.." required></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Kirim pesan</button>
                                </form>
                            </div>

                        </div>

                        
                    </div>
                </div>
            </div>
        </div>
    </div><!--.page-center-->

<script src="{{ asset('js/lib/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('js/lib/tether/tether.min.js') }}"></script>
<script src="{{ asset('js/lib/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/plugins.js') }}"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
@include('sweet::alert')

<script type="text/javascript" src="{{ asset('js/lib/match-height/jquery.matchHeight.min.js') }}"></script>
    <script>
        $(function() {
            $('.page-center').matchHeight({
                target: $('html')
            });

            $(window).resize(function(){
                setTimeout(function(){
                    $('.page-center').matchHeight({ remove: true });
                    $('.page-center').matchHeight({
                        target: $('html')
                    });
                },100);
            });
        });
    </script>

    <script>
        // Set the date we're counting down to
        var countDownDate = new Date("Mar 28, 2019 00:00:00").getTime();

        // Update the count down every 1 second
        var x = setInterval(function() {

            // Get todays date and time
            var now = new Date().getTime();
            
            // Find the distance between now and the count down date
            var distance = countDownDate - now;
            
            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            
            // Output the result in an element with id="demo"
            document.getElementById("demo").innerHTML = 
                `<div class="count-item">
                    <div class="count-item-number">`+days+`</div>
                    <div class="count-item-caption">Hari</div>
                </div><div class="count-item divider">:</div>`+
                `<div class="count-item">
                    <div class="count-item-number">`+hours+`</div>
                    <div class="count-item-caption">Jam</div>
                </div><div class="count-item divider">:</div>`+
                `<div class="count-item">
                    <div class="count-item-number">`+minutes+`</div>
                    <div class="count-item-caption">Menit</div>
                </div><div class="count-item divider">:</div>`+
                `<div class="count-item">
                    <div class="count-item-number">`+seconds+`</div>
                    <div class="count-item-caption">Detik</div>
                </div>`;
            //      + "d " + hours + "h "
            // + minutes + "m " + seconds + "s ";
            
            // If the count down is over, write some text 
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("demo").innerHTML = "EXPIRED";
            }
        }, 1000);
    </script>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
@extends('layouts.app')

@section('css')
    <style>
        #map_canvas_3{
            width: relative;
            height: 550px;
        }
    </style>
@endsection

@section('content')

    <!-- <div class="p-t-md"></div> -->
    
    <div class="row">
            
        <div class="col-md-3">
			@include('places.placeinfo')
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-block">
                    <ul class="nav nav-pills">
                        <li class="nav-item">
                            <a class="nav-link active" href="#maps" role="tab" data-toggle="tab">Maps</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#lapangan" role="tab" data-toggle="tab">Lapangan</a>
                        </li>
                        <!-- <li class="nav-item">
                            <a class="nav-link" href="#">Weakest</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Strongest</a>
                        </li> -->
                    </ul>
                </div>
            </div>
            <section class="contacts-page">
				<div class="tab-content">
					<div role="tabpanel" class="clearfix tab-pane active" id="maps">
                        <div class="box-typical">
						    <div id="map_canvas_3" class="map"></div>
                        </div>
					</div>
					<div role="tabpanel" class="clearfix tab-pane" id="lapangan">
						@foreach($fields as $field)
							@include('places._list')
						@endforeach
					</div>
				</div><!--.tab-content-->
			</section><!--.box-typical-->
        </div>
		<div class="col-md-3">
			@include('places._suggestion')
			
		</div>
    </div>

@endsection

@section('js')

<script src="https://maps.googleapis.com/maps/api/js?&key=AIzaSyAdpDQ8H7FJ5hilzJzhd7CKVYQj9u6xz6M&libraries=places&callback=initMap" async defer></script>
<script>
	function initMap() {

		var lat = {{ $place->lat }};
		var lng = {{ $place->lng }};
		var map = new google.maps.Map(document.getElementById('map_canvas_3'), {
			center:{
				lat: lat,
				lng: lng
			},
			zoom:15
		});
		
		var marker = new google.maps.Marker({
			position:{
				lat: lat,
				lng: lng
			},
			map:map
		});
	}
</script>
@endsection
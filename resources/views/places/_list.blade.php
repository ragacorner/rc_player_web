<div class="card">
    <article class="profile-post">
        <div class="profile-post-content">
            <div class="tbl profile-post-card">
                <div class="">
                    <div class="col-md-8">
                        <div class="tbl-cell tbl-cell-photo">
                            <a href="#">
                                <img src="{{ asset('img/others/categories/'.$category->name.'.png') }}" alt="">
                            </a>
                        </div>
                        <div class="tbl-cell">
                            <p class=""><b id="">Lapang {{ ucwords($field->name) }}</b></p>
                            <p>Jenis Rumput <b>{{ ucwords($field->grass_type) }}</b></p>
                            <p class="">Harga Malam /Jam <b>Rp {{ number_format($field->priceNight) }}</b></p>
                            <p class="">Harga Siang /Jam <b>Rp {{ number_format($field->priceDayLight) }}</b></p>
                            <p>Luas <b>{{ $field->width }} x {{ $field->length }}</b></p>

                        </div>
                    </div>
                    <div class="col-md-4">
                        
                    </div>
                </div>
            </div>
        </div>
    </article>
</div>
<article class="card-user box-typical">
    <div class="p-b-md image-size">
        @if(empty($place->photo))
            {!! Html::image(asset('img/place/building.jpg'), null, ['class'=>'','title'=>'http://solo.tribunnews.com/2018/02/14/gor-manahan-solo-segera-direnovasi-wali-kota-sebut-bakal-jadi-gor-berkelas-internasional']) !!}
        @else
            <img src="{{ url('http://place.ragacorner.dvlp/img/place/original/'.$place->photo) }}" alt="">
        @endif
    </div>
    <div class="card-user-name">{{ ucwords($place->name) }}</div>
    <div class="card-user-status">
        {{ strtoupper($category->name) }}
    </div>

</article>

<!-- <article class="card-user box-typical">
</article> -->

<section class="tabs-section">
    <div class="tabs-section-nav tabs-section-nav-icons">
        <div class="tbl">
            <ul class="nav" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#tabs-1-tab-1" role="tab" data-toggle="tab">
                        <span class="nav-link-in">
                            Info
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#tabs-1-tab-2" role="tab" data-toggle="tab">
                        <span class="nav-link-in">
                            Fasilitas
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </div><!--.tabs-section-nav-->

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="tabs-1-tab-1">
            <div class="card-block">
                <p class="line-with-icon">
                    <i class="font-icon font-icon-pin-2"></i>
                    {{ $place->address }}, 
                    {{ App\Models\Subdistrict::find($place->subdistrict_id)->display_name }},
                    {{ App\Models\Regency::find($place->regency_id)->display_name }}, 
                    {{ App\Models\Province::find($place->province_id)->display_name }}
                </p>
                <p class="line-with-icon">
                    <i class="font-icon fa fa-clock-o"></i>
                    @php
                        $open = Carbon\Carbon::createFromFormat('H:i:s',$place->open);
                        $close = Carbon\Carbon::createFromFormat('H:i:s',$place->close);
                    @endphp

                    {{ $open->format('H:i') }} - {{ $close->format('H:i') }} (Buka-Tutup)
                </p>
                <!-- <p class="line-with-icon">
                    +<i class="font-icon font-icon-phone"></i>
                    {{ $place->contact }} (Lapangan)
                </p> -->
            </div>
        </div><!--.tab-pane-->
        <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-2">
            <div class="card-block">
                @forelse($place->facilities as $f)
                    <p class="line-with-icon">
                        <i class="font-icon font-icon-arrow-right"></i>
                        {{ ucwords($f->name) }}<small></small>
                    </p>
                @empty
                    kosong
                @endforelse
                
            </div>
        </div><!--.tab-pane-->
    </div><!--.tab-content-->
</section><!--.tabs-section-->

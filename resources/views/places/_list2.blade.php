<div class="col-md-12 item_place" id="">
    <div class="card">
        <article class="profile-post">
            <div class="profile-post-content">
                <div class="tbl profile-post-card">
                    <div class="">
                        <div class="col-md-6">
                            <div class="tbl-cell tbl-cell-photo">
                                <a href="#">
                                    <img src="{{ asset('img/others/categories/'.$category->name.'.png') }}" alt="">
                                </a>
                            </div>
                            <div class="tbl-cell">
                                <p class=""><b id="">Lapang {{ ucwords($field->id) }}</b></p>
                                <p>Jenis Rumput <b>{{ ucwords($field->grass_type) }}</b></p>
                                
                                    <p class="">Harga /Jam <b>IDR {{ number_format($field->priceNight) }}</b></p>
                                

                                <p>Luas <b>{{ $field->width }} x {{ $field->length }}</b></p>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="tbl-cell tbl-cell-btns pull-right">
                                @php
                                    $booked = App\Models\BookedField::where('playing_date', $hari_diminta)
                                            ->where('field_id', $field->id)->get()->sortBy('playing_time');
                                @endphp

                                {{$field->id}} - {{$place->slug}}

                                @for($i = 9; $i <= 23; $i++)
                                    @php
                                        $jam = Carbon\Carbon::createFromFormat('H', $i)->toTimeString();
                                    @endphp

                                    @foreach($booked->where('playing_time', $jam) as $book)
                                        <span class="label label-danger">{{ $book->playing_time }}</span>  
                                    @endforeach
                                    
                                    <span class="label label-success">{{ $i.':00' }}</span>
                                @endfor
                                
                                <!-- <span class="label label-danger">08:00</span>
                                <span class="label label-danger">08:00</span>
                                <span class="label label-danger">08:00</span>
                                <span class="label label-danger">08:00</span>
                                <span class="label label-danger">08:00</span>
                                <span class="label label-danger">08:00</span>
                                <button class="btn btn-primary btn-sm">
                                    <span class="ladda-label">small</span>
                                </button>
                                <button class="btn btn-primary btn-sm">
                                    <span class="ladda-label">small</span>
                                </button>
                                <button class="btn btn-primary btn-sm">
                                    <span class="ladda-label">small</span>
                                </button>
                                <button class="btn btn-primary btn-sm">
                                    <span class="ladda-label">small</span>
                                </button>
                                <button class="btn btn-primary btn-sm">
                                    <span class="ladda-label">small</span>
                                </button>
                                <button class="btn btn-primary btn-sm">
                                    <span class="ladda-label">small</span>
                                </button> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>
</div>
@extends('player._sublayout')

@section('subcontent')
<div role="tabpanel" class="tab-pane active" id="tabs-2-tab-4">
    <section class="box-typical card-block">


        @if($unplayed_books->count() > 0)
            @php
                $now = $composer_now->toTimeString();
                $play = $unplayed_books->first()->playing_time;
                $warning_b4_play = Carbon\Carbon::createFromFormat('H:i:s', $play)->subHour()->toTimeString();
            @endphp
            
            @if($now >= $warning_b4_play && $now < $play)
                <div class="alert alert-warning alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>Perhatian!</strong> Waktu bermain kamu sebentar lagi.
                </div>
            @endif
        @else
            <div class="alert alert-info alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>My Games</strong> adalah daftar permainan yang kamu mainkan.
            </div>
        @endif

        @include('player.games.unplayed')
        @include('player.games.played')
        
        <ul class="text-center">
            {{-- $bookeds->links() --}}
        </ul>
    </section>

</div>
@endsection
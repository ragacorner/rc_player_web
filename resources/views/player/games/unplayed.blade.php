@forelse($unplayed_books as $booking)
    
    @php
        $tanggal_invoice = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $booking->updated_at)->format('d M Y');
        $tanggal_main = Carbon\Carbon::createFromFormat('Y-m-d', $booking->playing_date);
        $mulai = Carbon\Carbon::createFromFormat('H:i:s', $booking->playing_time);
        $selesai = Carbon\Carbon::createFromFormat('H:i:s', $booking->playing_time)->addHour($booking->duration)->format('H:i');
        $limit_booking = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $booking->created_at)->addHours(2)->format('H'.':00');
    @endphp
    
    @if($booking->paid_half == 'false' && $booking->paid_full == 'false')
    <section class="card card-orange profile-settings">
    @else
    <section class="card card-blue profile-settings">
    @endif
        <div class="">
            <article class="profile-post">
                @if($booking->paid_half == 'false' && $booking->paid_full == 'false')
                    <div class="card-header">
                        <header>
                            Segera Selesaikan Pembayaran Sebelum Pukul: {{ $limit_booking }}
                        </header>
                    </div>
                @endif
                <div class="profile-post-content">
                    <div class="tbl profile-post-card">
                        <div class="tbl-row">
                            <div class="tbl-cell tbl-cell-photo">
                                <a href="{{ route('place.show', $booking->place->slug) }}">
                                    @if(empty($booking->place->photo))
                                        {!! Html::image(asset('img/place/building.jpg'), null, ['class'=>'','style'=>'height:100px','title'=>'http://solo.tribunnews.com/2018/02/14/gor-manahan-solo-segera-direnovasi-wali-kota-sebut-bakal-jadi-gor-berkelas-internasional']) !!}
                                    @else
                                        {!! Html::image(url('http://place.ragacorner.dvlp/img/place/500x500/'.$booking->place_id.'.jpg'), null, ['class'=>'','style'=>'height:100px']) !!}<!-- <img src="{{ url('http://ragacorner.dvlp/img/player/original/'.$booking->player->profile->photo) }}" style="height:100px" alt=""> -->
                                    @endif
                                </a>
                            </div>
                            <div class="tbl-cell">
                                <p class="title">
                                    <a href="{{ route('place.show', $booking->place->slug) }}"><b>{{ $booking->place->name }}</b></a> (Lapangan {{ $booking->place->category->display_name }})
                                    <p>Lapang {{ $booking->field->name }}</p>
                                </p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="text-muted">Bermain Pada:</p>
                                        <p class="">{{ $tanggal_main->format('l, d M Y') }}</p>
                                        <p class="">{{ $mulai->format('H:i') }} - {{ $selesai }}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="text-muted">Jenis Permainan:</p>
                                        <p class="">{{ ucwords($booking->game_type) }}</p>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-typical-footer">
                    <div class="row">
                        <div class="col-md-7"> 
                            <strong>Code: #{{ $booking->code }}</strong>
                            
                            @if($booking->paid_half == 'false' && $booking->paid_full == 'false')
                                <span class="label label-warning">Belum Dibayar</span>
                            @elseif($booking->paid_half == 'true' && $booking->paid_full == 'false')
                                <span class="label label-primary">DP Terbayar</span>
                            @else
                                <span class="label label-success">Lunas</span>
                            @endif
                        </div>
                        <div class="col-md-5">
                            <div class="pull-right">
                                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                    @if($booking->paid_half == "false")
                                        <form id="cancel_booking" class="delete" action="{{ route('booking.cancel', $booking->id) }}" method="POST">
                                            <input type="hidden" name="_method" value="DELETE">
                                            @csrf
                                            <button type="submit" class="btn btn-default-outline btn-sm">Batal Pesan</button>
                                        </form>
                                    @endif
                                    @if($booking->paid_half == 'true' || $booking->paid_full == 'true')
                                    <a  href="{{ route('booking.change_time', $booking->code) }}"
                                        onclick="event.preventDefault(); document.getElementById('change-form-{{ $booking->code }}').submit();" 
                                        class="btn btn-default-outline btn-sm">
                                        Ubah Waktu
                                    </a>
                                    @endif

                                    @if($booking->paid_half == 'true' || $booking->paid_full == 'true')
                                    <form id="change-form-{{ $booking->code }}" action="{{ route('booking.change_time', $booking->code) }}" method="GET">
                                        @csrf
                                        <input type="hidden" name="hari" value="{{ $tanggal_main->addDays(7)->format('d-m-Y') }}">
                                        <input type="hidden" name="jam" value="{{ $mulai->format('H') }}">
                                        <input type="hidden" name="duration" value="{{ $booking->duration }}">
                                    </form>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
@endforeach
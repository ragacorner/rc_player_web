
@foreach($bookeds->sortByDesc('created_at') as $booking)
    @php
        $tanggal_invoice = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $booking->updated_at)->format('d M Y');
        $tanggal_main = Carbon\Carbon::createFromFormat('Y-m-d', $booking->playing_date);
        $mulai = Carbon\Carbon::createFromFormat('H:i:s', $booking->playing_time);
        $selesai = Carbon\Carbon::createFromFormat('H:i:s', $booking->playing_time)->addHour($booking->duration)->format('H:i');
    @endphp

    @if($booking->played == 'true')
    <section class="box-typical profile-settings">
        <div class="">
            <article class="profile-post">
                <div class="profile-post-content">
                    <div class="tbl profile-post-card">
                        <div class="tbl-row">
                            <div class="tbl-cell tbl-cell-photo">
                                <a href="{{ route('place.show', $booking->place->slug) }}">
                                    @if(empty($booking->place->photo))
                                        {!! Html::image(asset('img/place/building.jpg'), null, ['class'=>'','style'=>'height:100px','title'=>'http://solo.tribunnews.com/2018/02/14/gor-manahan-solo-segera-direnovasi-wali-kota-sebut-bakal-jadi-gor-berkelas-internasional']) !!}
                                    @else
                                        {!! Html::image(url('http://place.ragacorner.dvlp/img/place/500x500/'.$booking->place_id.'.jpg'), null, ['class'=>'','style'=>'height:100px']) !!}
                                        <!-- <img src="{{ url('http://ragacorner.dvlp/img/player/original/'.$booking->player->profile->photo) }}" style="height:100px" alt=""> -->
                                    @endif
                                    <!-- <img src="img/100x100.jpg" alt=""> -->
                                </a>
                            </div>
                            <div class="tbl-cell">
                                <p class="title">
                                    <a href="{{ route('place.show', $booking->place->slug) }}"><b>{{ $booking->place->name }}</b></a> (Lapangan {{ $booking->place->category->display_name }})
                                    <p>Lapang {{ $booking->field->name }}</p>
                                </p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="text-muted">Bermain Pada:</p>
                                        <p class="">{{ $tanggal_main->format('l, d M Y') }}</p>
                                        <p class="">{{ $mulai->format('H:i') }} - {{ $selesai }}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="text-muted">Jenis Permainan:</p>
                                        <p class="">{{ ucwords($booking->game_type) }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-typical-footer">
                    <div class="row">
                        <div class="col-md-6">
                            Code: 
                            <a href="{{ route('player.booklogs.invoice', [Auth::user()->profile->username,$booking->place->slug]) }}">
                                <strong>#{{ $booking->code }}</strong>
                            </a>
                        </div>
                        <div class="col-md-6"> 
                            <div class="pull-right">
                                <span class="label label-success">Permainan Selesai</span>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
    @else
    <section class="card  profile-settings">
        <div class="">
            <article class="profile-post">
                <div class="profile-post-content">
                    <div class="tbl profile-post-card">
                        <div class="tbl-row">
                            <div class="tbl-cell tbl-cell-photo">
                                <a href="{{ route('place.show', $booking->place->slug) }}">
                                    @if(empty($booking->place->photo))
                                        {!! Html::image(asset('img/place/building.jpg'), null, ['class'=>'','style'=>'height:100px','title'=>'http://solo.tribunnews.com/2018/02/14/gor-manahan-solo-segera-direnovasi-wali-kota-sebut-bakal-jadi-gor-berkelas-internasional']) !!}
                                    @else
                                        {!! Html::image(url('http://place.ragacorner.dvlp/img/place/500x500/'.$booking->place_id.'.jpg'), null, ['class'=>'','style'=>'height:100px']) !!}<!-- <img src="{{ url('http://ragacorner.dvlp/img/player/original/'.$booking->player->profile->photo) }}" style="height:100px" alt=""> -->
                                    @endif
                                </a>
                            </div>
                            <div class="tbl-cell">
                                <p class="title">
                                    <a href="{{ route('place.show', $booking->place->slug) }}"><b>{{ $booking->place->name }}</b></a> (Lapangan {{ $booking->place->category->display_name }})
                                    <p>Lapang {{ $booking->field->name }}</p>
                                </p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="text-muted">Bermain Pada:</p>
                                        <p class="">{{ $tanggal_main->format('l, d M Y') }}</p>
                                        <p class="">{{ $mulai->format('H:i') }} - {{ $selesai }}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="text-muted">Jenis Permainan:</p>
                                        <p class="">{{ ucwords($booking->game_type) }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-typical-footer">
                    <div class="row">
                        <div class="col-md-6">
                            Code: 
                            <a href="{{ route('player.booklogs.invoice', [Auth::user()->profile->username,$booking->place->slug]) }}">
                                <strong>#{{ $booking->code }}</strong>
                            </a>
                        </div>
                        <div class="col-md-6"> 
                            <div class="pull-right">
                                <span class="label label-danger">Permainan Terlewati</span>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
    @endif
@endforeach
<aside class="profile-side">
    <section class="box-typical profile-side-user">
        <a href="{{ route('player.profile.edit', Auth::user()->profile->username) }}" class="avatar-preview avatar-preview-128">
            @if(empty(Auth::user()->profile->photo))
                <img src="{{ asset('img/uistart/avatar-1-256.png') }}" alt="">
            @else
                <img src="{{ asset('img/player/800x800/'.Auth::user()->profile->username.'.jpg') }}" alt="">
            @endif
        </a>
        <div class="m-t-md"><b>{{ Auth::user()->name }}</b></div>
        <div class="bottom-txt">
            @if(empty(Auth::user()->profile->description))
                <p>
                    <!-- Deskripsikan dirimu.  -->
                    <a href="{{ route('player.profile.edit', Auth::user()->profile->username) }}" class="btn btn-rounded btn-success-outline">
                        <span class="font-icon font-icon-pencil"></span>
                        Edit
                    </a> 
                </p>
            @else
                <p>{{ Auth::user()->profile->description }}</p>
            @endif
        </div>

        @if(Request::segment(1) == 'booklogs')
            <a href="{{ route('player.profile.edit', Auth::user()->profile->username) }}" class="btn btn-rounded">
                <span class="font-icon font-icon-pencil"></span> Ubah Profile
            </a>
        @endif
    </section>

    <section class="box-typical">
        <header class="box-typical-header-sm bordered">Tentang Kamu</header>
        @php
            $profile = Auth::user()->profile;
            $now    = Carbon\Carbon::now();
            $lahir  = Carbon\Carbon::createFromFormat('Y-m-d', $profile->birthday);
            $umur   = $lahir->diffInYears($now);
            $join   = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $profile->player->created_at)->diffForHumans();
        @endphp
        <div class="box-typical-inner">
            <p class="line-with-icon">
                <i class="font-icon fa fa-venus-mars"></i>
                @if($profile->gender == 'male')
                    Pria
                @else
                    Wanita
                @endif
            </p>
            <p class="line-with-icon">
                <i class="font-icon fa fa-birthday-cake"></i>
                {{ $umur }} Tahun
            </p>
            <p class="line-with-icon">
                <i class="font-icon font-icon-case-3"></i>
                @if($profile->occupation == 'smp' || $profile->occupation == 'sma')
                    Pelajar {{ strtoupper($profile->occupation) }}
                @else
                    {{ ucwords($profile->occupation) }}
                @endif
            </p>
            <p class="line-with-icon">
                <i class="font-icon fa fa-heart"></i>
                {{ $profile->category->display_name }}
            </p>
            <p class="line-with-icon">
                <i class="font-icon font-icon-calend"></i>
                Join since {{ $join }}
            </p>
            
        </div>
    </section>
    
    <section class="box-typical">
        <header class="box-typical-header-sm bordered">Domisili</header>
        <div class="box-typical-inner">
            <p class="line-with-icon">
                <i class="font-icon font-icon-pin-2"></i>
                {{ $player_subd }}, {{ $player_regency }}
            </p>
        </div>
    </section>
</aside>
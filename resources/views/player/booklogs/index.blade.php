@extends('player._sublayout')

@section('subcontent')
<div role="tabpanel" class="tab-pane active" id="tabs-2-tab-4">
    <section class="box-typical card-block">
        <div class="alert alert-info alert-border-left alert-close alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>Booklogs</strong> adalah daftar catatan booking lapangan yang kamu lakukan dan sudah selesai kamu mainkan.
        </div>

        @forelse($bookings as $booking)
            @php
                $tanggal_invoice = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $booking->updated_at)->format('d M Y');
                $tanggal_main = Carbon\Carbon::createFromFormat('Y-m-d', $booking->playing_date)->format('l, d M Y');
                $mulai = Carbon\Carbon::createFromFormat('H:i:s', $booking->playing_time)->format('H:i');
                $selesai = Carbon\Carbon::createFromFormat('H:i:s', $booking->playing_time)->addHour($booking->duration)->format('H:i');
            @endphp
            <section class="box-typical profile-settings">
                <div class="">
                    <article class="profile-post">
                        <div class="profile-post-content">
                            <div class="tbl profile-post-card">
                                <div class="tbl-row">
                                    <div class="tbl-cell tbl-cell-photo">
                                        {!! Html::image(asset('img/others/invoice.jpg'), null, ['class'=>'','style'=>'height:100px','title'=>'https://www.freepik.com/free-vector/flat-composition-with-payment-elements_1265663.htm']) !!}
                                    </div>
                                    <div class="tbl-cell">
                                        <p class="title">
                                            <a href="{{ route('player.booklogs.invoice', [Auth::user()->profile->username,$booking->place->slug]) }}">
                                                <b>#{{ $booking->code }}</b>
                                                @if($booking->paid_half == 'false' && $booking->paid_full == 'false')
                                                    <span class="label label-warning">Belum Dibayar</span>
                                                @elseif($booking->paid_full == 'true')
                                                    <span class="label label-success">Pembayaran Lunas</span>
                                                @else
                                                    <span class="label label-primary">DP Terbayar</span>
                                                @endif

                                            </a>
                                        </p>

                                        <p class="text-muted">Tempat Bermain:</p>
                                        <p>
                                            <a href="{{ route('place.show', $booking->place->slug) }}">
                                                {{ $booking->place->name }}
                                            </a> 
                                            (Lapangan {{ $booking->place->category->display_name }})
                                        </p>
                                        <p class="">{{ $tanggal_main }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </section>
        @empty
            <section class="box-typical profile-settings">
                <div class="card-block text-center">
                    <h1 class="with-border">Kosong!</h1>
                    <h3>Kelihatannya kamu belum selesai bermain atau belum booking satupun :)</h3>
                </div>
            </section>
        @endforelse
        
        <ul class="text-center">
            {{ $bookings->links() }}
        </ul>
    </section>

</div>
@endsection
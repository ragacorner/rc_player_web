@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/separate/pages/invoice.min.css') }}">
@endsection

@section('content')
    @php
        $tanggal_invoice = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $booking->updated_at)->format('d M Y');
        $tanggal_main = Carbon\Carbon::createFromFormat('Y-m-d', $booking->playing_date);
        $mulai = Carbon\Carbon::createFromFormat('H:i:s', $booking->playing_time);
        $selesai = Carbon\Carbon::createFromFormat('H:i:s', $booking->playing_time)->addHour($booking->duration)->format('H:i');
    @endphp

    <section class="card">
        <header class="card-header card-header-lg">
            Ragacorner Invoice
        </header>
        <div class="card-block invoice">
            <div class="row">
                <div class="col-lg-6 company-info">
                    <h5>
                        {{ $place->name }}
                        <small>- Lapang {{ $place->category->display_name }}</small>
                    </h5>
                    <!-- <p>www.ragacorner.com</p> -->

                    <div class="invoice-block">
                        <div>{{ $place->address }}</div>
                        <div>{{ $place->subdistrict->display_name }}, {{ $place->regency->display_name }}</div>
                        <div>{{ $place->province->display_name }}, Indonesia</div>
                    </div>

                    <div class="invoice-block">
                        <div>{{ $place->contact }}</div>
                        <!-- <div>Fax: 555-692-7754</div> -->
                    </div>

                    <div class="invoice-block">
                        <h5>Invoice To:</h5>
                        <div>{{ $player->name }}</div>
                        <div>
                            {{ $player->profile->area }}, {{ $player->profile->subdistrict->display_name }}, {{ $booking->player->profile->regency->display_name }},<br> 
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 clearfix invoice-info">
                    <div class="text-lg-right">
                        <h5>INVOICE #{{ $booking->code }}</h5>
                        <div>Date: {{ $inv }}</div>
                    </div>

                    <div class="payment-details">
                        <strong>Payment Details</strong>
                        <table>
                            <tr>
                                <td>Jenis Pembayaran:</td>
                                <td>Ditempat (On The Spot)</td>
                            </tr>
                            <tr>
                                <td>Terbayar:</td>
                                <td>Rp {{ number_format($booking->payment->amount) }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row table-details">
                <div class="card-block">
                    <div class="col-lg-12">
                        <h5><strong>Booking Lapangan</strong></h5>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Nama Lapang</th>
                                    <th>Waktu Bermain</th>
                                    <th width="200">Jam Bermain</th>
                                    <th width="200">Durasi Bermain</th>
                                    <th width="300">Harga Lapang</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Lapang {{ $booking->field->name }}</td>
                                    <td>{{ $tanggal_main->format('l, d M Y') }}</td>
                                    <td>{{ $mulai->format('H:i') }}</td>
                                    <td>{{ $booking->duration }} Jam</td>
                                    <td>Rp {{ number_format($booking->price_field) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                @if($products->count() > 0)
                    <div class="card-block">
                        <div class="col-lg-12">
                            <h5><strong>Pembelian Produk</strong></h5>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Nama Item</th>
                                        <th width="200">Banyak</th>
                                        <th width="200">Harga /item</th>
                                        <th width="300">Harga Lapang</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($products as $product)
                                        <tr>
                                            <td>{{ ucwords($product->product->name) }}</td>
                                            <td>{{ $product->qty }} Buah</td>
                                            <td>Rp {{ number_format($product->product->price) }}</td>
                                            <td>Rp {{ number_format($product->amount) }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
            </div>
            <div class="row">
                <div class="col-lg-7 terms-and-conditions">
                    <strong>Terms and Conditions</strong>
                    Akan kami update di lain waktu.
                </div>
                <div class="col-lg-5 clearfix">
                    <div class="total-amount">
                        @php
                            $total = $booking->price_field + $products->sum('amount');
                        @endphp
                        <!-- <div>Sub - Total amount: <b>$4,800</b></div> -->
                        <!-- <div>VAT: $35</div> -->
                        <strong><div>Jumlah Total: <span class="colored">Rp {{ number_format($total) }}</span></div></strong>
                        <!-- <div class="actions">
                            <button class="btn btn-rounded btn-inline">Send</button>
                            <button class="btn btn-inline btn-secondary btn-rounded">Print</button>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
<section class="widget widget-accordion" id="accordion" role="tablist" aria-multiselectable="true">
    
    @if($bookings->where('played','false')->count() < 2)
        @forelse($bookings->where('played','false') as $booking)
            

            <article class="panel">
                <div class="panel-heading" role="tab" id="{{ $booking->id }}">
                    <a data-toggle="collapse"
                        data-parent="#accordion"
                        href="#coll{{ $booking->id }}"
                        aria-expanded="true"
                        aria-controls="collapseOne">
                        Permainan {{ $booking->category->display_name }}
                        <i class="font-icon font-icon-arrow-down"></i>
                    </a>
                </div>
                <div id="coll{{ $booking->id }}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="{{ $booking->id }}">
                    <div class="panel-collapse-in">
                        <div class="user-card-row">
                            <div class="tbl-row">
                                <div class="tbl-cell tbl-cell-photo">
                                    <a href="#">
                                        @if(empty($booking->place->photo))
                                            {!! Html::image(asset('img/place/building.jpg'), null, ['class'=>'','style'=>'height:500px,width:500px','title'=>'http://solo.tribunnews.com/2018/02/14/gor-manahan-solo-segera-direnovasi-wali-kota-sebut-bakal-jadi-gor-berkelas-internasional']) !!}
                                        @else
                                            <img src="{{ url('http://place.ragacorner.dvlp/img/place/original/'.$booking->place->photo) }}" style="height:500px, widht:500px" alt="">
                                        @endif
                                    </a>
                                </div>
                                <div class="tbl-cell">
                                    <p class="user-card-row-name"><a href="{{ route('place.show',$booking->place->slug) }}">{{ $booking->place->name }}</a></p>
                                    <p class="user-card-row-location">{{ $booking->place->address }}, {{ $booking->subdistrict->display_name }}, {{ $booking->regency->display_name }}</p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <header class="title">No Booking:</header>
                                <p class="no-margin">
                                    {{ $booking->code }} 
                                </p>
                            </div>
                            <div class="col-md-6">
                                <div class="pull-right">
                                    <header class="title">Pembayaran:</header>
                                    <p class="">
                                    @if($booking->paid_half == 'false' && $booking->paid_full == 'false')
                                        <span class="label label-warning">Belum Dibayar</span>
                                    @elseif($booking->paid_half == 'true' && $booking->paid_full == 'false')
                                        <span class="label label-primary">DP Terbayar</span>
                                    @else
                                        <span class="label label-success">Lunas</span>
                                    @endif
                                    </p>
                                </div>
                            </div>
                        </div>

                        <header class="title">Bermain {{ $booking->category->display_name }} Pada:</header>
                        <div class="row">
                            <div class="col-md-12">
                                <p>
                                    {{ $tanggal_main->format('l, d M Y') }}<br>{{ $mulai->format('H:i') }} - {{ $selesai }}
                                </p>
                            </div>
                            <div class="col-md-12">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        @empty
            <a href="{{ route('home') }}" class="btn btn-success btn-block">Mulai Booking</a>
        @endforelse
    @else
        @forelse($bookings->where('played','false') as $booking)
            @php
                $tanggal_invoice = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $booking->updated_at)->format('d M Y');
                $tanggal_main = Carbon\Carbon::createFromFormat('Y-m-d', $booking->playing_date);
                $mulai = Carbon\Carbon::createFromFormat('H:i:s', $booking->playing_time);
                $selesai = Carbon\Carbon::createFromFormat('H:i:s', $booking->playing_time)->addHour($booking->duration)->format('H:i');
            @endphp
            
            <article class="panel">
                <div class="panel-heading" role="tab" id="{{ $booking->id }}">
                    <a data-toggle="collapse"
                        data-parent="#accordion"
                        href="#coll{{ $booking->id }}"
                        aria-expanded="true"
                        aria-controls="collapseOne">
                        Permainan {{ $booking->category->display_name }}
                        <i class="font-icon font-icon-arrow-down"></i>
                    </a>
                </div>
                <div id="coll{{ $booking->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="{{ $booking->id }}">
                    <div class="panel-collapse-in">
                        <div class="user-card-row">
                            <div class="tbl-row">
                                <div class="tbl-cell tbl-cell-photo">
                                    <a href="#">
                                        @if(empty($booking->place->photo))
                                            {!! Html::image(asset('img/place/building.jpg'), null, ['class'=>'','style'=>'height:500px,width:500px','title'=>'http://solo.tribunnews.com/2018/02/14/gor-manahan-solo-segera-direnovasi-wali-kota-sebut-bakal-jadi-gor-berkelas-internasional']) !!}
                                        @else
                                            <img src="{{ url('http://place.ragacorner.dvlp/img/place/original/'.$booking->place->photo) }}" style="height:500px, widht:500px" alt="">
                                        @endif
                                    </a>
                                </div>
                                <div class="tbl-cell">
                                    <p class="user-card-row-name"><a href="{{ route('place.show',$booking->place->slug) }}">{{ $booking->place->name }}</a></p>
                                    <p class="user-card-row-location">{{ $booking->place->address }}, {{ $booking->subdistrict->display_name }}, {{ $booking->regency->display_name }}</p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <header class="title">No Booking:</header>
                                <p class="no-margin">
                                    {{ $booking->code }} 
                                </p>
                            </div>
                            <div class="col-md-6">
                                <div class="pull-right">
                                    <header class="title">Pembayaran:</header>
                                    <p class="">
                                    @if($booking->paid_half == 'false' && $booking->paid_full == 'false')
                                        <span class="label label-warning">Belum Dibayar</span>
                                    @elseif($booking->paid_half == 'true' && $booking->paid_full == 'false')
                                        <span class="label label-primary">DP Terbayar</span>
                                    @else
                                        <span class="label label-success">Lunas</span>
                                    @endif
                                    </p>
                                </div>
                            </div>
                        </div>

                        <header class="title">Bermain {{ $booking->category->display_name }} Pada:</header>
                        <div class="row">
                            <div class="col-md-12">
                                <p>
                                    {{ $tanggal_main->format('l, d M Y') }}<br>{{ $mulai->format('H:i') }} - {{ $selesai }}
                                </p>
                            </div>
                            <div class="col-md-12">
                            <div class="form-group text-center">
                                    <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                        @if($booking->paid_half == "false")
                                        @endif
                                        <a  href="{{ route('booking.cancel', $booking->id) }}"
                                            onclick="event.preventDefault(); document.getElementById('cancel-form-{{ $booking->id }}').submit();" 
                                            class="btn btn-default-outline">
                                            Batal Pesan
                                        </a>
                                        <a  href="{{ route('booking.change_time', $booking->code) }}"
                                            onclick="event.preventDefault(); document.getElementById('change-form-{{ $booking->code }}').submit();" 
                                            class="btn btn-default-outline">
                                            Ubah Waktu
                                        </a>

                                        <form id="cancel-form-{{ $booking->id }}" class="delete" action="{{ route('booking.cancel', $booking->id) }}" method="POST">
                                            <input type="hidden" name="_method" value="DELETE">
                                            @csrf
                                        </form>
                                        <form id="change-form-{{ $booking->code }}" action="{{ route('booking.change_time', $booking->code) }}" method="GET">
                                            @csrf
                                            <input type="hidden" name="hari" value="{{ $tanggal_main->addDays(7)->format('d-m-Y') }}">
                                            <input type="hidden" name="jam" value="{{ $mulai->format('H') }}">
                                            <input type="hidden" name="duration" value="{{ $booking->duration }}">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        @empty
            <a href="{{ route('home') }}" class="btn btn-success btn-block">Mulai Booking</a>
        @endforelse
    @endif
        
</section>
    
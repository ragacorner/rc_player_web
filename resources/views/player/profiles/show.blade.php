@extends('player._sublayout')

@section('subcontent')
	<div role="tabpanel" class="tab-pane active" id="tabs-2-tab-4">
        <section class="box-typical profile-settings">
            <section class="box-typical-section">
				<header class="box-typical-header-sm">Akun</header>
                <div class="form-group row">
                    <div class="col-xl-3">
                        <label class="form-label">Nama</label>
                    </div>
                    <div class="col-xl-7">
                        <p class="form-label">:
							<b>{{ Auth::user()->name }}</b>
						</p>
                    </div>
					<div class="col-xl-2 pull-left">
                        <a 	href="{{ route('player.profile.edit', $profile->username) }}"
							class="btn btn-success-outline">
                            <span class="font-icon font-icon-pencil"></span>
                            Edit
                        </a>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xl-3">
                        <label class="form-label">Email</label>
                    </div>
                    <div class="col-xl-7">
						<p class="form-label">:
							<b>{{ Auth::user()->email }}</b>
						</p>
                    </div>
                </div>
				<div class="form-group row">
                    <div class="col-xl-3">
                        <label class="form-label">No.Handphone</label>
                    </div>
                    <div class="col-xl-7">
						<p class="form-label">:
							<b>{{ Auth::user()->phone }}</b>
						</p>
                    </div>
                </div>
            </section>

            <section class="box-typical-section">
                <header class="box-typical-header-sm">Info</header>
				<div class="form-group row">
                    <div class="col-xl-3">
                        <label class="form-label">Tentang Kamu</label>
                    </div>
                    <div class="col-xl-7">
						<p class="form-label">:
							<b>{{ $profile->description }}</b>
						</p>
                    </div>
                </div>
				<div class="form-group row">
                    <div class="col-xl-3">
                        <label class="form-label">Username</label>
                    </div>
                    <div class="col-xl-7">
						<p class="form-label">:
							<b>{{ $profile->username }}</b>
						</p>
                    </div>
                </div>
				<div class="form-group row">
                    <div class="col-xl-3">
                        <label class="form-label">Umur</label>
                    </div>
                    <div class="col-xl-7">
						<p class="form-label">:
							<b>{{ $umur }}</b> Tahun
						</p>
                    </div>
                </div>
				<div class="form-group row">
                    <div class="col-xl-3">
                        <label class="form-label">Olahraga Favorit</label>
                    </div>
                    <div class="col-xl-7">
						<p class="form-label">:
							<b>{{ $profile->category->display_name }}</b>
						</p>
                    </div>
                </div>
				<div class="form-group row">
                    <div class="col-xl-3">
                        <label class="form-label">Pekerjaan</label>
                    </div>
                    <div class="col-xl-7">
						<p class="form-label">:
							<b>{{ ucwords($profile->occupation) }}</b>
						</p>
                    </div>
                </div>
				<div class="form-group row">
                    <div class="col-xl-3">
                        <label class="form-label">Foto Profil</label>
                    </div>
                    <div class="col-xl-7">
                        @if(empty($profile->photo))
							<img src="{{ asset('img/uistart/avatar-1-256.png') }}" alt="">
						@else
							<img src="{{ asset('img/player/800x800/'.$profile->username.'.jpg') }}" style="height:200px" alt="">
						@endif
                    </div>
                </div>
			</section>
            
			<section class="box-typical-section">
                <header class="box-typical-header-sm">Alamat</header>
				<div class="form-group row">
                    <div class="col-xl-3">
                        <label class="form-label">Domisili</label>
                    </div>
                    <div class="col-xl-7">
						<p class="form-label">:
							<b>{{ $profile->area }}, {{ $profile->subdistrict->display_name }}, {{ $profile->regency->display_name }}</b>
						</p>
                    </div>
                </div>
            </section>
        </section>
    </div><!--.tab-pane-->
@endsection
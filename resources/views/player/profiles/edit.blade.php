@extends('player._sublayout')

@section('subcontent')
	<div role="tabpanel" class="tab-pane active" id="">
        <section class="box-typical profile-settings">
        {!! Form::open(['url' => route('player.profile.update', Auth::user()->profile->username), 'method' => 'put', 'files'=>'true', 'id'=>'']) !!}
            {{ csrf_field() }}
           
            <section class="box-typical-section">
				<!-- <header class="box-typical-header">Akun</header> -->
                <div class="form-group row">
                    <div class="col-xl-3">
                        <label class="form-label">Nama <span class="color-red">*</span></label>
                    </div>
                    <div class="col-xl-7">
                        <input class="form-control" type="text" value="{{ Auth::user()->name }}" name="name" autofocus required/>
                        @if ($errors->has('name'))
                            <small class="text-danger">{{ $errors->first('name') }}</small>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xl-3">
                        <label class="form-label">Email <span class="color-red">*</span></label>
                    </div>
                    <div class="col-xl-7">
                        <input class="form-control" type="text" value="{{ Auth::user()->email }}" name="email" required/>
                        @if ($errors->has('email'))
                            <small class="text-danger">{{ $errors->first('email') }}</small>
                        @endif
                    </div>
                </div>
				<div class="form-group row">
                    <div class="col-xl-3">
                        <label class="form-label">No.Handphone <span class="color-red">*</span></label>
                    </div>
                    @if ($errors->has('phone'))
                        <div class="col-xl-7">
                            {!! Form::text('phone', null, ['class'=>'form-control form-control-red-fill','id'=>'phone-with-code-area-mask-input', 'placeholder'=>'Masukan nomor handphone', '']) !!}
                            <small class="text-muted">{{ $errors->first('phone') }}</small>
                        </div>
                    @else
                        <div class="col-xl-7">
                            {!! Form::text('phone', $player->phone, ['class'=>'form-control','id'=>'phone-with-code-area-mask-input', 'required']) !!}
                        </div>
                    @endif
                </div>
            </section>

            <section class="box-typical-section">
                <header class="box-typical-header-sm">Alamat Domisili</header>
                <div class="form-group row">
                    <div class="col-xl-3">
                        <label class="form-label">Kabupaten <span class="color-red">*</span></label>
                    </div>
                    <div class="col-lg-7">
                        <select name="regency_id" class="select2" required>
                            <option value="" selected>-- Pilih Kota/Kabupaten --</option>
                            
                            @foreach($composer_provincies as $prov)
                                <optgroup label="{{$prov->display_name}}">
                                    
                                    @foreach($composer_regencies as $reg) 
                                        @if($prov->id == $reg->province_id)
                                            @if($reg->name == $player->profile->regency->name)
                                                <option value="{{ $reg->id }}" selected>{{ $reg->display_name }}</option> 
                                            @else
                                                <option value="{{ $reg->id }}">{{ $reg->display_name }}</option>
                                            @endif
                                        @endif
                                    @endforeach
                                    
                                </optgroup>
                            @endforeach
                            
                        </select>
                        @if ($errors->has('regency_id'))
                            <small class="text-danger">{{ $errors->first('regency_id') }}</small>
                        @endif
                    </div>
                    <div class="col-lg-1"></div>
                </div>
                <div class="form-group row">
                    <div class="col-xl-3">
                        <label class="form-label">Kecamatan <span class="color-red">*</span></label>
                    </div>
                    <div class="col-lg-7">
                        <select name="subdistrict_id" class="select2" required>
                            @foreach($composer_subdistricts->where('regency_id', $profile->regency_id) as $sub)
                                @if($sub->id == $profile->subdistrict_id)
                                    <option value="{{ $sub->id }}" selected>{{ $sub->display_name }}</option>
                                @else
                                    <option value="{{ $sub->id }}">{{ $sub->display_name }}</option>
                                @endif
                            @endforeach
                        </select>
                        @if ($errors->has('subdistrict_id'))
                            <small class="text-danger">{{ $errors->first('subdistrict_id') }}</small>
                        @endif
                    </div>
                    <div class="col-lg-1"></div>
                </div>
                <div class="form-group row">
                    <div class="col-xl-3">
                        <label class="form-label">Kelurahan <span class="color-red">*</span></label>
                    </div>
                    <div class="col-lg-7">
                        {!! Form::text('area', $profile->area, ['class'=>'form-control','placeholder'=>'Nama Kelurahan','required']) !!}
                        @if ($errors->has('area'))
                            <small class="text-danger">{{ $errors->first('area') }}</small>
                        @else
                            <!-- <small class="text-muted">Nama Daerah</small> -->
                        @endif
                    </div>
                    <div class="col-lg-1">
                        
                    </div>
                </div>
            </section>

            <section class="box-typical-section">
                <header class="box-typical-header-sm">Info</header>
                <div class="form-group row">
                    <div class="col-xl-3">
                        <label class="form-label">Username <span class="color-red">*</span></label>
                    </div>
                    <div class="col-xl-7">
                        <input class="form-control" type="text" value="{{ Auth::user()->profile->username }}" name="username" required/>
                        @if ($errors->has('username'))
                            <small class="text-danger">{{ $errors->first('username') }}</small>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xl-3">
                        <label class="form-label">Jenis Kelamin <span class="color-red">*</span></label>
                    </div>
                    <div class="col-xl-7">
                        <select class="select2" name="gender" required>
                            <option value="male" {{$profile->gender == 'male' ? 'selected':''}}>Pria</option>
                            <option value="female" {{$profile->gender == 'female' ? 'selected':''}}>Wanita</option>
                        </select>
                        @if ($errors->has('gender'))
                            <small class="text-danger">{{ $errors->first('gender') }}</small>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xl-3">
                        <label class="form-label">Olahraga Favorit <span class="color-red">*</span></label>
                    </div>
                    <div class="col-xl-7">
                        <select name="category_id" class="select2-photo" required>
                            
                            @foreach($composer_categories as $cat)

                                @php $count_place = $cat->places->count(); @endphp

                                @if($count_place > 0)
                                    @if($profile->category_id == $cat->id)
                                        <option data-photo="{{ asset('img/others/categories/'.$cat->name.'.png') }}" 
                                            value="{{ $cat->id }}" selected>{{ $cat->display_name }}
                                        </option>
                                    @else
                                        <option data-photo="{{ asset('img/others/categories/'.$cat->name.'.png') }}" 
                                            value="{{ $cat->id }}">{{ $cat->display_name }}
                                        </option>
                                    @endif
                                @endif
                            @endforeach
                        </select>
                        @if ($errors->has('category_id'))
                            <small class="text-danger">Harap diisi dengan benar</small>
                        @endif
                    </div>
                </div>
				<div class="form-group row">
                    <div class="col-xl-3">
                        <label class="form-label">Pekerjaan <span class="color-red">*</span></label>
                    </div>
                    <div class="col-xl-7">
                        <select name="occupation" class="select2" required>
                            <option value="">-- Pilih Pekerjaan --</option>
                            
                            <option value="smp" {{$profile->occupation == 'smp' ? 'selected':''}}>Pelajar SMP</option>
                            <option value="sma" {{$profile->occupation == 'sma' ? 'selected':''}}>Pelajar SMA</option>
                            <option value="mahasiswa" {{$profile->occupation == 'mahasiswa' ? 'selected':''}}>Mahasiswa</option>
                            <option value="karyawan" {{$profile->occupation == 'karyawan' ? 'selected':''}}>Karyawan</option>
                            <option value="wiraswasta" {{$profile->occupation == 'wiraswasta' ? 'selected':''}}>Wiraswasta</option>
                        </select>
                        @if ($errors->has('occupation'))
                            <small class="text-danger">{{ $errors->first('occupation') }}</small>
                        @endif
                    </div>
                </div>
				<div class="form-group row">
                    <div class="col-xl-3">
                        <label class="form-label">Tentang Kamu</label>
                    </div>
                    <div class="col-xl-7">
                        @if(empty(Auth::user()->profile->description))
                            <textarea rows="3" class="form-control" name="description"></textarea>
                        @else
                            <textarea rows="3" class="form-control" name="description">{{ Auth::user()->profile->description }}</textarea>
                        @endif
                    </div>
                </div>
				<div class="form-group row">
                    <div class="col-xl-3">
                        <label class="form-label">Tanggal Lahir</label>
                    </div>
                    <div class="col-xl-7">
                        <div class='input-group date'>
                            @php
                                $default    = Carbon\Carbon::now()->subYears(10)->format('m/d/Y');
                                $birthday   = Carbon\Carbon::createFromFormat('Y-m-d', Auth::user()->profile->birthday)->format('m/d/Y');
                            @endphp
                            @if(Auth::user()->profile->birthday == NULL || empty(Auth::user()->profile->birthday))
                                <input id="daterange3" type="text" name="birthday" value="{{ $default }}" class="form-control">
                            @else
                                <input id="daterange3" type="text" name="birthday" value="{{ $birthday }}" class="form-control">
                            @endif
                            <span class="input-group-addon">
                                <i class="font-icon font-icon-calend"></i>
                            </span>
                        </div>
                    </div>
                </div>
				
				<div class="form-group row">
                    <div class="col-xl-3">
                        <label class="form-label">Foto Profil</label>
                    </div>
                    <div class="col-xl-7">
                        {!! Form::file('photo', null, ['class'=>'form-control']) !!}
						
                        @if(empty(Auth::user()->profile->photo))
							<img src="{{ asset('img/uistart/avatar-1-256.png') }}" class=" m-t-md" alt="">
						@else
							<img src="{{ asset('img/player/800x800/'.$profile->username.'.jpg') }}" style="height:200px" class="m-t-md">
						@endif
                    </div>
                </div>
			</section>

            <section class="box-typical-section profile-settings-btns">
                <button type="submit" class="btn btn-success">
                    <span class="font-icon glyphicon glyphicon-floppy-saved"></span> Update
                </button>
                <a href="{{ route('player.profile.show', Auth::user()->profile->username) }}" class="btn btn-info-outline">
                    <span class="font-icon glyphicon glyphicon-floppy-remove"></span> Cancel
                </a>
            </section>

        {!! Form::close() !!}
        </section>
    </div><!--.tab-pane-->
@endsection
@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('css/separate/pages/others.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/separate/vendor/bootstrap-daterangepicker.min.css') }}">
@yield('subcss')
@endsection

@section('content')
	<div class="row">
		<div class="col-md-3">
			@include('player._info')
		</div>
		<div class="col-md-6">
			<section class="tabs-section">
				<div class="tabs-section-nav tabs-section-nav-left">
					<ul class="nav" role="tablist">
						<li class="nav-item">
							<a 	class="nav-link {{ Request::segment(2) == 'my-games' ? 'active':'' }}" 
								href="{{ route('player.games.index', Auth::user()->profile->username) }}">
								<span class="nav-link-in">My Games</span>
							</a>
						</li>
						<!-- <li class="nav-item">
							<a 	class="nav-link {{-- Request::segment(2) == 'booklogs' ? 'active':'' --}}" 
								href="{{-- route('player.booklogs.index', Auth::user()->profile->username) --}}">
								<span class="nav-link-in">Booklogs</span>
							</a>
						</li> -->
						<li class="nav-item">
							<a 	class="nav-link {{ Request::segment(2) == 'profile' ? 'active':'' }}" 
								href="{{ route('player.profile.show', Auth::user()->profile->username) }}">
								<span class="nav-link-in">Profile</span>
							</a>
						</li>
					</ul>
				</div>

				<div class="tab-content no-styled profile-tabs">
					
					@yield('subcontent')
					
				</div>
			</section>
		</div>
        <div class="col-md-3">
			@if(Request::segment(3) != 'edit')
				<h5 class="with-border">Penantang Sparing</h5>
				
			@endif
		</div>
	</div>
@endsection

@section('js')
	<script src="{{ asset('js/lib/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('js/get-places.js') }}"></script>
	<script src="{{ asset('js/lib/input-mask/jquery.mask.min.js') }}"></script>

	<script type="text/javascript" src="{{ asset('js/lib/moment/moment-with-locales.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/lib/eonasdan-bootstrap-datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
	<script src="{{ asset('js/lib/daterangepicker/daterangepicker.js') }}"></script>
	
    <script type="text/javascript">
		$('#daterange3').daterangepicker({
			singleDatePicker: true,
			showDropdowns: true
		});

        $(document).ready(function() {
            $('#phone-with-code-area-mask-input').mask('000000000000', {placeholder: "____________"});
			// $('#username-mask-input').mask('AAAAAAAAA-AAAAAAAAAA', {placeholder: "____-____-____"});
        });

		$(".delete").on("submit", function(){
			return confirm("Kamu yakin ingin membatalkan pesanan?");
		});
	</script>
@endsection

@extends('player._sublayout')

@section('subcontent')
    <div role="tabpanel" class="tab-pane active" id="tabs-2-tab-1">
        <form class="box-typical">
            <input type="text" class="write-something" placeholder="What`s on your mind"/>
            <div class="box-typical-footer">
                <div class="tbl">
                    <div class="tbl-row">
                        <div class="tbl-cell">
                            <button type="button" class="btn-icon">
                                <i class="font-icon font-icon-earth"></i>
                            </button>
                            <button type="button" class="btn-icon">
                                <i class="font-icon font-icon-picture"></i>
                            </button>
                            <button type="button" class="btn-icon">
                                <i class="font-icon font-icon-calend"></i>
                            </button>
                            <button type="button" class="btn-icon">
                                <i class="font-icon font-icon-video-fill"></i>
                            </button>
                        </div>
                        <div class="tbl-cell tbl-cell-action">
                            <button type="submit" class="btn btn-rounded">Send</button>
                        </div>
                    </div>
                </div>
            </div>
        </form><!--.box-typical-->

        <article class="box-typical profile-post">
            <div class="profile-post-header">
                <div class="user-card-row">
                    <div class="tbl-row">
                        <div class="tbl-cell tbl-cell-photo">
                            <a href="#">
                                <img src="img/photo-64-2.jpg" alt="">
                            </a>
                        </div>
                        <div class="tbl-cell">
                            <div class="user-card-row-name"><a href="#">Tim Collins</a></div>
                            <div class="color-blue-grey-lighter">3 days ago - 23 min read</div>
                        </div>
                    </div>
                </div>
                <a href="#" class="shared">
                    <i class="font-icon font-icon-share"></i>
                </a>
            </div>
            <div class="profile-post-content">
                <p class="profile-post-content-note">Subminted a new post</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
            </div>
            <div class="box-typical-footer profile-post-meta">
                <a href="#" class="meta-item">
                    <i class="font-icon font-icon-heart"></i>
                    45 Like
                </a>
                <a href="#" class="meta-item">
                    <i class="font-icon font-icon-comment"></i>
                    18 Comment
                </a>
            </div>
            <div class="comment-rows-container hover-action scrollable-block">
                <div class="comment-row-item">
                    <div class="avatar-preview avatar-preview-32">
                        <a href="#">
                            <img src="img/photo-64-2.jpg" alt="">
                        </a>
                    </div>
                    <div class="tbl comment-row-item-header">
                        <div class="tbl-row">
                            <div class="tbl-cell tbl-cell-name">Eric Fox</div>
                            <div class="tbl-cell tbl-cell-date">04.07.15, 20:02 PM</div>
                        </div>
                    </div>
                    <div class="comment-row-item-content">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
                        <button type="button" class="comment-row-item-action edit">
                            <i class="font-icon font-icon-pencil"></i>
                        </button>
                        <button type="button" class="comment-row-item-action del">
                            <i class="font-icon font-icon-trash"></i>
                        </button>
                    </div>
                </div><!--.comment-row-item-->

                <div class="comment-row-item">
                    <div class="avatar-preview avatar-preview-32">
                        <a href="#">
                            <img src="img/avatar-2-64.png" alt="">
                        </a>
                    </div>
                    <div class="tbl comment-row-item-header">
                        <div class="tbl-row">
                            <div class="tbl-cell tbl-cell-name">Vasilisa</div>
                            <div class="tbl-cell tbl-cell-date">04.07.15, 20:02 PM</div>
                        </div>
                    </div>
                    <div class="comment-row-item-content">
                        <p>Yes!</p>
                        <button type="button" class="comment-row-item-action edit">
                            <i class="font-icon font-icon-pencil"></i>
                        </button>
                        <button type="button" class="comment-row-item-action del">
                            <i class="font-icon font-icon-trash"></i>
                        </button>
                    </div>
                </div><!--.comment-row-item-->

                <div class="comment-row-item">
                    <div class="avatar-preview avatar-preview-32">
                        <a href="#">
                            <img src="img/avatar-2-64.png" alt="">
                        </a>
                    </div>
                    <div class="tbl comment-row-item-header">
                        <div class="tbl-row">
                            <div class="tbl-cell tbl-cell-name">Vasilisa</div>
                            <div class="tbl-cell tbl-cell-date">04.07.15, 20:02 PM</div>
                        </div>
                    </div>
                    <div class="comment-row-item-content">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy...</p>
                        <button type="button" class="comment-row-item-action edit">
                            <i class="font-icon font-icon-pencil"></i>
                        </button>
                        <button type="button" class="comment-row-item-action del">
                            <i class="font-icon font-icon-trash"></i>
                        </button>
                        <div class="comment-row-item quote">
                            <div class="avatar-preview avatar-preview-32">
                                <a href="#">
                                    <img src="img/photo-64-2.jpg" alt="">
                                </a>
                            </div>
                            <div class="tbl comment-row-item-header">
                                <div class="tbl-row">
                                    <div class="tbl-cell tbl-cell-name">Adam Oliver</div>
                                    <div class="tbl-cell tbl-cell-date">04.07.15, 20:02 PM</div>
                                </div>
                            </div>
                            <div class="comment-row-item-content">
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet...</p>
                            </div>
                        </div><!--.comment-row-item-->
                    </div>
                </div><!--.comment-row-item-->

                <div class="comment-row-item">
                    <div class="avatar-preview avatar-preview-32">
                        <a href="#">
                            <img src="img/photo-64-2.jpg" alt="">
                        </a>
                    </div>
                    <div class="tbl comment-row-item-header">
                        <div class="tbl-row">
                            <div class="tbl-cell tbl-cell-name">Henry Olson</div>
                            <div class="tbl-cell tbl-cell-date">04.07.15, 20:02 PM</div>
                        </div>
                    </div>
                    <div class="comment-row-item-content">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                        <button type="button" class="comment-row-item-action edit">
                            <i class="font-icon font-icon-pencil"></i>
                        </button>
                        <button type="button" class="comment-row-item-action del">
                            <i class="font-icon font-icon-trash"></i>
                        </button>
                    </div>
                </div><!--.comment-row-item-->

                <div class="comment-row-item">
                    <div class="avatar-preview avatar-preview-32">
                        <a href="#">
                            <img src="img/photo-64-2.jpg" alt="">
                        </a>
                    </div>
                    <div class="tbl comment-row-item-header">
                        <div class="tbl-row">
                            <div class="tbl-cell tbl-cell-name">Henry Olson</div>
                            <div class="tbl-cell tbl-cell-date">04.07.15, 20:02 PM</div>
                        </div>
                    </div>
                    <div class="comment-row-item-content">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
                        <button type="button" class="comment-row-item-action edit">
                            <i class="font-icon font-icon-pencil"></i>
                        </button>
                        <button type="button" class="comment-row-item-action del">
                            <i class="font-icon font-icon-trash"></i>
                        </button>
                    </div>
                </div><!--.comment-row-item-->

                <div class="comment-row-item">
                    <div class="avatar-preview avatar-preview-32">
                        <a href="#">
                            <img src="img/avatar-2-64.png" alt="">
                        </a>
                    </div>
                    <div class="tbl comment-row-item-header">
                        <div class="tbl-row">
                            <div class="tbl-cell tbl-cell-name">Vasilisa</div>
                            <div class="tbl-cell tbl-cell-date">04.07.15, 20:02 PM</div>
                        </div>
                    </div>
                    <div class="comment-row-item-content">
                        <p>No!</p>
                        <button type="button" class="comment-row-item-action edit">
                            <i class="font-icon font-icon-pencil"></i>
                        </button>
                        <button type="button" class="comment-row-item-action del">
                            <i class="font-icon font-icon-trash"></i>
                        </button>
                    </div>
                </div><!--.comment-row-item-->

                <div class="comment-row-item">
                    <div class="avatar-preview avatar-preview-32">
                        <a href="#">
                            <img src="img/avatar-2-64.png" alt="">
                        </a>
                    </div>
                    <div class="tbl comment-row-item-header">
                        <div class="tbl-row">
                            <div class="tbl-cell tbl-cell-name">Vasilisa</div>
                            <div class="tbl-cell tbl-cell-date">04.07.15, 20:02 PM</div>
                        </div>
                    </div>
                    <div class="comment-row-item-content">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed...</p>
                        <button type="button" class="comment-row-item-action edit">
                            <i class="font-icon font-icon-pencil"></i>
                        </button>
                        <button type="button" class="comment-row-item-action del">
                            <i class="font-icon font-icon-trash"></i>
                        </button>
                    </div>
                </div><!--.comment-row-item-->

                <div class="comment-row-item">
                    <div class="avatar-preview avatar-preview-32">
                        <a href="#">
                            <img src="img/photo-64-2.jpg" alt="">
                        </a>
                    </div>
                    <div class="tbl comment-row-item-header">
                        <div class="tbl-row">
                            <div class="tbl-cell tbl-cell-name">Roger Dunn</div>
                            <div class="tbl-cell tbl-cell-date">04.07.15, 20:02 PM</div>
                        </div>
                    </div>
                    <div class="comment-row-item-content">
                        <p>Lorem ipsum dolor sit amet</p>
                        <button type="button" class="comment-row-item-action edit">
                            <i class="font-icon font-icon-pencil"></i>
                        </button>
                        <button type="button" class="comment-row-item-action del">
                            <i class="font-icon font-icon-trash"></i>
                        </button>
                    </div>
                </div><!--.comment-row-item-->

                <div class="comment-row-item">
                    <div class="avatar-preview avatar-preview-32">
                        <a href="#">
                            <img src="img/photo-64-2.jpg" alt="">
                        </a>
                    </div>
                    <div class="tbl comment-row-item-header">
                        <div class="tbl-row">
                            <div class="tbl-cell tbl-cell-name">Lorem ipsum dolor sit amet</div>
                            <div class="tbl-cell tbl-cell-date">04.07.15, 20:02 PM</div>
                        </div>
                    </div>
                    <div class="comment-row-item-content">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                        <button type="button" class="comment-row-item-action edit">
                            <i class="font-icon font-icon-pencil"></i>
                        </button>
                        <button type="button" class="comment-row-item-action del">
                            <i class="font-icon font-icon-trash"></i>
                        </button>
                    </div>
                </div><!--.comment-row-item-->

                <div class="comment-row-item">
                    <div class="avatar-preview avatar-preview-32">
                        <a href="#">
                            <img src="img/avatar-2-64.png" alt="">
                        </a>
                    </div>
                    <div class="tbl comment-row-item-header">
                        <div class="tbl-row">
                            <div class="tbl-cell tbl-cell-name">Vasilisa</div>
                            <div class="tbl-cell tbl-cell-date">04.07.15, 20:02 PM</div>
                        </div>
                    </div>
                    <div class="comment-row-item-content">
                        <p>Yes!</p>
                        <button type="button" class="comment-row-item-action edit">
                            <i class="font-icon font-icon-pencil"></i>
                        </button>
                        <button type="button" class="comment-row-item-action del">
                            <i class="font-icon font-icon-trash"></i>
                        </button>
                    </div>
                </div><!--.comment-row-item-->

                <div class="comment-row-item">
                    <div class="avatar-preview avatar-preview-32">
                        <a href="#">
                            <img src="img/avatar-2-64.png" alt="">
                        </a>
                    </div>
                    <div class="tbl comment-row-item-header">
                        <div class="tbl-row">
                            <div class="tbl-cell tbl-cell-name">Vasilisa</div>
                            <div class="tbl-cell tbl-cell-date">04.07.15, 20:02 PM</div>
                        </div>
                    </div>
                    <div class="comment-row-item-content">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt</p>
                        <button type="button" class="comment-row-item-action edit">
                            <i class="font-icon font-icon-pencil"></i>
                        </button>
                        <button type="button" class="comment-row-item-action del">
                            <i class="font-icon font-icon-trash"></i>
                        </button>
                    </div>
                </div><!--.comment-row-item-->

                <div class="comment-row-item">
                    <div class="avatar-preview avatar-preview-32">
                        <a href="#">
                            <img src="img/photo-64-2.jpg" alt="">
                        </a>
                    </div>
                    <div class="tbl comment-row-item-header">
                        <div class="tbl-row">
                            <div class="tbl-cell tbl-cell-name">Eric Fox</div>
                            <div class="tbl-cell tbl-cell-date">04.07.15, 20:02 PM</div>
                        </div>
                    </div>
                    <div class="comment-row-item-content">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt</p>
                        <button type="button" class="comment-row-item-action edit">
                            <i class="font-icon font-icon-pencil"></i>
                        </button>
                        <button type="button" class="comment-row-item-action del">
                            <i class="font-icon font-icon-trash"></i>
                        </button>
                    </div>
                </div><!--.comment-row-item-->
            </div><!--.comment-rows-container-->
            <input type="text" class="write-something" placeholder="Leave a comment"/>
            <div class="box-typical-footer">
                <div class="tbl">
                    <div class="tbl-row">
                        <div class="tbl-cell">
                            <button type="button" class="btn-icon">
                                <i class="font-icon font-icon-earth"></i>
                            </button>
                            <button type="button" class="btn-icon">
                                <i class="font-icon font-icon-picture"></i>
                            </button>
                            <button type="button" class="btn-icon">
                                <i class="font-icon font-icon-calend"></i>
                            </button>
                            <button type="button" class="btn-icon">
                                <i class="font-icon font-icon-video-fill"></i>
                            </button>
                        </div>
                        <div class="tbl-cell tbl-cell-action">
                            <button type="submit" class="btn btn-rounded">Send</button>
                        </div>
                    </div>
                </div>
            </div>
        </article>

        <article class="box-typical profile-post">
            <div class="profile-post-header">
                <div class="user-card-row">
                    <div class="tbl-row">
                        <div class="tbl-cell tbl-cell-photo">
                            <a href="#">
                                <img src="img/photo-64-2.jpg" alt="">
                            </a>
                        </div>
                        <div class="tbl-cell">
                            <div class="user-card-row-name"><a href="#">Tim Collins</a></div>
                            <div class="color-blue-grey-lighter">3 days ago - 23 min read</div>
                        </div>
                    </div>
                </div>
                <a href="#" class="shared">
                    <i class="font-icon font-icon-share"></i>
                </a>
            </div>
            <div class="profile-post-content">
                <p class="profile-post-content-note">Added 4 new pictures</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                <div class="profile-post-gall-fluid profile-post-gall-grid" data-columns>
                    <div class="col">
                        <a class="fancybox" rel="gall-1" href="img/gall-img-1.jpg">
                            <img src="img/gall-img-1.jpg" alt="">
                        </a>
                    </div>
                    <div class="col">
                        <a class="fancybox" rel="gall-1" href="img/gall-img-2.jpg">
                            <img src="img/gall-img-2.jpg" alt="">
                        </a>
                    </div>
                    <div class="col">
                        <a class="fancybox" rel="gall-1" href="img/gall-img-3.jpg">
                            <img src="img/gall-img-3.jpg" alt="">
                        </a>
                    </div>
                    <div class="col">
                        <a class="fancybox" rel="gall-1" href="img/gall-img-4.jpg">
                            <img src="img/gall-img-4.jpg" alt="">
                        </a>
                    </div>
                    <div class="col">
                        <a class="fancybox" rel="gall-1" href="img/gall-img-5.jpg">
                            <img src="img/gall-img-5.jpg" alt="">
                        </a>
                    </div>
                    <div class="col">
                        <a class="fancybox" rel="gall-1" href="img/gall-img-6.jpg">
                            <img src="img/gall-img-6.jpg" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="box-typical-footer profile-post-meta">
                <a href="#" class="meta-item">
                    <i class="font-icon font-icon-heart"></i>
                    45 Like
                </a>
                <a href="#" class="meta-item">
                    <i class="font-icon font-icon-comment"></i>
                    18 Comment
                </a>
            </div>
        </article>

        <article class="box-typical profile-post">
            <div class="profile-post-header">
                <div class="user-card-row">
                    <div class="tbl-row">
                        <div class="tbl-cell tbl-cell-photo">
                            <a href="#">
                                <img src="img/photo-64-2.jpg" alt="">
                            </a>
                        </div>
                        <div class="tbl-cell">
                            <div class="user-card-row-name"><a href="#">Tim Collins</a></div>
                            <div class="color-blue-grey-lighter">3 days ago - 23 min read</div>
                        </div>
                    </div>
                </div>
                <a href="#" class="shared">
                    <i class="font-icon font-icon-share"></i>
                </a>
            </div>
            <div class="profile-post-content">
                <p class="profile-post-content-note">Added a new video</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                <div class="cstm-video-player" style="background-image: url('img/player-photo-b.jpg');">
                    <div class="cstm-video-player-hover">
                        <i class="font-icon font-icon-play"></i>
                    </div>
                    <div class="cstm-video-player-controls">
                        <div class="cstm-video-player-progress">
                            <div class="downloaded" style="width:75%"></div>
                            <div class="played" style="width:35%"></div>
                        </div>
                        <div class="cstm-video-player-controls-left">
                            <button type="button" class="cstm-video-player-btn">
                                <i class="font-icon font-icon-play"></i>
                            </button>
                            <button type="button" class="cstm-video-player-btn">
                                <i class="font-icon font-icon-player-next"></i>
                            </button>
                            <button type="button" class="cstm-video-player-btn">
                                <i class="font-icon font-icon-player-sound"></i>
                            </button>
                        </div>
                        <div class="cstm-video-player-controls-right">
                            <button type="button" class="cstm-video-player-btn">
                                <i class="font-icon font-icon-player-subtitres"></i>
                            </button>
                            <button type="button" class="cstm-video-player-btn">
                                <i class="font-icon font-icon-player-settings"></i>
                            </button>
                            <button type="button" class="cstm-video-player-btn">
                                <i class="font-icon font-icon-player-wide-screen"></i>
                            </button>
                            <button type="button" class="cstm-video-player-btn">
                                <i class="font-icon font-icon-player-full-screen"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-typical-footer profile-post-meta">
                <a href="#" class="meta-item">
                    <i class="font-icon font-icon-heart"></i>
                    45 Like
                </a>
                <a href="#" class="meta-item">
                    <i class="font-icon font-icon-comment"></i>
                    18 Comment
                </a>
            </div>
        </article>

        <article class="box-typical profile-post">
            <div class="profile-post-header">
                <div class="user-card-row">
                    <div class="tbl-row">
                        <div class="tbl-cell tbl-cell-photo">
                            <a href="#">
                                <img src="img/photo-64-2.jpg" alt="">
                            </a>
                        </div>
                        <div class="tbl-cell">
                            <div class="user-card-row-name"><a href="#">Tim Collins</a></div>
                            <div class="color-blue-grey-lighter">3 days ago - 23 min read</div>
                        </div>
                    </div>
                </div>
                <a href="#" class="shared">
                    <i class="font-icon font-icon-share"></i>
                </a>
            </div>
            <div class="profile-post-content">
                <p class="profile-post-content-note">Is listening to the Evernote</p>
                <div class="minimalistic-player">
                    <div class="tbl minimalistic-player-header">
                        <div class="tbl-row">
                            <div class="tbl-cell tbl-cell-action">
                                <button type="button">
                                    <i class="font-icon font-icon-play-square"></i>
                                </button>
                            </div>
                            <div class="tbl-cell tbl-cell-action">
                                <button type="button">
                                    <i class="font-icon font-icon-play-prev-square"></i>
                                </button>
                            </div>
                            <div class="tbl-cell tbl-cell-action">
                                <button type="button">
                                    <i class="font-icon font-icon-play-next-square"></i>
                                </button>
                            </div>
                            <div class="tbl-cell tbl-cell-caption">Kylie Minogue  – Slow 2015</div>
                            <div class="tbl-cell tbl-cell-time">-04:01</div>
                        </div>
                    </div>
                    <div class="progress">
                        <div style="width: 25%"></div>
                    </div>
                    <div class="progress sound">
                        <div style="width: 50%"><div class="handle"></div></div>
                    </div>
                </div>
            </div>
            <div class="box-typical-footer profile-post-meta">
                <a href="#" class="meta-item">
                    <i class="font-icon font-icon-heart"></i>
                    45 Like
                </a>
                <a href="#" class="meta-item">
                    <i class="font-icon font-icon-comment"></i>
                    18 Comment
                </a>
            </div>
        </article>

        <article class="box-typical profile-post">
            <div class="profile-post-header">
                <div class="user-card-row">
                    <div class="tbl-row">
                        <div class="tbl-cell tbl-cell-photo">
                            <a href="#">
                                <img src="img/photo-64-2.jpg" alt="">
                            </a>
                        </div>
                        <div class="tbl-cell">
                            <div class="user-card-row-name"><a href="#">Tim Collins</a></div>
                            <div class="color-blue-grey-lighter">3 days ago - 23 min read</div>
                        </div>
                    </div>
                </div>
                <a href="#" class="shared">
                    <i class="font-icon font-icon-share"></i>
                </a>
            </div>
            <div class="profile-post-content">
                <p class="profile-post-content-note">Created an album collection</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                <div class="profile-post-gall-fluid profile-post-gall-grid" data-columns>
                    <div class="col">
                        <a class="fancybox" rel="gall-2" href="img/gall-img-1.jpg">
                            <img src="img/gall-img-1.jpg" alt="">
                        </a>
                    </div>
                    <div class="col">
                        <a class="fancybox" rel="gall-2" href="img/gall-img-2.jpg">
                            <img src="img/gall-img-2.jpg" alt="">
                        </a>
                    </div>
                    <div class="col">
                        <a class="fancybox" rel="gall-2" href="img/gall-img-3.jpg">
                            <img src="img/gall-img-3.jpg" alt="">
                        </a>
                    </div>
                    <div class="col">
                        <a class="fancybox" rel="gall-2" href="img/gall-img-4.jpg">
                            <img src="img/gall-img-4.jpg" alt="">
                        </a>
                    </div>
                    <div class="col">
                        <a class="fancybox" rel="gall-2" href="img/gall-img-5.jpg">
                            <img src="img/gall-img-5.jpg" alt="">
                        </a>
                    </div>
                    <div class="col">
                        <a class="fancybox" rel="gall-2" href="img/gall-img-6.jpg">
                            <img src="img/gall-img-6.jpg" alt="">
                        </a>
                    </div>
                    <div class="col">
                        <a class="fancybox" rel="gall-2" href="img/gall-img-7.jpg">
                            <img src="img/gall-img-7.jpg" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="box-typical-footer profile-post-meta">
                <a href="#" class="meta-item">
                    <i class="font-icon font-icon-heart"></i>
                    45 Like
                </a>
                <a href="#" class="meta-item">
                    <i class="font-icon font-icon-comment"></i>
                    18 Comment
                </a>
            </div>
        </article>

        <article class="box-typical profile-post">
            <div class="profile-post-header">
                <div class="user-card-row">
                    <div class="tbl-row">
                        <div class="tbl-cell tbl-cell-photo">
                            <a href="#">
                                <img src="img/photo-64-2.jpg" alt="">
                            </a>
                        </div>
                        <div class="tbl-cell">
                            <div class="user-card-row-name"><a href="#">Tim Collins</a></div>
                            <div class="color-blue-grey-lighter">3 days ago - 23 min read</div>
                        </div>
                    </div>
                </div>
                <a href="#" class="shared">
                    <i class="font-icon font-icon-share"></i>
                </a>
            </div>
            <div class="profile-post-content">
                <p class="profile-post-content-note">Scheduled a meeting whith <a href="#">Elen Adarna</a></p>
                <div class="tbl profile-post-card">
                    <div class="tbl-row">
                        <div class="tbl-cell tbl-cell-photo">
                            <a href="#">
                                <img src="img/100x100.jpg" alt="">
                            </a>
                        </div>
                        <div class="tbl-cell">
                            <p class="title"><a href="#">Telling Your Kife Story: Memoir Workshop Series</a></p>
                            <p>Monday, July 06, 2015 – Thuesday, July 07, 2015</p>
                            <p>SF Bay Theater</p>
                            <p>San Francisco, California, USA</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-typical-footer profile-post-meta">
                <a href="#" class="meta-item">
                    <i class="font-icon font-icon-heart"></i>
                    45 Like
                </a>
                <a href="#" class="meta-item">
                    <i class="font-icon font-icon-comment"></i>
                    18 Comment
                </a>
            </div>
        </article>
    </div><!--.tab-pane-->
@endsection

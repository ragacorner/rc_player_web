   
<div class="row">

    <!-- <input type="hidden" name="status" value="inactivated"> -->
    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
    <input type="hidden" name="status" value="">

    <div class="col-md-12">
        <!-- PHOTO -->
        <div class="form-group row">
            <label class="col-lg-4 form-label"><small class="">Foto</small></label>
            <div class="col-lg-7">
                <!-- <img src="" alt="" class=""> -->
                <div class="form-control-wrapper form-control-icon-left">
                    {!! Form::file('photo', null, ['class'=>'form-control']) !!}
                </div>
                @if ($errors->has('photo'))
                    <small class="text-danger">{{ $errors->first('photo') }}</small>
                @endif
            </div>
        </div>

        <!-- NAMA -->
        <div class="form-group row">
            <label class="col-lg-4 form-label"><small class="">Nama Lengkap</small></label>
            <div class="col-lg-7">
                {!! Form::text('name', Auth::user()->name, ['class'=>'form-control','placeholder'=>'']) !!}
                
                @if ($errors->has('name'))
                    <small class="text-danger">{{ $errors->first('name') }}</small>
                @endif
            </div>
            <div class="col-lg-1">
                
            </div>
        </div>

        <!-- NAMA -->
        <div class="form-group row">
            <label class="col-lg-4 form-label"><small class="">Nama Unik</small></label>
            <div class="col-lg-7">
                {!! Form::text('username', $player->profile->username, ['class'=>'form-control','placeholder'=>'Nama unik, tidak boleh ada yang sama']) !!}
                
                @if ($errors->has('username'))
                    <small class="text-danger">{{ $errors->first('username') }}</small>
                @endif
            </div>
            <div class="col-lg-1">
                
            </div>
        </div>      

        <!-- PEKERJAAN -->
        <div class="form-group row">
            <label class="col-lg-4 form-label"><small class="">Pekerjaan</small></label>
            <div class="col-lg-7">
                <select name="occupation" class="select2" required>
                    <option value="">-- Pilih Pekerjaan --</option>
                    @if($player->profile->occupation == 'smp') @endif
                    <option value="smp" {{$player->profile->occupation == 'smp' ? 'selected':''}}>Pelajar SMP</option>
                    <option value="sma" {{$player->profile->occupation == 'sma' ? 'selected':''}}>Pelajar SMA</option>
                    <option value="mahasiswa" {{$player->profile->occupation == 'mahasiswa' ? 'selected':''}}>Mahasiswa</option>
                    <option value="karyawan" {{$player->profile->occupation == 'karyawan' ? 'selected':''}}>Karyawan</option>
                    <option value="wiraswasta" {{$player->profile->occupation == 'wiraswasta' ? 'selected':''}}>Wiraswasta</option>
                </select>
                @if ($errors->has('occupation'))
                    <small class="text-danger">{{ $errors->first('occupation') }}</small>
                @endif
            </div>
            <div class="col-lg-1">
                <a href="#"
                    class=""
                    title="Kenapa mengisi Pekerjaan?"
                    data-container="body"
                    data-toggle="popover"
                    data-placement="right"
                    data-content="Agar kami dapat membantu kamu untuk tetap rutin berolahraga sesuai pekerjaan kamu.">
                    <i class="font-icon-question"></i>
                </a>
            </div>
        </div>

        <div class="form-group {{ $errors->has('phone')? 'form-group-error':''}} row">
        <label class="col-sm-4 form-control-label"><small class="">No. HP</small></label>
        @if ($errors->has('phone'))
            <div class="col-sm-7">
                {!! Form::email('phone', null, ['class'=>'form-control form-control-red-fill','id'=>'phone-with-code-area-mask-input', 'placeholder'=>'Masukan nomor handphone', '']) !!}
                <small class="text-muted">{{ $errors->first('phone') }}</small>
            </div>
        @else
            <div class="col-sm-7">
                {!! Form::text('phone', $player->phone, ['class'=>'form-control','id'=>'phone-with-code-area-mask-input', 'required']) !!}
            </div>
        @endif
    </div>

        <!-- ALAMAT -->
        <div class="form-group row">
            <label class="col-lg-4 form-label">
                <small class="">Alamat Domisili</small>
            </label>
            <div class="col-lg-7">
                <select name="regency_id" class="select2" required>
                    <option value="{{$player->profile->regency_id}}" selected>{{$player->profile->regency->display_name}}</option>
                    
                    @foreach($composer_provincies as $prov)
                        <optgroup label="{{$prov->display_name}}">
                            
                            @foreach($composer_regencies->where('province_id', $prov->id)->pluck('display_name','id') as $rgc => $value)
                                @if(Request::segment(2) == 'profile')
                                    @if($rgc == 171)
                                        <option value="{{ $rgc }}">{{ $value }}</option>
                                    @else
                                        <option value="{{ $rgc }}">{{ $value }}</option>
                                    @endif
                                @else
                                    @if($rgc == 171)
                                        <option value="{{ $rgc }}" selected>{{ $value }}</option>
                                    @else
                                        <option value="{{ $rgc }}" selected>{{ $value }}</option>
                                    @endif
                                @endif
                            @endforeach

                        </optgroup>
                    @endforeach
                </select>
                @if ($errors->has('regency_id'))
                    <small class="text-danger">{{ $errors->first('regency_id') }}</small>
                @endif
            </div>
            <div class="col-lg-1">
                <a href="#"
                    class=""
                    title="Kenapa mengisi Alamat?"
                    data-container="body"
                    data-toggle="popover"
                    data-placement="right"
                    data-content="Agar kami tunjukkan dan berikan info lapangan yang tersedia didaerah kamu">
                    <i class="font-icon-question"></i>
                </a>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-lg-4 form-label"></label>
            <div class="col-lg-7">
                <select name="subdistrict_id" class="select2" required>
                    @foreach($composer_subdistricts->where('regency_id', $profile->regency_id) as $sub)
                        @if($sub->id == $profile->subdistrict_id)
                            <option value="{{ $sub->id }}" selected>{{ $sub->display_name }}</option>
                        @else
                            <option value="{{ $sub->id }}">{{ $sub->display_name }}</option>
                        @endif
                    @endforeach
                </select>
                @if ($errors->has('subdistrict_id'))
                    <small class="text-danger">{{ $errors->first('subdistrict_id') }}</small>
                @endif
            </div>
            <div class="col-lg-1">
                
            </div>
        </div>
        <div class="form-group row">
            <label class="col-lg-4 form-label"></label>
            <div class="col-lg-7">
                {!! Form::text('area', $profile->area, ['class'=>'form-control','placeholder'=>'Nama Daerah']) !!}
                @if ($errors->has('area'))
                    <small class="text-danger">{{ $errors->first('area') }}</small>
                @else
                    <!-- <small class="text-muted">Nama Daerah</small> -->
                @endif
            </div>
            <div class="col-lg-1">
                
            </div>
        </div>
        
        <div class="form-group row">
            <div class="col-md-4"></div>
            <div class="col-md-7">
                <button type="submit" name="button" class="btn btn-success btn-block">
                    <!-- <span class="font-icon glyphicon glyphicon-floppy-save"></span>  -->
                    Selanjutnya
                </button>
            </div>
        </div>
    </div>
</div>

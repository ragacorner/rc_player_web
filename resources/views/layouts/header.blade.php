<header class="site-header">
    <div class="container-fluid">

        <a href="{{ url('/') }}" class="site-logo">
            <img class="hidden-md-down" src="{{ asset('img/others/ragacorner_md.png') }}" alt="">
            <img class="hidden-lg-up" src="{{ asset('img/others/ragacorner_sm.png') }}" alt="">
        </a>

        @guest
            <div class="site-header-content">
                <div class="site-header-content-in">
                    <div class="site-header-shown">
                        <div class="dropdown user-menu">
                            @if(Request::path() == '/')
                                <a href="{{ url('/login') }}" class="btn btn-sm btn-success-outline">
                                    <small><span class="font-icon glyphicon glyphicon-log-in"></span></small> Masuk
                                </a>
                                <a href="{{ url('/register') }}" class="btn btn-sm btn-success">
                                    <small><span class="font-icon glyphicon glyphicon-plus-sign"></span></small> Daftar
                                </a>
                            @elseif(Request::path() == 'login')
                                <a href="{{ url('/') }}" class="btn btn-sm btn-primary-outline">
                                    <small><span class="font-icon glyphicon glyphicon-calendar"></span></small> Booking
                                </a>
                                <a href="{{ url('/register') }}" class="btn btn-sm btn-success">
                                    <small><span class="font-icon glyphicon glyphicon-plus-sign"></span></small> Daftar
                                </a>
                            @else
                                <a href="{{ url('/') }}" class="btn btn-sm btn-primary-outline">
                                    <small><span class="font-icon glyphicon glyphicon-calendar"></span></small> Booking
                                </a>
                                <a href="{{ url('/login') }}" class="btn btn-sm btn-success-outline">
                                    <small><span class="font-icon glyphicon glyphicon-log-in"></span></small> Masuk
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="site-header-content">
                <div class="site-header-content-in">
                    <div class="site-header-shown">
                        <div class="dropdown user-menu dropdown-notification notif">
                            <ul class="nav nav-pills">
                                @if(Auth::user()->has_setup == 'false')
                                    <li class="nav-item">
                                        <a 
                                            class="btn btn-nav btn-rounded btn-info-outline" 
                                            href="#">
                                            <span class="font-icon glyphicon glyphicon-user"></span>
                                            Profile
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                        
                        <div class="dropdown user-menu">
                            <button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if(empty(Auth::user()->profile->photo))
                                    <img src="{{ asset('img/uistart/avatar-2-64.png') }}" alt="">
                                @else
                                    <img src="{{ asset('img/player/500x500/'.Auth::user()->profile->username.'.jpg') }}" alt="">
                                @endif
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">

                                @if(Auth::user()->has_setup == 'true')
                                    <a class="dropdown-item" href="{{ route('player.profile.show', Auth::user()->profile->username) }}"><span class="font-icon glyphicon glyphicon-user"></span>{{ Auth::user()->name }}</a>
                                    <!-- <a class="dropdown-item" href="#"><span class="font-icon glyphicon glyphicon-question-sign"></span>Help</a> -->
                                    <div class="dropdown-divider"></div>
                                @else
                                    <a class="dropdown-item" href="#"><span class="font-icon glyphicon glyphicon-user"></span>{{ Auth::user()->email }}</a>
                                    <div class="dropdown-divider"></div>
                                @endif
                                
                                <a  class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                                    
                                    <span class="font-icon glyphicon glyphicon-log-out"></span>Logout
                                </a>

                                <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                                
                            </div>
                        </div>

                        @if(Auth::user()->has_setup == 'true')
                            <button type="button" class="burger-right">
                                <i class="font-icon-menu-addl"></i>
                            </button>
                        @endif
                    </div>
                    @if(Auth::user()->has_setup == 'true')
                        <div class="mobile-menu-right-overlay"></div>
                        <div class="site-header-collapsed">
                            <div class="site-header-collapsed-in">
                                <div class="dropdown">
                                    <a href="{{ url('/') }}" class="btn btn-nav btn-info-outline btn-rounded">
                                        <span class="font-icon glyphicon glyphicon-calendar"></span> Booking
                                    </a>
                                    <!-- <a class="btn btn-nav btn-rounded btn-info-outline" href="{{ url('/') }}">Booking</a> -->
                                    @php
                                        $bookings = App\Models\Booking::where('player_id', Auth::id())->where('played','false')->get()->sortByDesc('updated_at');
                                    @endphp
                                    <a 
                                        class="btn btn-nav btn-info-outline btn-rounded" 
                                        href="{{ route('player.games.index', Auth::user()->profile->username) }}">
                                        <span class="font-icon glyphicon glyphicon-fire"></span> My Games

                                        @if($bookings->count() > 0)
                                        <span class="label label-pill label-danger">{{ $bookings->count() }}</span>
                                        @endif
                                    </a>
                                </div>
                                <!-- <div class="site-header-search-container">
                                    <form class="site-header-search closed">
                                        <input type="text" placeholder="Search"/>
                                        <button type="submit">
                                            <span class="font-icon-search"></span>
                                        </button>
                                        <div class="overlay"></div>
                                    </form>
                                </div> -->
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        @endguest
    </div>
</header>
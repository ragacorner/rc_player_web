
<b>{{ $jam_diminta->format('H:i') == '09:00' ? '9 Pagi':'' }}</b>
<b>{{ $jam_diminta->format('H:i') == '10:00' ? '10 Pagi':'' }}</b>
<b>{{ $jam_diminta->format('H:i') == '11:00' ? '11 Siang':'' }}</b>
<b>{{ $jam_diminta->format('H:i') == '12:00' ? '12 Siang':'' }}</b>
<b>{{ $jam_diminta->format('H:i') == '13:00' ? '1 Siang':'' }}</b>
<b>{{ $jam_diminta->format('H:i') == '14:00' ? '2 Siang':'' }}</b>
<b>{{ $jam_diminta->format('H:i') == '15:00' ? '3 Sore':'' }}</b>
<b>{{ $jam_diminta->format('H:i') == '16:00' ? '4 Sore':'' }}</b>
<b>{{ $jam_diminta->format('H:i') == '17:00' ? '5 Sore':'' }}</b>
<b>{{ $jam_diminta->format('H:i') == '18:00' ? '6 Malam':'' }}</b>
<b>{{ $jam_diminta->format('H:i') == '19:00' ? '7 Malam':'' }}</b>
<b>{{ $jam_diminta->format('H:i') == '20:00' ? '8 Malam':'' }}</b>
<b>{{ $jam_diminta->format('H:i') == '21:00' ? '9 Malam':'' }}</b>
<b>{{ $jam_diminta->format('H:i') == '22:00' ? '10 Malam':'' }}</b>
<b>{{ $jam_diminta->format('H:i') == '23:00' ? '11 Malam':'' }}</b>

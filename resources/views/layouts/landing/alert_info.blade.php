@if(Auth::guest() && Request::path() == '/')

    @guest
    <div class="row">
        <div class="col-md-12">
        <a href="{{ url('http://place.ragacorner.dvlp/') }}" class="">
            <div class="ribbon-block round relative text-center with-image">
                <div class="background-image"  style="background-image: url(img/others/rumput_crop.jpg)"><!-- --></div>
                <i class="block-icon fa fa-home"></i>
                <span class="title">
                    <strong>
                        Anda Punya Lapangan Olahraga? Gabung Bersama Ragacorner Yuk 
                        
                    </strong>
                </span>
            </div>
            <div class="m-b-lg"></div>
        </a>
        </div>
        <!-- <div class="col-md-6">
            <div class="ribbon-block round relative text-center with-image">
                <div class="background-image"  style="background-image: url(img/others/team.jpg)"></div>
                <i class="block-icon fa fa-users"></i>
                <span class="title">
                    <strong>
                        Daftarkan Team Profesional Anda, dan Biarkan Kami Mengurus Sisanya 
                        <a href="{{ url('http://club.ragacorner.dvlp/login') }}" class="btn btn-primary btn-sm">Daftar</a>
                    </strong>
                </span>
            </div>
            <div class="m-b-lg"></div>
        </div> -->
    </div>
    @else
        <div class="alert alert-twitter alert-close alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <i class="font-icon font-icon-alarm"></i>
            <strong>Pemberitahuan</strong>
            <p>
                <ol>
                    <li>
                        Jika anda pemilik tempat futsal, daftarkan tempat futsal anda segera!!!
                        <a href="{{ route('owner.register.create') }}" class="btn btn-warning btn-sm">Register Owner</a>
                    </li>
                    <li>Jika kamu seorang pemain, kamu bisa login untuk bisa memesan tempat langganan kamu bermain secara langsung.</li>
                </ol>
            </p>
        </div>
        <div class="m-b-lg"></div>
    @endguest

@endif
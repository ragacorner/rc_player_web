
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	
	@if(Request::path() == '/')
		<title>Ragacorner - Tempat Pemesanan Lapangan Olahraga</title>
	@else
		<title>@yield('title') Ragacorner</title>
	@endif

	<link href="{{ asset('img/favicon.144x144.png') }}" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="{{ asset('img/favicon.114x114.png') }}" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="{{ asset('img/favicon.72x72.png') }}" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="{{ asset('img/favicon.57x57.png') }}" rel="apple-touch-icon" type="image/png">
	<link href="{{ asset('favicon.ico') }}" rel="shortcut icon">

	@yield('css')
	
    <!-- FORM CSS - START -->
	<link rel="stylesheet" href="{{ asset('css/separate/vendor/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/separate/vendor/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/separate/vendor/datepicker.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/separate/vendor/bootstrap-touchspin.min.css') }}">
    
    <!-- ALERT NOTIFICATION CSS - START -->
	<!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css"> -->
    
    <!-- CONTENT CSS - START -->
    <link rel="stylesheet" href="{{ asset('css/separate/pages/user.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/separate/pages/contacts.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/separate/pages/widgets.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/separate/pages/profile-2.min.css') }}">
    
	<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" /> -->
	<!-- <link rel="stylesheet" href="{{ asset('css/separate/pages/files.min.css') }}"> -->
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css"> -->
	<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" /> -->

	<style>
        .widget-triger {
            margin: 0 0 0px;
        }
        img {
            max-width: 100%;
            max-height: 100%;
        }

        .image-size {
            height: 160px;
            /* width: 30px; */
        }
	</style>

    <!-- MAIN CSS - START -->
	<link rel="stylesheet" href="{{ asset('css/lib/font-awesome/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/lib/bootstrap/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/main.css') }}">
</head>

@if(Auth::guest() || Auth::user()->is_booker == 'setup')
	<body>
@else
	<body class="">
@endif

	@include('layouts.header')

	<div class="page-content">
	    <div class="container-fluid">
			
			@if(Request::path() == '/')
				@include('layouts.flash_message')
				@yield('content')
			@else
				@if(Auth::guest() || Auth::user()->has_setup == 'true')
					@if(Request::segment(1) == 'search' || Request::segment(1) == 'places')
						@include('searching.inc.triger_v2')
						@include('layouts.flash_message')
					@else
					@endif
				@endif

				@yield('content')
			@endif
			
	    </div>
	</div>
	
	{{--@include('layouts.footer')--}}

    <!-- MAIN JS - START -->
	<script src="{{ asset('js/lib/jquery/jquery.min.js') }}"></script>
	<script src="{{ asset('js/lib/tether/tether.min.js') }}"></script>
	<script src="{{ asset('js/lib/bootstrap/bootstrap.min.js') }}"></script>
	<script src="{{ asset('js/plugins.js') }}"></script>
    
    <!-- ALERT NOTIFICATION JS - START -->
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script src="{{ asset('js/lib/notie/notie.js') }}"></script>
	@include('sweet::alert')

    <!-- FORM JS - START -->
	<script src="{{ asset('js/lib/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('js/lib/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
	<script src="{{ asset('js/lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>

	@yield('js')

	<script>
        $(document).ready(function() {
            $('#datePicker').datepicker({
                    format: 'dd-mm-yyyy',
                    autoclose: true,
                    todayHighlight: true,
            });
            
            $("input[id='duration']").TouchSpin({
                buttondown_class: "btn btn-default-outline",
                buttonup_class: "btn btn-default-outline"
            });
        });

		@if(Session::has('success'))
			$('#notie-success').ready(function() {
					notie.alert(1, "{{ Session::get('success') }}", 4);
			});

			@php
				Session::forget('success');
			@endphp
		@endif

		@if(Session::has('info'))
			$('#notie-info').ready(function() {
					notie.alert(4, "{{ Session::get('info') }}", 4);
			});

			@php
				Session::forget('info');
			@endphp
		@endif

		@if(Session::has('warning'))
			$('#notie-warning').ready(function() {
					notie.alert(2, "{{ Session::get('warning') }}", 4);
			});

			@php
				Session::forget('warning');
			@endphp
		@endif

		@if(Session::has('error'))
			$('#notie-error').ready(function() {
					notie.alert(3, "{{ Session::get('error') }}", 4);
			});

			@php
				Session::forget('error');
			@endphp
		@endif
	</script>

    <!-- MAIN JS - START -->
	<script src="{{ asset('js/app.js') }}"></script>

    <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script> -->

</body>
</html>
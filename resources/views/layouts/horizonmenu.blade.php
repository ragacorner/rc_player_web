<!-- <div class="mobile-menu-left-overlay"></div> -->
<ul class="text-center main-nav nav nav-inline">
		<li class="nav-item">
			<a class="nav-link {{ Request::path() == '/' ? 'active':'' }}" href="{{ route('searching.landing') }}">Home</a>
		</li>
		<li class="nav-item">
			<a class="nav-link {{ Request::segment(1) == 'activities' ? 'active':'' }}" href="{{ route('activity.index') }}">Activities</a>
		</li>
		<li class="nav-item dropdown">
			<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Setting</a>
			<div class="dropdown-menu">
				<a class="dropdown-item" href="#">Action</a>
				<a class="dropdown-item" href="#">Another action</a>
				<a class="dropdown-item" href="#">Something else here</a>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="#">Separated link</a>
			</div>
		</li>
		<!-- <li class="nav-item">
			<a data-toggle="collapse"
				data-parent="#accordion"
				href="#triger"
				aria-expanded="true"
				aria-controls="collapseOne"
				class="btn btn-sm btn-warning">
				<i class="font-icon font-icon-search"></i>
				Cari
			</a>
		</li> -->
	</ul>
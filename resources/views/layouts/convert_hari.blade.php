<b>{{ $hari_diminta->format('l') == 'Wednesday' ? ucwords('hari rabu'):'' }}</b>
<b>{{ $hari_diminta->format('l') == 'Thursday' ? ucwords('hari kamis'):'' }}</b>
<b>{{ $hari_diminta->format('l') == 'Friday' ? ucwords('hari jumat'):'' }}</b>
<b>{{ $hari_diminta->format('l') == 'Saturday' ? ucwords('hari sabtu'):'' }}</b>
<b>{{ $hari_diminta->format('l') == 'Sunday' ? ucwords('hari minggu'):'' }}</b>
<b>{{ $hari_diminta->format('l') == 'Monday' ? ucwords('hari senin'):'' }}</b>
<b>{{ $hari_diminta->format('l') == 'Tuesday' ? ucwords('hari selasa'):'' }}</b>
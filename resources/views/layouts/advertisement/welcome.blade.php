<!-- <hr>
<h3>Kenapa Bergabung Bersama Kami?</h3> -->
<div class="row">
    <div class="col-xl-3">
        <div class="ribbon-block round relative text-center with-bg">
            <!-- <div class="background-image"  style="background-image: url(img/others/atlit1.jpg)"></div> -->
            <i class="block-icon fa fa-clock-o"></i>
            <span class="title">
                <strong>Booking Kapanpun Kamu Mau</strong>
            </span>
        </div>
    </div><!--.col-->
    <div class="col-xl-3">
        <div class="ribbon-block round relative text-center with-bg">
            <!-- <div class="background-image"  style="background-image: url(img/others/atlit1.jpg)"></div> -->
            <i class="block-icon fa fa-map"></i>
            <span class="title">
                <strong>Booking Dimanapun Kamu Berada</strong>
            </span>
        </div>
    </div><!--.col-->
    <div class="col-xl-3">
        <div class="ribbon-block round relative text-center with-bg">
            <!-- <div class="background-image"  style="background-image: url(img/others/atlit1.jpg)"></div> -->
            <i class="block-icon fa fa-exchange"></i>
            <span class="title">
                <strong>Temukan Lawan Sparing Didaerahmu</strong>
            </span>
        </div>
    </div>
    <div class="col-xl-3">
        <!-- <div class="ribbon-block round relative text-center with-bg">
            <div class="background-image"  style="background-image: url(img/others/atlit1.jpg)"></div>
            <i class="block-icon fa fa-user"></i>
            <span class="title">
                <strong>Atur Olahragamu Sesuai Aktivitasmu</strong>
            </span>
        </div> -->
        <div class="ribbon-block round relative text-center with-image">
                <div class="background-image"  style="background-image: url(img/others/rumput_crop.jpg)"><!-- --></div>
                <i class="block-icon fa fa-home"></i>
                <span class="title">
                    <strong>
                        Anda Punya Lapangan Olahraga? Gabung Yuk Bersama Kami 
                        <a href="{{ url('http://place.ragacorner.dvlp/') }}" class="btn btn-warning btn-sm">Gabung</a>
                    </strong>
                </span>
            </div>
    </div><!--.col-->
</div>

<!-- <div class="row">
    <div class="col-xl-3">
        <div class="ribbon-block round relative text-center with-image">
            <div class="background-image"  style="background-image: url(img/others/atlit1.jpg)"></div>
            <i class="block-icon fa fa-home"></i>
            <span class="title">
                <strong>Pesan Lapangan Ga Pake Ribet</strong>
            </span>
        </div>
    </div><
    <div class="col-xl-6">
        <div class="ribbon-block round relative text-center with-image">
            <div class="background-image"  style="background-image: url(img/others/atlit1.jpg)"></div>
            <i class="block-icon fa fa-exchange"></i>
            <span class="title">
                <strong>Lawan Sparing Yang Selalu Menanti Kamu</strong>
            </span>
        </div>
    </div>
    <div class="col-xl-3">
        <div class="ribbon-block round relative text-center with-image">
            <div class="background-image"  style="background-image: url(img/others/atlit1.jpg)"></div>
            <i class="block-icon fa fa-users"></i>
            <span class="title">
                <strong>Mudahnya Menjadi Team Profesional</strong>
            </span>
        </div>
    </div>
</div> -->

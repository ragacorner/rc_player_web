<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>StartUI - Premium Bootstrap 4 Admin Dashboard Template</title>

	<link href="img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.png" rel="icon" type="image/png">
	<link href="img/favicon.ico" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    <link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="with-side-menu">

	@include('layouts.header')

	<div class="mobile-menu-left-overlay"></div>
	<nav class="side-menu">
		<section>
			<header class="side-menu-title">Documentation</header>
			<ul class="side-table-of-contents">
				<li><a href="#">Index page</a>
					<ul>
						<li><a href="#">Getting Started</a></li>
						<li><a href="#" class="active">Creating a new project</a></li>
						<li><a href="#">Adding customers</a></li>
						<li><a href="#">Settings</a></li>
						<li><a href="#">Importing data</a></li>
						<li><a href="#">Exporting data</a></li>
					</ul>
				</li>
				<li><a href="#">Getting Started</a>
					<ul>
						<li><a href="#">Installing on OSX</a></li>
						<li><a href="#">Installing on Windows</a></li>
						<li><a href="#">Working on the SuperDocs cloud</a></li>
						<li><a href="#">Troubleshooting</a></li>
					</ul>
				</li>
				<li><a href="#">Creating New Projects</a></li>
				<li><a href="#">Managing Users</a></li>
				<li><a href="#">Authors, Editors &amp; Permissions</a></li>
			</ul>
		</section>
	</nav><!--.side-menu-->

	<div class="page-content">
		<div class="container-fluid">
			@yield('content')
		</div><!--.container-fluid-->
	</div><!--.page-content-->

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

<script src="js/app.js"></script>
</body>
</html>
@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/separate/vendor/slick.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/separate/pages/prices.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/separate/pages/profile-2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/separate/pages/user.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/separate/vendor/bootstrap-touchspin.min.css') }}">
@endsection

@section('content')
    
    <div class="p-t-md">
        @include('searching.inc.alert')
    </div>

    <div class="row">
            
        <div class="col-md-3">
            @include('searching.inc.place.placeinfo')
        </div>
        <div class="col-md-6">

            @include('searching.inc.place.triger_v3')
            
            @foreach($fields as $field)
                @include('searching.inc.place.list')
            @endforeach
            @foreach($booked_fields as $booked)
                @include('searching.inc.place.bookedlist')
            @endforeach

        </div>
        <div class="col-md-3">
            {{-- @include('searching.inc.place.suggestion') --}}
        </div>
    </div>

@endsection

@section('js')

<script src="{{ asset('js/lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>
<script>
		$(document).ready(function(){
			$("input[name='durasi']").TouchSpin({
				buttondown_class: "btn btn-default-outline",
				buttonup_class: "btn btn-default-outline"
			});
		});
	</script>

@endsection
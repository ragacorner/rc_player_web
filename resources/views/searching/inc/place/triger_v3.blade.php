
<div class="card">
    <div class="card-block">
        <!-- <div class="m-t-lg"></div> -->
        <form method="get" action="{{ route('searching.place', $place->slug) }}" id="form-comment" class="">
            @csrf

            {!! Form::hidden('regency', $place->regency->name) !!}
            <div class="row">
                <div class="col-md-4">
                    <fieldset class="form-group {{ $errors->has('jam') ? 'form-group-error' : '' }}">
                        <div class="form-control-wrapper">
                            
                            <select class="select2-photo" name="jam">
                                @for($jam = 9; $jam <= 23; $jam++)
                                    @if($jam == $request_jam)
                                        @if($jam < 10)
                                            <option data-photo="{{ asset('img/others/jam/'.$jam.'.jpg') }}" value="{{$jam}}" selected>{{ '0'.$jam.':00' }}</option>
                                        @else
                                            <option data-photo="{{ asset('img/others/jam/'.$jam.'.jpg') }}" value="{{$jam}}" selected>{{ $jam.':00' }}</option>
                                        @endif
                                    @else
                                        @if($jam < 10)
                                            <option data-photo="{{ asset('img/others/jam/'.$jam.'.jpg') }}" value="{{$jam}}">{{ '0'.$jam.':00' }}</option>
                                        @else
                                            <option data-photo="{{ asset('img/others/jam/'.$jam.'.jpg') }}" value="{{$jam}}">{{ $jam.':00' }}</option>
                                        @endif
                                    @endif
                                @endfor
                            </select>
                            @if ($errors->has('jam'))
                                <div class="form-tooltip-error">{{ $errors->first('jam') }}</div>
                            @endif
                        </div>
                        <small class="text-muted">Pilih Jam Bermain</small>
                    </fieldset>
                </div>
                <div class="col-md-4 date">
                    <div class="form-group">
                        <div class="form-control-wrapper">
                            <div class="input-group input-append date" id="datePicker2">
                                <!-- <input type="text" class="form-control" name="hari" value="{{ Carbon\Carbon::now('Asia/Jakarta')->format('d-m-Y') }}" required/> -->
                                <input type="text" class="form-control" name="hari" value="{{ $hari_diminta->format('d-m-Y')}}" required/>
                                <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                            @if ($errors->has('hari'))
                                <div class="form-tooltip-error">{{ $errors->first('hari') }}</div>
                            @endif
                        </div>
                        <small class="text-muted">Pilih tanggal bermain</small>
                    </div>
                </div>
                <div class="col-md-4">
                    <input type="submit" class="btn btn-success-outline btn-block" value="Cari Waktu">
                </div>
            </div>
        </form>
    </div>
    <!-- <header class="page-content-header m-b-0">   
        <section class="widget-triger widget-accordion no-border" id="accordion" role="tablist" aria-multiselectable="true">
            <div id="regency" class="panel-collapse collapse {{ Request::path() != 'search/reg' ? '':'in' }}" role="tabpanel" aria-labelledby="headingOne">
            </div>
        </section>
    </header> -->
</div>



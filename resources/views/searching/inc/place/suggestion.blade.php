<section class="box-typical">
    <header class="box-typical-header-sm">Saran Tempat</header>
        <div class="box-typical-body panel-body">
            <div class="contact-row-list">
                
                @foreach($places as $plc)
                    
                    @if($plc->id != $place->id)
                        <article class="contact-row">
                            <div class="user-card-row">
                                <div class="tbl-row">
                                    <div class="tbl-cell tbl-cell-photo">
                                            <!-- <img src="{{ asset('img/photo-64-2.jpg') }}" alt=""> -->
                                            @if(empty($plc->photo))
                                                <img src="{{ asset('img/place/building.jpg') }}" alt="">
                                            @else
                                                <img src="{{ asset('img/place/'.$place->photo) }}" alt="">
                                            @endif
                                        <a href="#">
                                        </a>
                                    </div>
                                    <div class="tbl-cell">
                                        <p class="user-card-row-name"><a href="{{route('place.show', $plc->slug)}}">{{ $plc->name }}</a></p>
                                        <p class="user-card-row-mail">{{ $plc->address }}</p>
                                    </div>
                                    <!-- <div class="tbl-cell tbl-cell-status">Director at Tony’s</div> -->
                                </div>
                            </div>
                        </article>
                    @endif
                @endforeach
            </div>
        </div>
    </section>
</section><!--.box-typical-->
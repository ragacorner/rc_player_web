<div class="form-group">
    <div class="form-control-wrapper">
        <label class="form-label"><strong>Tanggal</strong></label>
        <div class="input-group input-append date datepicker-orient-bottom" id="datePicker">

            @if(Request::path() == '/')
                @if(\Carbon\Carbon::now()->format('H:i:s') >=  '22:00:00')
                <input type="text" class="form-control" name="hari" value="{{ Carbon\Carbon::now('Asia/Jakarta')->addDay()->format('d-m-Y') }}" required/>
                @else
                <input type="text" class="form-control" name="hari" value="{{ Carbon\Carbon::now('Asia/Jakarta')->format('d-m-Y') }}" required/>
                @endif
            @else
                <input type="text" class="form-control" name="hari" value="{{ $hari_diminta->format('d-m-Y') }}" required/>
            @endif

            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
        </div>
        @if ($errors->has('hari'))
            <div class="form-tooltip-error">{{ $errors->first('hari') }}</div>
        @endif
    </div>
    <!-- <small class="text-muted">Pilih Tanggal Bermain</small> -->
</div>

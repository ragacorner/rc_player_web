<fieldset class="{{ $errors->has('category') ? 'form-group-error' : 'form-group ' }}">
    <div class="form-control-wrapper">
        <label class="form-label"><strong>Kategori</strong></label>
        <select class="select2 select2-photo" name="category" required>
            <!-- <option value=""></option> -->
            @guest
                @foreach($composer_categories as $cat)    
                    @php $count_place = $cat->places->count(); @endphp

                    @if($count_place > 0)
                        @if(Request::path() == '/' )
                            <option data-photo="{{ asset('img/others/categories/'.$cat->name.'.png') }}" value="{{ $cat->name }}">{{ $cat->display_name }}</option>
                        @else
                            @if($cat->name == $category->name)
                                <option data-photo="{{ asset('img/others/categories/'.$cat->name.'.png') }}" value="{{ $cat->name }}" selected>{{ $cat->display_name }}</option>
                            @else
                                <option data-photo="{{ asset('img/others/categories/'.$cat->name.'.png') }}" value="{{ $cat->name }}">{{ $cat->display_name }}</option>
                            @endif
                        @endif
                    @endif
                @endforeach
            @else
                @php 
                    $booked = \App\Models\Booking::all();
                    $booked_played = $booked
                                    ->where('player_id', Auth::id())
                                    ->where('played','false')
                                    ->pluck('category_id')
                                    ->all();
                    $book_history = $booked
                                    ->where('player_id', Auth::id())
                                    ->whereNotIn('id', $booked_played)
                                    ->sortByDesc('created_at')
                                    ->first();
                @endphp

                @foreach($composer_categories->whereNotIn('id', $booked_played) as $cat)
                    @php $count = $cat->places->count(); @endphp

                    @if($count > 0)
                        @if(count($booked_played) > 0)
                            @if(Request::path() == '/' )
                                @if($cat->name == $book_history->category->name)
                                    <option data-photo="{{ asset('img/others/categories/'.$cat->name.'.png') }}" value="{{ $cat->name }}" selected>{{ $cat->display_name }}</option>
                                @else
                                    <option data-photo="{{ asset('img/others/categories/'.$cat->name.'.png') }}" value="{{ $cat->name }}" selected>{{ $cat->display_name }}</option>
                                @endif
                            @else
                                @if($cat->name == $category->name)
                                    <option data-photo="{{ asset('img/others/categories/'.$cat->name.'.png') }}" value="{{ $cat->name }}" selected>{{ $cat->display_name }}</option>
                                @else
                                    <option data-photo="{{ asset('img/others/categories/'.$cat->name.'.png') }}" value="{{ $cat->name }}">{{ $cat->display_name }}</option>
                                @endif
                            @endif
                        @else
                            @if($cat->id == Auth::user()->profile->category_id)
                            <option data-photo="{{ asset('img/others/categories/'.$cat->name.'.png') }}" value="{{ $cat->name }}" selected>{{ $cat->display_name }}</option>
                            @else
                            <option data-photo="{{ asset('img/others/categories/'.$cat->name.'.png') }}" value="{{ $cat->name }}">{{ $cat->display_name }}</option>
                            @endif
                        @endif
                    @endif
                @endforeach

                @foreach($composer_categories->whereIn('id', $booked_played) as $cat)    
                    <option data-photo="{{ asset('img/others/categories/'.$cat->name.'.png') }}" value="{{ $cat->name }}" disabled>{{ $cat->display_name }} (Sudah Dipesan)</option>
                @endforeach
            @endguest
        </select>
        @if ($errors->has('category'))
            <div class="form-tooltip-error">{{ $errors->first('category') }}</div>
        @endif
    </div>
    <!-- <small class="text-muted">Pilih Jenis Olahraga</small> -->
</fieldset>
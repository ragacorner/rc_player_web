<div class="col-md-4"></div>
<div class="col-md-4">
    <section class="widget widget-accordion no-margin no-padding" id="accordion" role="tablist" aria-multiselectable="true">
        <article class="panel"> 
            <div id="choose_regency" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="no-margin">
                    <div class="{{ $errors->has('regency') ? 'form-group-error' : 'form-group' }}">
                        <div class="form-control-wrapper">
                            <label class="form-label"><strong>Wilayah</strong></label>
                            <select class="select2" name="regency" required>
                                
                                @foreach($composer_provincies as $prov)
                                    <optgroup label="{{$prov->display_name}}">
                                        
                                        @foreach($composer_regencies as $reg) 
                                            @if($prov->id == $reg->province_id)

                                                @php
                                                    $count_place = App\Models\Place::where('regency_id', $reg->id)->get()->count();
                                                @endphp

                                                @if(Request::path() == '/')
                                                    @if(Auth::check() && $reg->name == Auth::user()->profile->regency->name)
                                                        <option value="{{ $reg->name }}" selected>
                                                            {{ $reg->display_name }} ({{ $count_place }} Tempat)
                                                        </option>
                                                    @elseif(Auth::guest() && $reg->name == 'kabupaten-karawang')
                                                        <option value="{{ $reg->name }}" selected>{{ $reg->display_name }} ({{ $count_place }} Tempat)</option> 
                                                    @else
                                                        <option value="{{ $reg->name }}">{{ $reg->display_name }} ({{ $count_place }} Tempat)</option>
                                                    @endif
                                                @else
                                                    @if($reg->name == $regency->name)
                                                        <option value="{{ $reg->name }}" selected>{{ $reg->display_name }} ({{ $count_place }} Tempat)</option>
                                                    @else
                                                        <option value="{{ $reg->name }}">{{ $reg->display_name }} ({{ $count_place }} Tempat)</option>
                                                    @endif
                                                @endif
                                            @endif
                                        @endforeach
                                        
                                    </optgroup>
                                @endforeach

                            </select>
                            
                            @if ($errors->has('regency'))
                                <div class="form-tooltip-error">{{ $errors->first('regency') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>        
        </article>
    </section>
</div>
<div class="">

    @guest
        <div class="alert alert-twitter alert-close alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
    
            <strong>Hasil Pencarian</strong><br/>
            Kamu mencari tempat bermain <b>{{ ucwords($category->name) }}</b> dengan durasi <b>{{$request_duration}} jam</b>, pada
    
            @if($hari_diminta->format('l') == $composer_today->format('l'))
                <b>{{ ucwords('hari ini') }}</b>
            @else
                @include('layouts.convert_hari')
            @endif
    
            ,<b>{{ $hari_diminta->format('j M Y') }}</b>
            pukul @include('layouts.convert_jam')
            daerah <b>{{$regency_displayname}}</b>.
    
            <i class="font-icon font-icon-search"></i>
            @if(Request::path() != 'search/reg')
                Cari lagi?
    
                <a data-toggle="collapse"
                    data-parent="#accordion"
                    href="#triger"
                    aria-expanded="true"
                    aria-controls="collapseOne"
                    class="btn btn-sm btn-warning">
                    Cari Tempat Lain
                </a>
            @endif
        </div>
    @else
        {{--
        @foreach($booked as $book)
            <div class="alert alert-warning alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>Hei, mungkin kamu lupa pada waktu tersebut kamu ada permainan olahraga.</strong>
            </div>
        @endforeach
        --}}
    
        <div class="alert alert-twitter alert-close alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
    
            <strong>Hasil Pencarian</strong><br/>
            Kamu mencari tempat bermain <b>{{ ucwords($category->name) }}</b> dengan durasi <b>{{$request_duration}} jam</b>, pada
    
            @if($hari_diminta->format('l') == $composer_today->format('l'))
                <b>{{ ucwords('hari ini') }}</b>
            @else
                <b>{{ $hari_diminta->format('l') == 'Wednesday' ? ucwords('hari rabu'):'' }}</b>
                <b>{{ $hari_diminta->format('l') == 'Thursday' ? ucwords('hari kamis'):'' }}</b>
                <b>{{ $hari_diminta->format('l') == 'Friday' ? ucwords('hari jumat'):'' }}</b>
                <b>{{ $hari_diminta->format('l') == 'Saturday' ? ucwords('hari sabtu'):'' }}</b>
                <b>{{ $hari_diminta->format('l') == 'Sunday' ? ucwords('hari minggu'):'' }}</b>
                <b>{{ $hari_diminta->format('l') == 'Monday' ? ucwords('hari senin'):'' }}</b>
                <b>{{ $hari_diminta->format('l') == 'Tuesday' ? ucwords('hari selasa'):'' }}</b>
            @endif
    
            ,<b>{{ $hari_diminta->format('j M Y') }}</b>
            pukul <b>{{ $jam_diminta->format('H:i') }}</b>
            daerah <b>{{$regency_displayname}}</b>.
    
            <i class="font-icon font-icon-search"></i>
            @if(Request::path() != 'search/reg')
                Cari lagi?
            
            <a data-toggle="collapse"
                    data-parent="#accordion"
                    href="#triger"
                    aria-expanded="true"
                    aria-controls="collapseOne"
                    class="btn btn-sm btn-warning">
                    <i class="font-icon font-icon-search"></i>
                    Cari Tempat Lain
                </a>
            @endif
        </div>
    @endguest
</div>
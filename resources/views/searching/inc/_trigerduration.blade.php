<div class="form-group">
    <div class="form-control-wrapper">
        <label class="form-label"><strong>Durasi Bermain</strong></label>

        @if(Request::path() == '/')
            <!-- <input id="duration" class="form-control" type="text" value="1" placeholder="Durasi" name="durasi" required> -->
            <select name="durasi" class="select2" required>
                <option value="1" selected>1 Jam</option>
                <option value="2">2 Jam</option>
                <option value="3">3 Jam</option>
                <option value="4">4 Jam</option>
                <option value="5">5 Jam</option>
                <option value="6">6 Jam</option>
                <option value="7">7 Jam</option>
                <option value="8">8 Jam</option>
                <option value="9">9 Jam</option>
                <option value="10">10 Jam</option>
            </select>
        @else
            <select name="durasi" class="select2" required>
                <option value="1" {{ $request_duration == '1'? "selected":"" }}>1 Jam</option>
                <option value="2" {{ $request_duration == '2'? "selected":"" }}>2 Jam</option>
                <option value="3" {{ $request_duration == '3'? "selected":"" }}>3 Jam</option>
                <option value="4" {{ $request_duration == '4'? "selected":"" }}>4 Jam</option>
                <option value="5" {{ $request_duration == '5'? "selected":"" }}>5 Jam</option>
                <option value="6" {{ $request_duration == '6'? "selected":"" }}>6 Jam</option>
                <option value="7" {{ $request_duration == '7'? "selected":"" }}>7 Jam</option>
                <option value="8" {{ $request_duration == '8'? "selected":"" }}>8 Jam</option>
                <option value="9" {{ $request_duration == '9'? "selected":"" }}>9 Jam</option>
                <option value="10" {{ $request_duration == '10'? "selected":"" }}>10 Jam</option>
            </select>
            <!-- <input id="duration" class="form-control" type="text" value="{{ $request_duration }}" placeholder="Durasi" name="durasi" required> -->
        @endif

        @if ($errors->has('durasi'))
            <div class="form-tooltip-error">Durasi minimal 1 jam, maksimal 15 jam</div>
        @endif
    </div>
    <!-- <small class="text-muted">Pilih Durasi Bermain dalam jam</small> -->

</div>
<section class="widget top-tabs widget-tabs-compact">
    <div class="widget-tabs-nav bordered">
        <ul class="tbl-row" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#w-3-tab-1" role="tab" aria-expanded="true">
                    <i class="font-icon font-icon-earth-bordered"></i>
                    Booking Online
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#w-3-tab-2" role="tab" aria-expanded="false">
                    <i class="font-icon font-icon-home"></i>
                    Booking di Tempat
                </a>
            </li>
        </ul>
    </div>
    <div class="tab-content widget-tabs-content">
        <div class="tab-pane active" id="w-3-tab-1" role="tabpanel">
            <div class="">
                <div class="card-block">
                    <h5 class="with-border text-center">
                        Cari Tempat Olahraga di {{ $regency_displayname }}
                        <a class="btn btn-inline btn-sm btn-primary-outline"
                            data-toggle="collapse"
                            data-parent="#accordion"
                            href="#choose_regency"
                            aria-expanded="false"
                            aria-controls="collapseTwo">
                            <span class="font-icon glyphicon glyphicon-map-marker"></span>
                            Cari Wilayah
                        </a>
                    </h5><div class="m-t-lg "></div>
            
                    <form method="get" action="{{ route('searching.regency') }}" id="form-comment" class="">
                        @csrf
            
                        <div class="row">
                            @include('searching.inc._trigerlocation')
                        </div>
                        
                        <div class="row">
                            <div class="col-md-3">
                                @include('searching.inc._trigercategory')
                            </div>
                            <div class="col-md-3">
                                @include('searching.inc._trigerdate')
                            </div>
                            <div class="col-md-3">
                                @include('searching.inc._trigerclock')
                            </div>
                            <div class="col-md-3">
                                @include('searching.inc._trigerduration')
                            </div>
                        </div>
                        
                        <div class="row">
                            <section class="contacts-page-section text-center">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success btn-inline btn-block">
                                            <span class="font-icon glyphicon glyphicon-search"></span>
                                            Cari Lapang
                                        </button>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="w-3-tab-2" role="tabpanel">
            <div class="card-block">
                <h5 class="with-border text-center">
                    Booking di Tempat
                </h5><div class="m-t-lg "></div>
        
                <form method="post" action="{{ route('booking.onplace') }}" id="form-comment" class="">
                    @csrf
        
                    <div class="row">
                        <div class="col-md-5"></div>
                        <div class="col-md-2">
                            <fieldset class="form-group {{ $errors->has('place_id') ? 'form-group-error' : '' }}">
                            <label class="form-label"><strong>Kode Booking</strong></label>
                                <input type="text" name="code" class="form-control" required>
                                @if ($errors->has('place_id'))
                                    <div class="form-tooltip-error">{{ $errors->first('place_id') }}</div>
                                @endif
                            </fieldset>
                        </div>
                    </div>
                    
                    <div class="row">
                        <section class="contacts-page-section text-center">
                            <div class="col-md-5"></div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-warning btn-inline btn-block">
                                        <span class="font-icon glyphicon glyphicon-bookmark"></span>
                                        Booking Langsung
                                    </button>
                                </div>
                            </div>
                        </section>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>



<fieldset class="form-group {{ $errors->has('jam') ? 'form-group-error' : '' }}">
    <div class="form-control-wrapper">
        <label class="form-label"><strong>Jam</strong></label>
        <select class="select2-photo" name="jam">
            @for($jam = 9; $jam <= 23; $jam++)
                @if(Request::segment(1) == 'search')
                    @if($jam == $request_jam)
                        @if($jam < 10)
                            <option data-photo="{{ asset('img/others/jam/'.$jam.'.jpg') }}" value="{{$jam}}" selected>{{ '0'.$jam.':00' }}</option>
                        @else
                            <option data-photo="{{ asset('img/others/jam/'.$jam.'.jpg') }}" value="{{$jam}}" selected>{{ $jam.':00' }}</option>
                        @endif
                    @else
                        @if($jam < 10)
                            <option data-photo="{{ asset('img/others/jam/'.$jam.'.jpg') }}" value="{{$jam}}">{{ '0'.$jam.':00' }}</option>
                        @else
                            <option data-photo="{{ asset('img/others/jam/'.$jam.'.jpg') }}" value="{{$jam}}">{{ $jam.':00' }}</option>
                        @endif
                    @endif
                @else
                    @if($composer_now_hour >= 9)
                        @php
                            $hourslater = Carbon\Carbon::createFromFormat('H', $composer_now_hour)->addHours(2)->format('H');
                        @endphp    
                        
                        @if($jam == $hourslater)
                            @if($jam < 10)
                                <option data-photo="{{ asset('img/others/jam/'.$jam.'.jpg') }}" value="{{$jam}}" selected>{{ '0'.$jam.':00' }}</option>
                            @else
                                <option data-photo="{{ asset('img/others/jam/'.$jam.'.jpg') }}" value="{{$jam}}" selected>{{ $jam.':00' }}</option>
                            @endif
                        @else
                            @if($jam < 10)
                                <option data-photo="{{ asset('img/others/jam/'.$jam.'.jpg') }}" value="{{$jam}}">{{ '0'.$jam.':00' }}</option>
                            @else
                                <option data-photo="{{ asset('img/others/jam/'.$jam.'.jpg') }}" value="{{$jam}}">{{ $jam.':00' }}</option>
                            @endif
                        @endif
                    @else
                        @if($jam < 10)
                            @if($jam == 9)
                            <option data-photo="{{ asset('img/others/jam/'.$jam.'.jpg') }}" value="{{$jam}}" selected>{{ '0'.$jam.':00' }}</option>
                            @else
                            <option data-photo="{{ asset('img/others/jam/'.$jam.'.jpg') }}" value="{{$jam}}">{{ '0'.$jam.':00' }}</option>
                            @endif
                        @else
                            <option data-photo="{{ asset('img/others/jam/'.$jam.'.jpg') }}" value="{{$jam}}">{{ $jam.':00' }}</option>
                        @endif
                    @endif
                @endif
                
            @endfor
        </select>
        @if ($errors->has('jam'))
            <div class="form-tooltip-error">{{ $errors->first('jam') }}</div>
        @endif
    </div>
    <!-- <small class="text-muted">Pilih Jam Bermain</small> -->
</fieldset>
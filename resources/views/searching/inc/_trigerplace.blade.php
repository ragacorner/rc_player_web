<fieldset class="form-group {{ $errors->has('place_id') ? 'form-group-error' : '' }}">
    <div class="form-control-wrapper">
        <select name="place_id" class="select2">
            <option>-- Pilih tempat --</option>
        </select>
        @if ($errors->has('place_id'))
            <div class="form-tooltip-error">{{ $errors->first('place_id') }}</div>
        @endif
    </div>
    <small class="text-muted">Pilih tempat bermain</small>
</fieldset>

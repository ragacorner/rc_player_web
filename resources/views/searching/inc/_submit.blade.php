@php 
    $places = \App\Models\Place::all()->unique('category_id')->pluck('category_id')->all();
    $booked_played = \App\Models\Booking::where('player_id', Auth::id())->where('played','false')->pluck('category_id')->all();
    $categories = \App\Models\Category::whereIn('id', $places)->whereNotIn('id', $booked_played)->get();
@endphp 

@if($categories->count() > 0)
    @if($categories->count() == 1)
        <div class="col-md-5"></div>
        @foreach($categories as $cat)
            <div class="col-md-2">
                <input  type="submit" 
                        class="btn btn-{{ $cat->color }} btn-rounded btn-inline btn-block" 
                        name="submit" 
                        value="{{ $cat->display_name }}">
            </div>
        @endforeach
    @elseif($categories->count() == 2)
        <div class="col-md-4"></div>
        @foreach($categories as $cat)
            <div class="col-md-2">
                <input  type="submit" 
                        class="btn btn-{{ $cat->color }} btn-rounded btn-inline btn-block" 
                        name="submit" 
                        value="{{ $cat->display_name }}">
                <!-- <a href="#" class="btn btn-{{ $cat->color }} btn-inline btn-block">
                    <small>Cari di lapang </small><br>{{ $cat->display_name }}
                </a> -->
            </div>
        @endforeach
    @elseif($categories->count() == 3)
        <div class="col-md-3"></div>
        @foreach($categories as $cat)
            <div class="col-md-2">
                <input  type="submit" 
                        class="btn btn-{{ $cat->color }} btn-rounded btn-block" 
                        name="submit" 
                        value="{{ $cat->display_name }}">
            </div>
        @endforeach
    @else
        <div class="col-md-2"></div>
        @foreach($categories as $cat)
            <div class="col-md-2">
                <input  type="submit" 
                        class="btn btn-{{ $cat->color }} btn-rounded btn-block" 
                        name="submit" 
                        value="{{ $cat->display_name }}">
            </div>
        @endforeach
    @endif
@else
    <!-- <div class="col-md-5"></div>
    <div class="col-md-2">
        <input type="submit" class="btn btn-success btn-rounded btn-block" name="submit" value="Futsal" disabled>
    </div> -->
    <!-- <div class="col-md-2">
        <input type="submit" class="btn btn-primary btn-rounded btn-block" name="submit" value="Badminton" disabled>
    </div>
    <div class="col-md-2">
        <input type="submit" class="btn btn-warning btn-rounded btn-block" name="submit" value="Basket" disabled>
    </div> -->
@endif
<!-- <div class="col-md-2">
    <input type="submit" class="btn btn-success btn-rounded btn-block" name="submit" value="Futsal">
</div>
<div class="col-md-2">
    <input type="submit" class="btn btn-primary btn-rounded btn-block" name="submit" value="Badminton">
</div>
<div class="col-md-2">
    <input type="submit" class="btn btn-warning btn-rounded btn-block" name="submit" value="Basket">
</div> -->
<div class="row">
    <div class="col-md-12 item_place" id="">
        <div class="card">
            <article class="profile-post">
                <div class="profile-post-content">
                    <div class="tbl profile-post-card">
                        <div class="tbl-row">
                            @php
                                $category = App\Models\Category::find($field->place->category_id);
                                $subdistrict = App\Models\Subdistrict::find($field->place->subdistrict_id);
                                $regency = App\Models\Regency::find($field->place->regency_id);
                                $province = App\Models\Province::find($field->place->province_id);
                            @endphp
                            <div class="tbl-cell tbl-cell-photo">
                                <a href="{{route('place.show', $field->place->slug)}}">
                                    @if(empty($field->place->photo))
                                        <img src="{{ asset('img/place/building.jpg') }}" alt="">
                                    @else
                                        <img src="{{ url('http://place.ragacorner.dvlp/img/place/original/'.$field->place->photo) }}" alt="">
                                    @endif
                                </a>
                            </div>
                            <div class="tbl-cell">
                                <a href="{{ route('place.show', $field->place->slug) }}" target="_blank">
                                    <small>
                                    <div class="subdistrict">
                                        <p class="title"><b id="">{{ ucwords($field->place->name) }} - (Rumput {{ ucwords($field->grass_type) }})</b></p>
                                        <div class="color-blue-grey">
                                            <p>{{ ucwords($field->place->address) }}</p>
                                            <p>{{ ucwords($subdistrict->display_name) }}</p>
                                        </div>
                                        <p class="">
                                            @if($jam_diminta->format('H') >= '18')
                                                <strong class="font-16">Rp {{ number_format($field->priceNight) }} /Jam</strong>
                                            @else
                                                <strong class="font-16">Rp {{ number_format($field->priceDayLight) }} /Jam</strong>
                                            @endif
                                        </p>
                                    </div>
                                    </small>
                                </a>
                            </div>
                            <div class="tbl-cell tbl-cell-btns">
                                <div class="pull-right">
                                    
                                    {!! Form::open(['url' => route('booking.create', $field->place->slug), 'method' => 'get', 'class'=>'']) !!}
                                        {{ csrf_field() }}

                                        {!! Form::hidden('duration', $request_duration) !!}
                                        <input type="hidden" name="field_id" value="{{ $field->id }}">
                                        {!! Form::hidden('jam', $jam_diminta->format('H')) !!}
                                        {!! Form::hidden('hari', $hari_diminta->format('d-m-Y')) !!}
                                        
                                        <button type="submit" class="btn btn-{{ $field->place->category->color }}">
                                            Book Now
                                            <span class="font-icon glyphicon glyphicon-arrow-right"></span>
                                        </button>

                                    {!! Form::close() !!}

                                    <!-- <a href="{{route('searching.place', $field->place->slug)}}" class="btn btn-success btn-block">
                                    Booking<br> 
                                    <small>Lapang {{ $field->name }}</small>    
                                    </a> -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </div>
</div>
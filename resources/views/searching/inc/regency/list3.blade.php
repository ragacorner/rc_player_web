<div class="col-md-12 item_place" id="">
    <div class="card">
        <article class="profile-post">
        
            <a  href="{{ route('booking.create', $field->place->slug) }}" 
                onclick="event.preventDefault();document.getElementById('booking-form{{ $field->id }}').submit();">
                
                <div class="profile-post-content">
                    <div class="tbl profile-post-card">
                        <div class="tbl-row">
                            @php
                                $category = App\Models\Category::find($field->place->category_id);
                                $subdistrict = App\Models\Subdistrict::find($field->place->subdistrict_id);
                                $regency = App\Models\Regency::find($field->place->regency_id);
                                $province = App\Models\Province::find($field->place->province_id);
                            @endphp
                            <div class="tbl-cell tbl-cell-photo">
                                @if(empty($field->place->photo))
                                    <img src="{{ asset('img/place/building.jpg') }}" alt="">
                                @else
                                    <img src="{{ url('http://place.ragacorner.dvlp/img/place/original/'.$field->place->photo) }}" alt="">
                                @endif
                            </div>
                            <div class="tbl-cell">
                                <div class="subdistrict">
                                    <p class="title"><b id="">{{ ucwords($field->place->name) }} - (Lapang {{ $field->name }}: {{ ucwords($field->grass_type) }})</b></p>
                                    <font color="grey">
                                        <p class="">
                                            {{ ucwords($field->place->address) }}<br>
                                            {{ ucwords($subdistrict->display_name) }}, {{ $regency->display_name }}, {{ $province->display_name }}
                                        </p>
                                    </font>
                                </div>
                            </div>
                            <div class="tbl-cell tbl-cell-btns">
                                <div class="pull-right">
                                    @if($field->place->category_id == 1)
                                    <span class="label label-success">
                                    @elseif($field->place->category_id == 2)
                                    <span class="label label-primary">
                                    @elseif($field->place->category_id == 3)
                                    <span class="label label-warning">
                                    @endif

                                        @if($jam_diminta->format('H') >= '18')
                                            <h3 class="no-margin"><strong>Rp {{ number_format($field->priceNight) }}</strong></h3>
                                        @else
                                            <h3 class="no-margin"><strong>Rp {{ number_format($field->priceDayLight) }}</strong></h3>
                                        @endif
                                        <p class="">/ jam</p>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            
            <form id="booking-form{{ $field->id }}" action="{{ route('booking.create', $field->place->slug) }}" method="GET" style="display: none;">
                
                @csrf
                <input type="hidden" name="field_id" value="{{ $field->id }}">
                <input type="hidden" name="jam" value="{{ $jam_diminta->format('H') }}">
                <input type="hidden" name="hari" value="{{ $hari_diminta->format('d-m-Y') }}">
                <input type="hidden" name="duration" value="{{ $request_duration }}">
            </form>
            
        </article>
    </div>
</div>
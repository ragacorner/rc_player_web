<div class="col-md-12 item_place" id="">
    <div class="card">
        <article class="profile-post">
            <div class="profile-post-content">
                <div class="tbl profile-post-card">
                    <div class="tbl-row">
                        @php
                            $category = App\Models\Category::find($field->place->category_id);
                            $subdistrict = App\Models\Subdistrict::find($field->place->subdistrict_id);
                            $regency = App\Models\Regency::find($field->place->regency_id);
                            $province = App\Models\Province::find($field->place->province_id);
                        @endphp
                        <div class="tbl-cell tbl-cell-photo">
                            <a href="#">
                                @if(empty($field->place->photo))
                                    <img src="{{ asset('img/place/building.jpg') }}" alt="">
                                @else
                                    <img src="{{ url('http://place.ragacorner.dvlp/img/place/original/'.$field->place->photo) }}" alt="">
                                @endif
                            </a>
                        </div>
                        <div class="tbl-cell">
                            <p class="title"><a href="{{route('place.show', $field->place->slug)}}"><b id="">{{ ucwords($field->place->name) }} - (Lapang {{ $field->name }})</b></a></p>
                            <p class="subdistrict">{{ ucwords($field->place->address) }}, {{ ucwords($subdistrict->display_name) }}</p>
                            <p>{{ $regency->display_name }}, {{ $province->display_name }}</p>
                        </div>
                        <div class="tbl-cell tbl-cell-btns">
                            <div class="pull-right">
                                
                                {!! Form::open(['url' => route('searching.place', $field->place->slug), 'method' => 'get', 'class'=>'']) !!}
                                    {{ csrf_field() }}

                                    {!! Form::hidden('regency', $regency->name) !!}
                                    {!! Form::hidden('jam', $jam_diminta->format('H')) !!}
                                    {!! Form::hidden('hari', $hari_diminta->format('d-m-Y')) !!}
                                    
                                    <button type="submit" class="btn btn-success btn-block">Kunjungi<br> 
                                        <small>Tempat</small>
                                    </button>

                                {!! Form::close() !!}

                                <!-- <a href="{{route('searching.place', $field->place->slug)}}" class="btn btn-success btn-block">
                                Booking<br> 
                                <small>Lapang {{ $field->name }}</small>    
                                </a> -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>
</div>

<div class="row">
    <header class="page-content-header m-b-0">   
        <section class="widget-triger widget-accordion no-border" id="accordion" role="tablist" aria-multiselectable="true">
            <div id="triger" class="panel-collapse collapse {{ Request::path() == 'search/reg' ? 'in':'' }}" role="tabpanel" aria-labelledby="headingOne">
                <div class="card-block">
                    <h5 class="with-border text-center">
                        Cari Tempat Olahraga {{ $regency_displayname }}
                        <a class="btn btn-inline btn-sm btn-primary-outline"
                            data-toggle="collapse"
                            data-parent="#accordion"
                            href="#choose_regency"
                            aria-expanded="false"
                            aria-controls="collapseTwo">
                            <span class="font-icon glyphicon glyphicon-map-marker"></span>
                            Cari Wilayah
                        </a>
                    </h5><div class="m-t-lg "></div>

                    <form method="get" action="{{ route('searching.regency') }}" id="form-comment" class="">
                        @csrf

                        <div class="row">
                            @include('searching.inc._trigerlocation')
                        </div>
                        
                        <div class="row">
                            <div class="col-md-3">
                                @include('searching.inc._trigercategory')
                            </div>
                            <div class="col-md-3">
                                @include('searching.inc._trigerdate')
                            </div>
                            <div class="col-md-3">
                                @include('searching.inc._trigerclock')
                            </div>
                            <div class="col-md-3">
                                @include('searching.inc._trigerduration')
                            </div>
                        </div>
                        <div class="row">
                            <section class="contacts-page-section text-center">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success btn-inline btn-block">
                                            <span class="font-icon glyphicon glyphicon-search"></span>
                                            Cari Lapang
                                        </button>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </header>
</div>



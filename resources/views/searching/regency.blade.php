@extends('layouts.app')

@section('css')
@endsection

@section('content')
    
    <div class="">
        <div class="p-t-md"></div>
        
        <div class="row">
            <div class="col-md-3">
                
            </div>
            <div class="col-md-6">

                <div class="card-user-grid">
                    @if($hari_diminta <= $batas_hari && $hari_diminta >= $composer_today)
                        @if(($request_jam <= $composer_now_hour) && ($hari_diminta->format('d-m-Y') == $composer_now_date))
                            <div class="card">
                                <div class="card-block">
                                    <div class="text-center">
                                        <h3 class="with-border">Maaf, sekarang jam {{ $composer_now_hour_minute }} </h3>
                                        <h5>Waktu yang kamu cari sudah lewat, cobalah cari waktu untuk jam berikutnya :)</h5>
                                    </div>
                                </div>
                            </div>
                        @else
                            @if($jam_diminta->format('i') != '00' )
                                <div class="text-center">
                                    <h1 class="with-border">Pemberitahuan!</h1>
                                    <h3>Harap gunakan waktu pas (contoh: 09:00, 10:00, 11:00)</h3>
                                </div>
                            @else
                                @if($fields->count() > 0)
                                    @include('searching.inc.alert')
                                    <div class="card">
                                        <div class="card-block">
                                            <input  type="text" 
                                                    id="myInputSubdistrict" 
                                                    onkeyup="theSub()" 
                                                    class="form-control" 
                                                    autofocus
                                                    placeholder="Ketik untuk mencari tempat atau daerah">
                                        </div>
                                    </div>
                                @endif

                                <div id="myTable2">
                                    @forelse($fields as $field)
                                        @if($jam_diminta < $field->place->open)
                                            <div class="text-center">
                                                <h1 class="with-border">Maaf :)</h1>
                                                <h3>Kelihatannya masih belum ada yang buka pada waktu itu</h3>
                                            </div>
                                        @else
                                            @include('searching.inc.regency.list')
                                        @endif
                                    @empty
                                    <div class="card">
                                        <div class="card-block text-center">
                                            <h3 class="with-border">Oops Kosong!</h3>
                                            <div class="col-md-12">
                                                <h5>
                                                    Undang Tempat {{ ucwords($request_category) }} di Daerahmu Untuk Bergabung ke Ragacorner
                                                </h5>
                                            </div>
                                        </div>
                                    </div>
                                    @endforelse
                                </div>                      
                            @endif                    
                        @endif
                    @else
                    <div class="card">
                        <div class="card-block">
                            <div class="text-center">
                                <h1 class="with-border">Maaf :)</h1>
                                <h3>Batas pemesanan di 
                                <i>Ragacorner</i> 
                                hanya bisa dilakukan mulai 
                                <b>hari ini</b> 
                                sampai 
                                <b>1 bulan</b>
                                ke depan.</h3>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-sm"
            tabindex="-1"
            role="dialog"
            aria-labelledby="mySmallModalLabel"
            aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                        <i class="font-icon-close-2"></i>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Buat Undangan</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">
                        <span class="font-icon glyphicon glyphicon-send"></span>  Kirim
                    </button>
                    <!-- <button type="button" class="btn btn-rounded btn-default" data-dismiss="modal">Close</button> -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        function theSub() {
            var input, filter, table, tr, td, i, txtValue, title, subdistrict;
            input = document.getElementById("myInputSubdistrict");
            filter = input.value.toUpperCase();

            table = document.getElementById("myTable2");
            tr = table.getElementsByClassName("item_place");
            
            for (i = 0; i < tr.length; i++) {
                
                title = tr[i].getElementsByClassName("subdistrict")[0];

                if (title) {
                    txtValue = title.textContent || title.innerText;
                
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }else{
                    console.log('ga ada daerahnya');
                }
            }
        }
    </script>
@endsection

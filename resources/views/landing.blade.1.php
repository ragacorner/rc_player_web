@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('css/separate/pages/ribbons.min.css') }}">
@endsection

@section('content')

    @if($bookings->count() > 0)
        <div class="alert alert-warning alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>Sekedar Info!</strong> Kamu masih ada permainan yang belum kamu selesaikan.
        </div>
        
        <div class="m-b-lg"></div>
    @endif
sadfa
    @include('searching.inc.triger_v1')
    @include('layouts.landing.alert_info')

    {{-- @include('layouts.advertisement.welcome') --}}
@endsection

@section('js')
@endsection
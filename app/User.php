<?php

namespace App;

use App\Models\Place;
// use Laravel\Passport\HasApiTokens;
use App\Models\Profile;
use App\Models\Notification;
// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable // implements MustVerifyEmail
{
    use Notifiable, SoftDeletes;
    
    const VERIFIED_USER = '1';
    const UNVERIFIED_USER = '0';
    
    const ADMIN_USER = "true";
    const REGULAR_USER = "false";

    protected $table = 'users';
    protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','phone', 'password','is_booker','has_place','is_admin','verified','verification_token','termsOfUse'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token',
        // 'verification_token'
    ];

    public function setNameAttribute($name)
    {
        $this->attributes['name'] = strtolower($name);
    }

    public function getNameAttribute($value)
    {
        return ucwords($value);
    }

    public function setEmailAttribute($email)
    {
        $this->attributes['email'] = strtolower($email);
    }

    public function getEmailAttribute($email)
    {
        return $email;
    }

    public function isVerified()
    {
        return $this->verified == User::VERIFIED_USER;
    }

    public function isAdmin()
    {
        return $this->is_admin == User::ADMIN_USER;
    }

    public static function generateVerificationCode()
    {
        return str_random(40);
    }

    //==================MODEL=============================

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    public function places()
    {
        return $this->belongsToMany(Place::class);
    }

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }
}

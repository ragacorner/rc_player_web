<?php

namespace App;

use App\Models\Profile;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Player extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, SoftDeletes;
    
    const VERIFIED_USER = '1';
    const UNVERIFIED_USER = '0';
    
    const ADMIN_USER = "true";
    const REGULAR_USER = "false";

    // const HAS_PLACE = "true";

    protected $table = 'players';
    protected $dates = ['deleted_at'];
    // public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','phone','has_setup', 'password','verified','verification_token','termsOfUse','provider', 'provider_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token',
        // 'verification_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setNameAttribute($name)
    {
        $this->attributes['name'] = strtolower($name);
    }

    public function getNameAttribute($value)
    {
        return ucwords($value);
    }

    public function setEmailAttribute($email)
    {
        $this->attributes['email'] = strtolower($email);
    }

    public function getEmailAttribute($email)
    {
        return $email;
    }

    public function isVerified()
    {
        return $this->verified == Player::VERIFIED_USER;
    }

    public function isAdmin()
    {
        return $this->admin == Player::ADMIN_USER;
    }

    public static function generateVerificationCode()
    {
        return str_random(40);
    }

    //==================MODEL=============================

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }
}

<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Booking;
use Illuminate\Console\Command;

class RemoveBooking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove:unpaid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Hapus semua booking yang tak terbayar';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now            = Carbon::now('Asia/Jakarta');
        $unpaid_book    = Booking::where('played','false')
                            ->where('paid_half','false')
                            ->where('paid_full','false')
                            ->where('playing_date','<=', $now->format('Y-m-d'))
                            ->where('playing_time','<=', $now->format('H:i:s'))
                            ->get();

        $past_book    = Booking::where('played','false')
                            ->where('playing_date','<=', $now->format('Y-m-d'))
                            ->where('playing_time','<=', $now->format('H:i:s'))
                            ->get();
                            
        foreach ($unpaid_book as $book) 
        {
            $book->delete();
        }

        foreach ($past_book as $book) 
        {
            $book->played = "missed";
            $book->save();
        }

        // return;

        // session()->put('success','Pesanan Kamu Sudah Dibatalkan');
        // return redirect()->route('player.games.index', Auth::user()->profile->username);
    }
}

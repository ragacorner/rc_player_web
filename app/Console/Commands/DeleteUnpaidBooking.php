<?php

namespace App\Console\Commands;

use App\Booking;
use App\StockField;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class DeleteUnpaidBooking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DeleteUnpaidBooking:deletebooking';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bookings = StockField::where('booking_id',2)->delete();
        
        // foreach ($bookings as $booking) {
        //     $booking->stock_field->delete();
        //     // Booking::find($booking->id)->delete();
        // }
        // $bookings->destroy();
        DB::table('bookings')
            // ->where('status','unpay')
            // ->where('created_at', '')
            ->delete(2);
    }
}

<?php

namespace App\Http\ViewComposers;

use App\Models\Notification;
use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;

class NotificationsComposer
{
    public function compose(View $view)
    {
        $notifications   = Notification::where('user_id', Auth::id())->where('status','undone')->get();

        $view->with('composer_notifications', $notifications);
    }
}
<?php

namespace App\Http\ViewComposers;

use App\Models\Place;
use App\Models\Regency;
use App\Models\Province;
use Illuminate\View\View;
use App\Models\Subdistrict;
use Illuminate\Support\Facades\Auth;

class LocationsComposer
{
    public function compose(View $view)
    {
        $places         = Place::all();
        $subdistricts   = Subdistrict::all();
        $regencies      = Regency::all(); //DB::table('regencies')->pluck('name','id')->all();
        $provincies     = Province::all(); //DB::table('provinces')->select()->all();

        $view->with('composer_places', $places);
        $view->with('composer_subdistricts', $subdistricts);
        $view->with('composer_regencies', $regencies);
        $view->with('composer_provincies', $provincies);
    }
}
<?php

namespace App\Http\ViewComposers;

use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Carbon\Carbon;

class TimesComposer
{
    public function compose(View $view)
    {
        $now = Carbon::now('Asia/Jakarta');
        $now_year	= $now->format('Y');
        $now_month	= $now->format('m');
        $now_day	= $now->format('d');
        $now_date	= $now->format('d-m-Y');
        $now_hour	= $now->format('H');
        $now_minute	= $now->format('i');
        $now_second	= $now->format('s');
        $now_hour_minute	= $now->format('H:i');
        $now_clock	= $now->toTimeString();

        $today = Carbon::today('Asia/Jakarta');

        $view->with('composer_now', $now);
        $view->with('composer_now_year', $now_year);
        $view->with('composer_now_month', $now_month);
        $view->with('composer_now_day', $now_day);
        $view->with('composer_now_date', $now_date);
        $view->with('composer_now_hour', $now_hour);
        $view->with('composer_now_month', $now_month);
        $view->with('composer_now_second', $now_second);
        $view->with('composer_now_clock', $now_clock);
        $view->with('composer_now_hour_minute', $now_hour_minute);
        $view->with('composer_today', $today);

    }
}

<?php

namespace App\Http\ViewComposers;

use App\Models\Category;
use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;

class CategoriesComposer
{
    public function compose(View $view)
    {
        $categories   = Category::all();

        $view->with('composer_categories', $categories);
    }
}
<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Field;
use App\Models\Place;
use App\Models\Booking;
use App\Models\BookedField;
use Illuminate\Http\Request;
use App\Models\PlayerOffline;
use App\Models\BookingPayment;
use App\Models\BookingGenerate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class GuestController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function bookingonplace(Request $request)
    {
        // $rules = [
        //     'duration'    => 'required|integer|in:1,2,3,4,5,6,7,8,9,10',
        // ];

        // $this->validate($request, $rules, [
        //     'duration.required'=>'Silahkan mengisi durasi bermain',
        // ]);

        $request_code = $request->code;
        $request_email = $request->email;
        // $request_duration = $request->durasi;

        $bookgen_get = BookingGenerate::where('code', $request_code)->get();
        $bookgen_count = $bookgen_get->count();
        $bookgen = $bookgen_get->first();
        
        if(!empty($bookgen)){
            $bookgen->player_email = $request_email;
            // $bookgen->duration = $bookgen->duration;
            $bookgen->save();

            $place = Place::find($bookgen->place_id);
            $field = Field::find($bookgen->field_id);
            $bookings   = Booking::where('place_id', $place->id)->get();
            $booked     = DB::table('booked_fields')->where('field_id', $field->id)
            ->where('playing_date', $bookgen->playing_date)
            ->where('playing_time', $bookgen->playing_time)
            ->first();
            
            if(!empty($booked))
            {
                session()->put('warning','Maaf, Tidak bisa memesan 2 kali');
                return redirect()->route('searching.landing');
            }else{
                $booking                    = new Booking();
                $booking->player_id         = Auth::check() ? Auth::id() : 3;
                $booking->category_id       = $place->category_id;
                $booking->place_id          = $place->id;
                $booking->field_id          = $field->id;

                if ($booking->playing_time >= '18:00:00' ) {
                    $booking->price_field   = $field->priceNight;
                }else{
                    $booking->price_field   = $field->priceDayLight;
                }

                $booking->regency_id        = $place->regency_id;
                $booking->subdistrict_id    = $place->subdistrict_id;
                $booking->playing_date      = $bookgen->playing_date;
                $booking->playing_time      = $bookgen->playing_time;
                $booking->duration          = $bookgen_count;
                $booking->played            = 'false';
                $booking->game_type         = 'regular';
                $booking->save();
                
                $timer = Carbon::createFromFormat('H:i:s', $bookgen->playing_time);
                for ($durasi=0; $durasi < $bookgen_count; $durasi++) 
                {
                    $booked = new BookedField();
                    $booked->subdistrict_id = $place->subdistrict_id;
                    $booked->booking_id     = $booking->id;
                    $booked->field_id       = $booking->field_id;
                    $booked->playing_date   = $booking->playing_date;
                    
                    if ($durasi < 1) {
                        $booked->playing_time  = $timer->addHour($durasi)->toTimeString();
                    } else {
                        $booked->playing_time  = $timer->addHour(1)->toTimeString();
                    }
                    
                    $booked->save();
                }
                
                $exp = explode(" ", $place->name);
                $acronym = "";
                foreach ($exp as $w) {
                    $acronym .= $w[0];
                }

                $playoff = PlayerOffline::where('code', $request_code)->first();
                $playoff->booking_id = $booking->id;
                $playoff->save();


                // $inv            = 'INV.'.$booking->id.".$acronym"."3".$bookings->count();
                $booking->code  = $request_code;
                $booking->save();

                //  METHODE PEMBAYARAN
                $payment                = new BookingPayment();
                $payment->player_id     = Auth::check() ? Auth::id() : 3;
                $payment->booking_id    = $booking->id;
                $payment->method        = 'onthespot';

                if ($booking->playing_time >= '18:00:00' ) {
                    $payment->amount        = $field->priceNight;
                    $payment->toFutsal      = $field->priceNight;
                }else{
                    $payment->amount        = $field->priceDayLight;
                    $payment->toFutsal      = $field->priceDayLight;
                }

                $payment->toCompany = 0;
                $payment->status    = 'undone';
                $payment->save();

                BookingGenerate::where('code', $request_code)->delete();

                session()->put('success','Selamat, pemesanan lapangan Kamu berhasil, segera lakukan pembayaran :)');
                return redirect()->route('booking.onplace.payment', $booking->code);
            }
        }

        session()->put('error','Maaf, kamu salah memasukan kode atau hubungi customer care ragacorner');
        return redirect()->route('searching.landing');

    }

    public function payment(Request $request, $code)
    {
        $carb_now =  Carbon::now();
        $booked     = DB::table('bookings')
                    ->where('code', $code)
                    ->where('playing_date', $carb_now->toDateString())
                    ->where('paid_half', 'false')
                    ->where('paid_full', 'false')
                    ->where('played', 'false')
                    ->first();

        if(!empty($booked))
        {
            $place          = DB::table('places')->find($booked->place_id);
            $category       = DB::table('categories')->find($place->category_id)->display_name; 
            $playing_date   = Carbon::createFromFormat('Y-m-d', $booked->playing_date)->format('d M Y');
            $playing_time   = Carbon::createFromFormat('H:i:s', $booked->playing_time)->format('H:i');
            $duration       = $booked->duration;
            $field          = DB::table('fields')->find($booked->field_id);

            return view('booking.onthespot.booked', compact('code','place','category','playing_date','playing_time','duration','field'));
        }
        
        return abort(404);
    }
}

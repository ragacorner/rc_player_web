<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Booking;
use App\Models\Regency;
use App\Models\Subdistrict;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GamesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('as:player');
    }

    public function index(Request $request, $id)
    {
        $user           = Auth::user()->profile;

        if (!empty($user)) 
        {    
            $player_area    = ucwords($user->area);
            $player_subd    = Subdistrict::find($user->subdistrict_id)->display_name;
            $player_regency = Regency::find($user->regency_id)->display_name;
            $bookings       = Booking::where('player_id', Auth::id())->orderBy('created_at','Desc');
            $unplayed_books = $bookings->where('played', 'false')->get();
            $bookeds        = Booking::where('player_id', Auth::id())->where('played','!=','false')->get();
            $warning_play   = Carbon::now('Asia/Jakarta')->toTimeString();
            
            return view('player.games.index', compact(
                'bookings','bookeds','unplayed_books','user','player_area','player_subd','player_regency','warning_play'
            ));
        }

        return redirect()->route('home');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Regency;
use App\Models\Subdistrict;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BoardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('as:player');
    }

    public function index(Request $request)
    {
        $user           = Auth::user()->profile;

        if (!empty($user)) 
        {    
            $player_area    = ucwords($user->area);
            $player_subd    = Subdistrict::find($user->subdistrict_id)->display_name;
            $player_regency = Regency::find($user->regency_id)->display_name;
            $bookings       = Booking::where('player_id', Auth::id())->get()->sortByDesc('updated_at');
            
            return view('boards.index', compact(
                'bookings','user','player_area','player_subd','player_regency'
            ));
        }

        return redirect()->route('home');
    }
}

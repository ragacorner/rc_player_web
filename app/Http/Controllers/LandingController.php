<?php

namespace App\Http\Controllers;

use App\Player;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Cookie;

class LandingController extends Controller
{
    /**
     * FUNCTION GETREG '/getreg'
     * untuk mengganti wilayah kota/kabupaten, dan menyimpan dengan cookie selamat 1 bulan.
     * 1. validasi form regency
     * 2. update cookie dgn cara menghapus lalu menambahkan ulang data ke cookie
     */
    public function getreg(Request $request)
    {
        $rules = [
            'regency'       => 'required|string|exists:regencies,name',
        ];

        $this->validate($request, $rules, [
            'regency.required'=>'isi dengan kota/kabupaten kamu ingin bermain :)',
        ]);

        Cookie::forget('request_regency');
        Cookie::queue('request_regency', $request->regency, 40320);

        return redirect()->route('searching.landing');
    }

    /**
     * FUNCTION LANDING '/'
     * form untuk mencari lapangan kosong dan menampilkan jumlah booking yg tersisa
     * 1. cek tamu, jika iya maka berikan data record regency daerah karawang
     * 2. jika bukan maka ambil dari profile, dan hitung jumlah booking tersisa
     * 3. Hapus cookie
     */
    public function landing(Request $request)
    {   
        if (Auth::guest() || Auth::user()->has_setup != 'false')
        {
            if(Auth::guest()){
                $request_regency = 'kabupaten-karawang';
                $regency_displayname = DB::table('regencies')->where('name',$request_regency)->first()->display_name;
            }else{
                $regency_displayname = Auth::user()->profile->regency->display_name;
                $bookings           = DB::table('bookings')
                                    ->where('player_id', Auth::id())
                                    ->where('played','false')
                                    ->count();
            }

            $cookie_subdistrict = Cookie::forget('subdistrict_id');
            $cookie_category    = Cookie::forget('request_category');
            $cookie_jam         = Cookie::forget('request_jam');
            $cookie_hari        = Cookie::forget('request_hari');
            $cookie_url         = Cookie::forget('request_url');
            
            return response(view('landing', compact('bookings','regency_displayname')))
                    ->withCookie($cookie_subdistrict)
                    ->withCookie($cookie_category)
                    ->withCookie($cookie_jam)
                    ->withCookie($cookie_hari)
                    ->withCookie($cookie_url);
        }

        return redirect()->route('home');
    }

    public function termsOfUse()
    {
        return view('laws.termsofuse');
    }

    public function privacy()
    {
        return view('laws.privacy');
    }

}

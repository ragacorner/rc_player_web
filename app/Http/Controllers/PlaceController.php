<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Field;
use App\Models\Place;
use App\Models\Regency;
use App\Models\Category;
use App\Models\BookedField;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class PlaceController extends Controller
{
    public function show(Request $request, $place_slug)
    {
        if(Auth::guest() || (Auth::user()->has_setup == 'true'))
        {
            $request_duration   = $request->duration;
            $place          = Place::where('slug',$place_slug)->first();
            
            if (!empty($place)) {
                $category = Category::find($place->category_id);
            } else {
                return abort(404);
            }

            $fields = Field::where('place_id', $place->id)->get();
            $places = Place::where('category_id', $place->category_id)->where('id','!=', $place->id)->get()->random(5);

            if(!empty($places)){
                return response(
                        view('places.show', compact('place','category','places','fields','request_duration')
                ));
            }else{
                return abort(404);
            }
            
        }

        return redirect('/');
    }
}

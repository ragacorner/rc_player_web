<?php

namespace App\Http\Controllers;

use App\Player;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Place;
use App\Models\Booking;
use App\Models\Profile;
use App\Models\Regency;
use App\Models\Category;
use App\Models\Province;
use App\Models\Subdistrict;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class SetupProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('as:player_setup');
    }

    public function create()
    {
        if(Auth::user()->has_setup == "false")
        {
            $req = Auth::user()->name;
            $string = str_replace(' ', '-', $req);
            $username = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $string));

            // $places = Place::all();
            // $provinces = Province::whereIn('id', $places->unique('province_id')->pluck('province_id'))->get();
            // $regencies = Regency::whereIn('id', $places->unique('regency_id')->pluck('regency_id'))->get();
            
            session()->put('info','Terima Kasih Telah Mendaftar, Silahkan Lengkapi Profile Untuk Memulai Permainan :)');
            return view('settings.setup.create', compact('username'));
        }

        return redirect('/');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'gender'        => 'in:male,female',
            'phone'         => 'required|unique:players,phone',
            'category'      => 'required|exists:categories,name',
            'regency_id'       => 'required|exists:regencies,id',
            'subdistrict_id'   => 'required|exists:subdistricts,id',
            'photo'         => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $birthday  = Carbon::createFromFormat('m/d/Y', $request->birthday)->toDateString();
        $category   = DB::table('categories')->where('name',$request->category)->first();
        
        $profile    = Profile::create([
            'player_id'     => Auth::id(),
            'gender'        => $request->gender,
            'username'      => Auth::user()->name,
            'description'   => 'Pemain '.$category->display_name.' Profesional',
            'birthday'      => $birthday,
            'occupation'    => 'smp',
            'category_id'   => $category->id,
            'area'          => $request->area,
            'regency_id'    => $request->regency_id,
            'subdistrict_id'=> $request->subdistrict_id,
        ]);

        if ($request->hasFile('photo')) {
            $destinationPath1 = public_path('/img/player/500x500');
            $destinationPath2 = public_path('/img/player/800x800');
            $img = Image::make($request->file('photo')->getRealPath());
            
            $img->resize(500, 500, function ($constraint) {
                $constraint->aspectRatio();
            })->crop(500, 500)->save($destinationPath1.'/'.$profile->username.'.jpg');

            $img->resize(800, 800, function ($constraint) {
                $constraint->aspectRatio();
            })->crop(800, 800)->save($destinationPath2.'/'.$profile->username.'.jpg');

            $profile->photo = $this->savePhoto($request->file('photo'));
            $profile->save();
        }

        $player = Player::find(Auth::id());
        $player->phone      = $request->phone;
        $player->has_setup  = 'true';
        $player->verified   = 1;
        $player->save();

        session()->put('success','Selamat, Kamu Sudah Bisa Mulai Bermain.');
        return redirect('/');

    }

    protected function savePhoto(UploadedFile $photo) 
    {
        $fileName = str_random(40) . '.' . $photo->guessClientExtension(); 
        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/player/original'; 
        $photo->move($destinationPath, $fileName);

        return $fileName;
    }
}

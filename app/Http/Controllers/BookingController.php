<?php

namespace App\Http\Controllers;

use App\Player;
use Carbon\Carbon;
use App\Models\Field;
use App\Models\Place;
use GuzzleHttp\Client;
use App\Models\BookingPayment;
use App\Models\Booking;
use App\Models\Regency;
use App\Models\Category;
use App\Models\Province;
use App\Models\StockField;
use App\Models\BookedField;
use App\Models\Subdistrict;
use Illuminate\Http\Request;
use App\Models\PlaceFacility;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Cookie;

class BookingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('as:player');
    }

    /**
     * halaman landing pencarian ragacorner.com
     * 1. ambil data dari table regencies
     * 2. ambil data dari table places secara ajax berdasarkan regency_id form 
     */
    
    public function create(Request $request, $slug)
    {
        $rules = [
            'duration'    => 'required|integer|in:1,2,3,4,5,6,7,8,9,10',
        ];

        $this->validate($request, $rules, [
            'duration.required'=>'Silahkan mengisi durasi bermain',
        ]);

        // return $slug;

        // $cookie_regency     = Cookie::forget('request_regency');
        // $cookie_category    = Cookie::forget('request_category');
        // $cookie_jam         = Cookie::forget('request_jam');
        // $cookie_hari        = Cookie::forget('request_hari');
        // $cookie_place       = Cookie::forget('place_id');
        // $cookie_field       = Cookie::forget('field_id');
        // $cookie_url         = Cookie::forget('url');

        $request_category   = $request->cookie('request_category');
        $request_regency    = $request->regency;
        $request_jam        = $request->jam;
        $request_hari       = $request->hari;
        
        // jangan lupa cek database untuk booking jika sudah terbooking, berikan pilihan pada client
        if (Auth::user()->has_setup == 'true')
        {
            if (empty($request->cookie('request_jam'))) {
                session()->put('warning',"Oops, ada kesalahan! silahkan dicoba kembali");
                return redirect('/');
            }

            if (Auth::check()) {
                $cat = Category::where('name', $request_category)->first();
                $booked_played  = Booking::where('player_id', Auth::id())
                                ->where('played','false')
                                ->where('category_id', $cat->id)
                                ->pluck('category_id')
                                ->first();

                if(!empty($booked_played)){
                    alert()->warning("Kamu harus menyelesaikan permainan $cat->name, cobalah minta kawan untuk membuat pemesanan baru.", 'Maaf')->autoclose(10000);
                    return redirect('/');
                }
            }

            $place          = Place::where('slug',$slug)->first();
            $jam_diminta    = Carbon::createFromFormat("H", $request_jam);
            $hari_diminta   = Carbon::createFromFormat("d-m-Y", $request_hari);
            
            $booked         = DB::table('booked_fields')
                            ->where('subdistrict_id', $place->subdistrict_id)
                            ->where('playing_date', $hari_diminta->toDateString())
                            ->where('playing_time', $jam_diminta->toTimeString())
                            ->get();
                
            $fields         = DB::table('fields')
                            ->where('place_id', $place->id)
                            ->whereNotIn('id', $booked->pluck('field_id')->all())
                            ->get();
            
            $field          = Field::find($request->field_id);
            $duration       = $request->duration;
            $facilities     = PlaceFacility::where('place_id', $place->id)->get();

            return response(
                view('booking.create', compact('fields','slug',
                    'place','field','duration','facilities','jam_diminta','hari_diminta','request_jam','request_hari','jam','hari'
                ))
            );
            
            // ->withCookie($cookie_place)
            // ->withCookie($cookie_field);
            // ->withCookie($cookie_url);
        }

        return redirect()->route('home');
    }

    public function store(Request $request, $slug)
    {
        $this->validate($request, [
            'user_id'   => 'exists:users,id',
            'duration'  => 'required|in:1,2,3,4,5,6,7,8,9,10',
            'payment'   => 'required|string',
        ]);

        $request_category   = $request->cookie('request_category');
        $request_regency    = $request->cookie('request_regency');
        $request_jam        = $request->cookie('request_jam');
        $request_hari       = $request->cookie('request_hari');
        $request_field      = $request->cookie('request_field');

        if (empty($request->cookie('request_jam'))) {
            session()->put('warning',"Maaf, ada kesalahan! Silahkan dicoba kembali");
            return redirect('/');
        }
        
        // jangan lupa cek database untuk booking jika sudah terbooking, berikan pilihan pada client
        if (Auth::user()->has_setup == 'true')
        {
            if (empty($request->cookie('request_jam'))) {
                alert()->warning('Maaf waktu kamu habis.', 'Terima kasih :)')->autoclose(5000);
                return redirect('/');
            }

            $cat            = Category::where('name', $request_category)->first();
            $jam_diminta    = Carbon::createFromFormat("H", $request_jam);
            $hari_diminta   = Carbon::createFromFormat("d-m-Y", $request_hari);
            $place          = Place::find($request->place_id);
            $field          = Field::find($request->field_id);
            $duration       = $request->duration;
            $facilities     = PlaceFacility::where('place_id', $place->id)->get();

            $bookings   = Booking::where('place_id', $place->id)->get();
            $booked     = $bookings->where('category_id', $cat->id)
            ->where('playing_date', $hari_diminta)
            ->where('playing_time', $jam_diminta)
            ->first();
            
            if(!empty($booked))
            {
                session()->put('warning','Maaf, Tidak bisa memesan 2 kali pada waktu yang sama');
                return redirect()->route('player.booklogs.index', Auth::user()->profile->username);
            }else{
                $booking                    = new Booking();
                $booking->player_id         = Auth::id();
                $booking->category_id       = $cat->id;
                $booking->place_id          = $place->id;
                $booking->field_id          = $field->id;
                $booking->price_field       = $request->price_field;
                $booking->regency_id        = $place->regency_id;
                $booking->subdistrict_id    = $place->subdistrict_id;
                $booking->playing_date      = $hari_diminta->toDateString();
                $booking->playing_time      = $jam_diminta->toTimeString();
                $booking->duration          = $duration;
                $booking->played            = 'false';
                $booking->game_type         = $request->game_type;
                $booking->save();
                
                $timer = Carbon::createFromFormat('H:i:s', $jam_diminta->toTimeString());
                for ($durasi=0; $durasi < $duration; $durasi++) 
                {
                    $booked = new BookedField();
                    $booked->subdistrict_id = $place->subdistrict_id;
                    $booked->booking_id     = $booking->id;
                    $booked->field_id       = $booking->field_id;
                    $booked->playing_date   = $booking->playing_date;
                    
                    if ($durasi < 1) {
                        $booked->playing_time  = $timer->addHour($durasi)->toTimeString();
                    } else {
                        $booked->playing_time  = $timer->addHour(1)->toTimeString();
                    }
                    
                    $booked->save();
                }
                
                $exp = explode(" ", $place->name);
                $acronym = "";
                foreach ($exp as $w) {
                    $acronym .= $w[0];
                }

                $inv            = 'INV.'.$booking->id.".$acronym".Auth::id().''.$bookings->count();
                $booking->code  = $inv;
                $booking->save();

                //  METHODE PEMBAYARAN
                $payment                = new BookingPayment();
                $payment->player_id     = Auth::id();
                $payment->booking_id    = $booking->id;
                $payment->method        = $request->payment;

                if ($booking->playing_time >= '18:00:00' ) {
                    $payment->amount        = $field->priceNight;
                    $payment->toFutsal      = $field->priceNight;
                }else{
                    $payment->amount        = $field->priceDayLight;
                    $payment->toFutsal      = $field->priceDayLight;
                }

                $payment->toCompany = 0;
                $payment->status    = 'undone';
                $payment->save();

                session()->put('success','Selamat, pemesanan lapangan Kamu berhasil, segera lakukan pembayaran :)');
                return redirect()->route('player.games.index', Auth::user()->profile->username);
            }
        }

        return redirect('/');
    }

    public function change_time(Request $request, $booking_code)
    {
        $rules = [
            'duration'    => 'required|in:1,2,3,4,5,6,7,8,9,10',
        ];

        $this->validate($request, $rules, [
            'duration.required'=>'Silahkan mengisi durasi bermain',
        ]);

        $request_hari       = $request->hari;
        $request_jam        = $request->jam;
        $request_duration   = $request->duration;
        
        $booking            = Booking::where('code',$booking_code)->first();

        if ($booking->player_id == Auth::id()) 
        {
            $regency            = Regency::where('name', $booking->category->name)->get()->first();
            $now                = Carbon::now('Asia/Jakarta');
            $batas_hari         = $now->addMonth();
            $jam_diminta        = Carbon::createFromFormat("H", $request_jam);
            $hari_diminta       = Carbon::createFromFormat("d-m-Y", $request_hari);
            $batas_hari         = $now->addMonth();
            
            $duration = array();
            for ($i=0; $i < $request_duration; $i++) 
            { 
                if($i < 1){
                    $inc_hours = Carbon::createFromFormat("H", $request_jam)->addHour(0)->toTimeString();
                }else{
                    $inc_hours = Carbon::createFromFormat("H", $request_jam)->addHour($i)->toTimeString();
                }
                
                array_push($duration, $inc_hours);
            }
            
            $place          = Place::find($booking->place_id);
            $category       = Category::find($place->category_id);

            $booked         = DB::table('booked_fields')
                            ->where('playing_date', $hari_diminta->toDateString())
                            ->whereIn('playing_time', $duration)
                            ->get();
                            
            $fields         = Field::where('place_id', $place->id)
                            ->whereNotIn('id', $booked->pluck('field_id')->all())
                            ->get();

            $booked_fields  = Field::where('place_id', $place->id)
                            ->whereIn('id', $booked->pluck('field_id')->all())
                            ->get();
    
            return response(
                view('booking.change_time.show', compact('duration',
                    'place','category','booked','fields', 'booked_fields','booking',
                    'now','jam_diminta','hari_diminta','request_jam','request_duration','batas_hari'
                    )
                )
            )
            ->cookie('booking_code', $booking->code, 60);
        } else {
            return abort(404);
        }
    }

    public function update_time(Request $request, $booking_code)
    {
        if (empty($request->cookie('booking_code'))) {
            alert()->warning('Maaf waktu kamu habis.', 'Silahkan coba kembali :)')->autoclose(5000);
            return redirect()->route('player.booklogs.index', Auth::user()->profile->username);
        }

        $jam_diminta    = Carbon::createFromFormat("H", $request->jam);
        $hari_diminta   = Carbon::createFromFormat("d-m-Y", $request->hari);
        $duration       = $request->duration;
        $field_id       = $request->field_id;

        $booking = Booking::where('code', $booking_code)->first();
        $booking->field_id      = $field_id;
        $booking->playing_date  = $hari_diminta->toDateString();
        $booking->playing_time  = $jam_diminta->toTimeString();
        $booking->duration      = $duration;
        
        $booked_fields = BookedField::where('booking_id', $booking->id)->get();
        $timer = Carbon::createFromFormat('H:i:s', $jam_diminta->toTimeString());
        
        foreach ($booked_fields as $booked) {
            $booked->delete();
        }

        for ($durasi=0; $durasi < $duration; $durasi++) 
        {
            $booked = new BookedField();
            $booked->booking_id     = $booking->id;
            $booked->subdistrict_id = $booking->place->subdistrict_id;
            $booked->field_id       = $booking->field_id;
            $booked->playing_date   = $booking->playing_date;
            
            if ($durasi < 1) {
                $booked->playing_time  = $timer->addHour($durasi)->toTimeString();
            } else {
                $booked->playing_time  = $timer->addHour(1)->toTimeString();
            }
            
            $booked->save();
        }
        
        $booking->save();

        session()->put('success','Selamat, Waktu booking Kamu telah ter-update :)');
        return redirect()->route('player.games.index', Auth::user()->profile->username);
    }

    public function cancel(Request $request, $booking_id)
    {
        $booking = Booking::destroy($booking_id);

        session()->put('success','Pesanan Kamu Sudah Dibatalkan');
        return redirect()->route('player.games.index', Auth::user()->profile->username);
        // return "sudah di cancel booking $booking_id";
    }

    

    /**
     * fungsi ajax untuk mendapatkan list tempat dari table "places" pada bagian triger searching
     */
    public function getPlaces(Request $request, $regency_id)
    {
        $places = DB::table("places")->where("regency_id", $regency_id)->pluck("name","id")->all();
        return json_encode($places);
    }

    public function index(Request $request)
    {
        Carbon::setLocale('id');
        $now = Carbon::now('Asia/Jakarta');

        if (!Gate::allows('futsal_owner-access') || Gate::allows('futsal_admin-access'))
        {
            $rules = [
                'regency' => 'required|integer|exists:regencies,id',
                'jam' => 'required',
                'hari' => 'required|date_format:"d-m-Y"',
            ];

            $this->validate($request, $rules, [
                'regency.required'=>'isi dengan daerah tempat kamu ingin bermain :)'
            ]);

            $request_url        = $request->fullUrl();
            $request_jam        = $request->get('jam');
            $request_hari       = $request->get('hari');
            $request_regency    = $request->get('regency_id');

            $convert_jam    = Carbon::createFromFormat("H", $request_jam);
            $convert_hari   = Carbon::createFromFormat("d-m-Y", $request_hari);
            $jam_diminta    = $convert_jam->format('H:i:s');
            $hari_diminta   = $convert_hari;
            $batas_hari     = $now->addMonth();

            $regency    = Regency::find($request_regency);
            $bookings   = StockField::where('regency_id', $regency->id)
                        ->where('playing_date', $convert_hari->toDateString())
                        ->where('playing_time','<=', $jam_diminta)
                        ->where('remain_field', 0)
                        ->pluck('place_id')
                        ->all();
                        // ->unique('place_id');
            $places     = Place::where('regency_id', $regency->id)
                        // ->where('open', '>=', $jam_diminta)
                        // ->where('close','<=', $jam_diminta)
                        ->whereNotIn('id', $bookings)
                        ->get();

            return view('booking.search', compact(
                'convert_jam',
                'convert_hari',
                'hari_diminta',
                'jam_diminta',
                'batas_hari',
                'regency',
                'places',
                'bookings',
                'request_hari',
                'request_jam',
                'now'
            ));
            // return response(
            //     view('booking.result',compact(
            //         'convert_jam','convert_hari','hari_diminta','jam_diminta','batas_hari','regency','places','bookings')))
            //         ->cookie('fullUrl', $request_url, 120)
            //         ->cookie('ip', $request->getClientIp(), 120);

        }

        return redirect('home');
    }

}

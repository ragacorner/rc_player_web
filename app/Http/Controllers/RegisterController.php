<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Profile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('as:player_setup');
    }

    public function create()
    {
        return view('player.register.address.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            // 'name' => 'required|string',
            // 'phone' => 'required',
            'regency_id' => 'required|exists:regencies,id',
            'subdistrict_id' => 'required|exists:subdistricts,id',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $profile = Profile::create([
            'user_id'       => Auth::id(),
            'address'       => $request->address,
            'regency_id'    => $request->regency_id,
            'subdistrict_id'=> $request->subdistrict_id,
        ]);

        if ($request->hasFile('photo')) {
            $imageName = $profile->id.'_'.md5(time()).'.'.$request->photo->getClientOriginalExtension();
            $request->photo->move(public_path('img/player'), $imageName);
            $profile->photo = $imageName;
            $profile->save();
        }

        $player = User::find(Auth::id());
        $player->name = $request->name;
        $player->phone = $request->phone;
        $player->is_booker = 'false';
        $player->save();

        return redirect('/');
    }
}

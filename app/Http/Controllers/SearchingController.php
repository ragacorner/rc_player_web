<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
// use App\Player;
// use GuzzleHttp\Client;
// use App\Models\Province;
// use App\Models\StockField;
// use App\Models\Subdistrict;
// use Illuminate\Support\Facades\Gate;
use App\Models\Field;
use App\Models\Place;
use App\Models\Booking;
use App\Models\Regency;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class SearchingController extends Controller
{
    /**
     * FUNCTION REGENCY 'search/reg'
     * pencarian yang diambil dari property triger dan menampilkan tempat yg diinginkan
     * 1. validasi form
     * 2. pintu otorisasi
     * 3. ambil request triger form
     * 4. convert jam dan hari dengan carbon
     * 5. panggil masing2 database yg bersangkutan
     */

    public function regency(Request $request)
    {
        $rules = [
            'category'      => 'required|string|exists:categories,name',
            'hari'          => 'required|date_format:"d-m-Y"',
            'jam'           => 'required|date_format:"H"',
            'durasi'        => 'required|in:1,2,3,4,5,6,7,8,9,10',
        ];

        $this->validate($request, $rules, [
            'regency.required'=>'isi dengan kota/kabupaten kamu ingin bermain :)',
            'durasi.required'=>'Durasi harus berupa angka :)',
        ]);
        
        $request_regency    = $request->regency;
        $request_category   = $request->category;
        $request_hari       = $request->hari;
        $request_jam        = $request->jam;
        $request_duration   = $request->durasi;

        $now                = Carbon::now('Asia/Jakarta');
        $batas_hari         = $now->addMonth();
        $jam_diminta        = Carbon::createFromFormat("H", $request_jam);
        $hari_diminta       = Carbon::createFromFormat("d-m-Y", $request_hari);

        $category       = DB::table('categories')->where('name',$request_category)->first();
        $regency        = DB::table('regencies')->where('name',$request_regency)->first();
        $regency_displayname = 'di '.$regency->display_name;

        if (Auth::check()) {
            $booked_played  = DB::table('bookings')->where('player_id', Auth::id())
                            ->where('played','false')
                            ->where('category_id', $category->id)
                            ->pluck('category_id')
                            ->first();

            if(!empty($booked_played)){
                session()->put('warning',"Maaf, Kamu punya permainan ".$category->display_name." yang belum diselesaikan");
                return redirect('/');
            }
        }

        $duration = array();
        for ($i=0; $i < $request_duration; $i++) 
        { 
            if($i < 1){
                $inc_hours = Carbon::createFromFormat("H", $request_jam)->addHour(0)->toTimeString();
            }else{
                $inc_hours = Carbon::createFromFormat("H", $request_jam)->addHour($i)->toTimeString();
            }

            array_push($duration, $inc_hours);
        }
     
        $owners         = DB::table('owners')
                            ->where('verified', 1)
                            ->pluck('id')
                            ->all();

        $places         = DB::table('places')
                            ->where('category_id', $category->id)
                            ->where('regency_id', $regency->id)
                            ->whereIn('owner_id', $owners)
                            ->get();

        $booked         = DB::table('booked_fields')
                            ->where('playing_date', $hari_diminta->toDateString())
                            ->whereIn('playing_time', $duration)
                            ->where('deleted_at', null)
                            ->get()->sortBy('field_id');

        $fields         = Field::whereIn('place_id', $places->pluck('id')->all())
                            ->whereNotIn('id', $booked->pluck('field_id')->all())
                            ->get()
                            ->unique('place_id');


        Cookie::queue('request_regency', $request->regency, 40320);
        return response(
            view('searching.regency', compact('booked','duration',
                'request_category','request_regency','request_hari','request_jam','request_duration',
                'now','jam_diminta','hari_diminta','batas_hari',
                'category','regency','regency_displayname','stock_fields','places','place','fields','subdistricts'
                )
            )
        )
        ->cookie('request_category', $request_category, 60)
        ->cookie('request_regency', $request_regency, 60)
        ->cookie('request_jam', $jam_diminta->format('H'), 60)
        ->cookie('request_hari', $hari_diminta->format('d-m-Y'), 60);
        
    }

    public function place(Request $request, $place_slug)
    {
        $rules = [
            'jam'           => 'required|date_format:"H"',
            'hari'          => 'required|date_format:"d-m-Y"',
        ];

        $request_regency    = $request->regency;
        $request_hari       = $request->hari;
        $request_jam        = $request->jam;
        $request_duration   = $request->duration;
        
        $now            = Carbon::now();
        $jam_diminta    = Carbon::createFromFormat("H", $request_jam);
        $hari_diminta   = Carbon::createFromFormat("d-m-Y", $request_hari);
        $place          = Place::where('slug',$place_slug)->first();
        $regency        = Regency::where('name', $request_regency)->first();

        if($request_jam < $now->format('H') && $request_hari <= $now->format('d-m-Y')){
            session()->put('warning',"Maaf, Tidak bisa memesan pada waktu yang telah lewat");
            return redirect('/');
        }

        if (Auth::check()) {
            $booked_played  = Booking::where('player_id', Auth::id())
                            ->where('played','false')
                            ->where('category_id', $place->category->id)
                            ->pluck('category_id')
                            ->first();

            if(!empty($booked_played)){
                session()->put('warning',"Maaf, Harap menyelesaikan permainan ".$place->category->display_name." terlebih dahulu");
                return redirect('/');
            }
        }

        if(Cookie::has('request_jam') && Cookie::has('request_hari'))
        {
            if (!empty($place)) {
                $category       = Category::find($place->category_id);
            } else {
                return abort(404);
            }

            $owners          = DB::table('owners')
                            ->where('verified', 1)
                            ->pluck('id')
                            ->all();

            $bookings       = DB::table('bookings')
                            ->where('subdistrict_id', $place->subdistrict_id)
                            ->where('playing_date', $hari_diminta->toDateString())
                            ->where('playing_time', $jam_diminta->toTimeString())
                            ->get();
                            
            $places         = DB::table('places')
                            ->where('category_id', $category->id)
                            ->where('regency_id', $regency->id)
                            ->whereNotIn('id', $bookings->pluck('place_id')->all())
                            ->whereIn('owner_id', $owners)
                            ->where('id','!=',$place->id)
                            ->get()
                            ->random(5);

            $booked         = DB::table('booked_fields')
                            ->where('playing_date', $hari_diminta->toDateString())
                            ->where('playing_time', $jam_diminta->toTimeString())
                            ->get();
            
            $fields         = DB::table('fields')
                            ->where('place_id', $place->id)
                            ->whereNotIn('id', $booked->pluck('field_id')->all())
                            ->get();

            $booked_fields  = DB::table('fields')
                            ->where('place_id', $place->id)
                            ->whereIn('id', $booked->pluck('field_id')->all())
                            ->get();
    
            return response(
                    view('searching.place', compact('booked_fields','booked','bookings','request_hari','request_jam',
                    'category','stock_fields','place','places','fields','jam_diminta','hari_diminta','request_regency','request_duration'
                )
            ));
            
        }else{
            return redirect()->route('searching.landing');
        }
        
    }
}

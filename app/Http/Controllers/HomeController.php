<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Player;
use App\Models\Booking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function home(Request $request)
    {
        $booked_user = Booking::where('player_id', Auth::id())->get();
        $now = Carbon::now('Asia/Jakarta');

        if(count($booked_user) > 0){
            foreach($booked_user->where('has_played','false') as $booked){
                if($now->toTimeString() > $booked->playing_time && $now->toDateString() >= $booked->playing_date){
                    $booked->played = 'true';
                    $booked->save();
                }
            }
        }

        if(Auth::user()->has_setup == 'false')
        {
            return redirect()->route('setup.profile.create');
        }
        elseif(Auth::user()->has_setup == 'true')
        {
            return redirect('/');
        }

        return abort(404);
    }

    public function getPlaces(Request $request, $regency_id)
    {
        $subdistricts = DB::table("subdistricts")->where("regency_id", $regency_id)->pluck("display_name","id")->all();
        return json_encode($subdistricts);
    }
}

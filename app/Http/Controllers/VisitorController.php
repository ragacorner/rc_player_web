<?php

namespace App\Http\Controllers;

use Alert;
use App\Models\Visitor;
use App\Models\VisitorMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class VisitorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return redirect()->action('VisitorController@comingsoon')->withCookie('ip', $request->getClientIp());
    }

    public function comingsoon(Request $request)
    {
        $hit = 1 + $request->cookie('hits');
        $user_ip = $request->getClientIp();
        
        // $visitor = Visitor::where('ip', $user_ip)->get()->last();
        // $visitor->ip = $user_ip;
        // $visitor->last_visited = \Carbon\Carbon::now();
        // $visitor->hits = $hit;
        // $visitor->save();
        // if (!empty($visitor) > 0) {
        // }

        $counter = $hit - 1;
        return response(view('comingsoon', compact('counter')))->withCookie('hits', $hit);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'visitor_message' => 'required|string'
        ]);

        $visitor = Visitor::where('ip', $request->getClientIp())->first();
        
        if(empty($visitor)){
            $visitor                = new Visitor();
            $visitor->ip            = $request->getClientIp();
            $visitor->last_visited  = \Carbon\Carbon::now();
            $visitor->hits          = 1;
        }else{
            $visitor->ip            = $request->getClientIp();
            $visitor->last_visited  = \Carbon\Carbon::now();
            $visitor->hits          = 1 + $request->cookie('hits');
        }

        $visitor->save();

        $message = new VisitorMessage();
        $message->visitor_id = $visitor->id;
        $message->message = $request->visitor_message;
        $message->save();
        
        alert()->success('Telah menunggu kami, pesanmu berhasil tersimpan di database kami, akan kami gunakan pesan kamu dengan bijak.', 'Terima kasih :)')->autoclose(5000);
        return redirect()->action('VisitorController@comingsoon');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Visitor  $visitor
     * @return \Illuminate\Http\Response
     */
    public function show(Visitor $visitor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Visitor  $visitor
     * @return \Illuminate\Http\Response
     */
    public function edit(Visitor $visitor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Visitor  $visitor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Visitor $visitor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Visitor  $visitor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Visitor $visitor)
    {
        //
    }
}

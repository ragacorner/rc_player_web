<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Place;
use App\Player;
use App\Models\Booking;
use App\Models\PlayerProfile;
use App\Models\Regency;
use App\Models\Province;
use App\Models\Subdistrict;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('as:player');
    }

    public function show(Request $request, $slug)
    {
        $player = Player::find(Auth::id());
        $profile = $player->profile;

        $player_area    = ucwords($profile->area);
        $player_subd    = Subdistrict::find($profile->subdistrict_id)->display_name;
        $player_regency = Regency::find($profile->regency_id)->display_name;
        
        $now    = Carbon::now();
        $lahir  = Carbon::createFromFormat('Y-m-d', $profile->birthday);
        $umur   = $lahir->diffInYears($now);

        $bookings       = Booking::where('player_id', Auth::id())->get()->sortByDesc('updated_at');

        if (!empty($player)) 
        {
            return view('player.profiles.show', compact('now','lahir',
                'player','umur','profile','bookings','player_area','player_subd','player_regency'
            ));
        }

        return redirect()->route('home');
    }

    public function edit(Request $request, $slug)
    {
        $player = Player::find(Auth::id());
        $profile = $player->profile;

        $player_area    = ucwords($profile->area);
        $player_subd    = Subdistrict::find($profile->subdistrict_id)->display_name;
        $player_regency = Regency::find($profile->regency_id)->display_name;

        $places         = Place::all();
        $provinces      = Province::whereIn('id', $places->unique('province_id')->pluck('province_id'))->get();
        $regencies      = Regency::whereIn('id', $places->unique('regency_id')->pluck('regency_id'))->get();

        if (!empty($player)) 
        {
            return view('player.profiles.edit', compact(
                'player','profile','bookings','player_area','player_subd','player_regency','provinces','regencies'
            ));
        }


        return redirect()->route('home');
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'phone' => 'required',
            'email' => 'required',
            'gender' => 'required|in:male,female',
            'category_id' => 'required|exists:categories,id',
            'regency_id' => 'required|exists:regencies,id',
            'subdistrict_id' => 'required|exists:subdistricts,id',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $tgl_lahir = Carbon::createFromFormat('m/d/Y', $request->birthday)->toDateString();

        $player = Player::find(Auth::id());
        $player->name       = $request->name;
        $player->email      = $request->email;
        $player->phone      = $request->phone;
        $player->save();

        $profile                    = $player->profile;
        $profile->gender            = $request->gender;
        $profile->username          = $request->username;
        $profile->description       = $request->description;
        $profile->occupation        = $request->occupation;
        $profile->birthday          = $tgl_lahir;
        $profile->category_id       = $request->category_id;
        $profile->area              = $request->area;
        $profile->regency_id        = $request->regency_id;
        $profile->subdistrict_id    = $request->subdistrict_id;
        $profile->save();
        
        if ($request->hasFile('photo')) {
            $destinationPath1 = public_path('/img/player/500x500');
            $destinationPath2 = public_path('/img/player/800x800');
            $img = Image::make($request->file('photo')->getRealPath());
            
            if ($profile->photo !== '') $this->deletePhoto($profile->photo, $profile->username);

            $img->resize(500, 500, function ($constraint) {
                $constraint->aspectRatio();
            })->crop(500, 500)->save($destinationPath1.'/'.$profile->username.'.jpg');

            $img->resize(800, 800, function ($constraint) {
                $constraint->aspectRatio();
            })->crop(800, 800)->save($destinationPath2.'/'.$profile->username.'.jpg');

            $profile->photo = $this->savePhoto($request->file('photo'));
            $profile->save();
        }

        session()->put('success','Profile Kamu Berhasil Diupdate.');
        return redirect()->route('player.profile.show', Auth::user()->profile->username);
    }

    protected function savePhoto(UploadedFile $photo) 
    {
        $fileName = str_random(40) . '.' . $photo->guessClientExtension(); 
        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/player/original'; 
        $photo->move($destinationPath, $fileName);

        return $fileName;
    }

    public function deletePhoto($filename, $username) {
        $path3 = public_path() . DIRECTORY_SEPARATOR . 'img/player/800x800' . DIRECTORY_SEPARATOR . $username.'.jpg';
        $path2 = public_path() . DIRECTORY_SEPARATOR . 'img/player/500x500' . DIRECTORY_SEPARATOR . $username.'.jpg';
        $path1 = public_path() . DIRECTORY_SEPARATOR . 'img/player/original' . DIRECTORY_SEPARATOR . $filename;
        
        File::delete($path3);
        File::delete($path2);
        File::delete($path1);

        return $path1;
    }
}

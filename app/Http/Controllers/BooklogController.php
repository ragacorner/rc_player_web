<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Place;
use App\Models\Booking;
use App\Models\Regency;
use App\Models\Subdistrict;
use Illuminate\Http\Request;
use App\Models\BookingPayment;
use App\Models\BookingProduct;
use Illuminate\Support\Facades\Auth;

class BooklogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('as:player');
    }

    public function index(Request $request, $id)
    {
        $user           = Auth::user()->profile;

        if (!empty($user)) 
        {    
            $player_area    = ucwords($user->area);
            $player_subd    = Subdistrict::find($user->subdistrict_id)->display_name;
            $player_regency = Regency::find($user->regency_id)->display_name;
            $bookings       = Booking::where('player_id', Auth::id())
                            // ->where('played','true')
                            // ->get()
                            ->orderBy('created_at','Desc')->paginate(3);
            
            return view('player.booklogs.index', compact(
                'bookings','user','player_area','player_subd','player_regency'
            ));
        }

        return redirect()->route('home');
    }

    public function invoice(Request $request, $player, $slug)
    {
        $player = Auth::user();
        $place = Place::where('slug', $slug)->first();
        $booking = Booking::where('player_id', Auth::id())->where('place_id', $place->id)->get()->sortByDesc('created_at')->first();
        $products = BookingProduct::where('booking_id', $booking->id)->get();
        $payments = BookingPayment::where('booking_id', $booking->id)->get();
        $inv = Carbon::createFromFormat('Y-m-d H:i:s', $booking->created_at)->format('l, d M Y');
        
        return view('player.booklogs.invoice', compact('player','place','booking','inv','products','payments','slug'));
    }
}

<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Field;
use App\Models\Place;
use GuzzleHttp\Client;
use App\Models\Booking;
use App\Models\Regency;
use App\Models\Category;
use App\Models\Province;
use App\Models\StockField;
use App\Models\Subdistrict;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Cookie;

class SearchingController extends Controller
{
    

    /**
     * pencarian yang diambil dari property triger dan menampilkan tempat yg diinginkan
     * 1. validasi form
     * 2. pintu otorisasi
     * 3. ambil request triger form
     * 4. convert jam dan hari dengan carbon
     * 5. panggil masing2 database yg bersangkutan
     */

    public function regency(Request $request)
    {
        $rules = [
            'category'      => 'required|string|exists:categories,name',
            'regency'       => 'required|string|exists:regencies,name',
            'jam'           => 'required|date_format:"H"',
            'hari'          => 'required|date_format:"d-m-Y"',
        ];

        $this->validate($request, $rules, [
            'category.required'=>'pilih category terlebih dahulu :)',
            'regency.required'=>'isi dengan kota/kabupaten kamu ingin bermain :)',
        ]);

        $request_category   = $request->category;
        $request_regency    = $request->regency;
        $request_jam        = $request->jam;
        $request_hari       = $request->hari;

        $now                = Carbon::now('Asia/Jakarta');
        $batas_hari         = $now->addMonth();
        $jam_diminta        = Carbon::createFromFormat("H", $request_jam);
        $hari_diminta       = Carbon::createFromFormat("d-m-Y", $request_hari);

        $category       = Category::where('name',$request_category)->get()->first();
        $regency        = Regency::where('name',$request_regency)->get()->first();

        if (Auth::check()) {
            $booked_played  = Booking::where('player_id', Auth::id())
                            ->where('played','false')
                            ->where('category_id', $category->id)
                            ->pluck('category_id')
                            ->first();

            if(!empty($booked_played)){
                return abort(404);
            }
        }
     
        $owners         = DB::table('owners')
                            ->where('verified', 1)
                            ->pluck('id')
                            ->all();

        $places         = DB::table('places')
                            ->where('category_id', $category->id)
                            ->where('regency_id', $regency->id)
                            ->whereIn('owner_id', $owners)
                            ->get();

        $booked         = DB::table('booked_fields')
                            ->where('playing_date', $hari_diminta->toDateString())
                            ->where('playing_time', $jam_diminta->toTimeString())
                            ->get()->sortBy('field_id');

        $fields         = Field::whereIn('place_id', $places->pluck('id')->all())
                            ->whereNotIn('id', $booked->pluck('field_id')->all())
                            ->get()
                            ->unique('place_id');

        return response(
            view('searching.regency', compact('booked',
                'request_category','request_regency','request_hari','request_jam',
                'now','jam_diminta','hari_diminta','batas_hari',
                'category','regency','stock_fields','places','place','fields','subdistricts'
                )
            )
        )
        ->cookie('request_category', $request_category, 60)
        ->cookie('request_regency', $request_regency, 60)
        ->cookie('request_jam', $jam_diminta->format('H'), 60)
        ->cookie('request_hari', $hari_diminta->format('d-m-Y'), 60);
        
    }

    public function place(Request $request, $place_slug)
    {
        $rules = [
            'jam'           => 'required|date_format:"H"',
            'hari'          => 'required|date_format:"d-m-Y"',
        ];

        $request_regency    = $request->regency;
        $request_jam        = $request->jam;
        $request_hari       = $request->hari;
        
        $now            = Carbon::now();
        $jam_diminta    = Carbon::createFromFormat("H", $request_jam);
        $hari_diminta   = Carbon::createFromFormat("d-m-Y", $request_hari);
        $place          = Place::where('slug',$place_slug)->first();
        $regency        = Regency::where('name', $request_regency)->first();

        if($request_jam < $now->format('H') && $request_hari <= $now->format('d-m-Y')){
            session()->put('warning',"Maaf, Tidak bisa memesan pada waktu yang telah lewat");
            return redirect('/');
        }

        if (Auth::check()) {
            $booked_played  = Booking::where('player_id', Auth::id())
                            ->where('played','false')
                            ->where('category_id', $place->category->id)
                            ->pluck('category_id')
                            ->first();

            if(!empty($booked_played)){
                session()->put('warning',"Maaf, Harap menyelesaikan permainan ".$place->category->display_name." terlebih dahulu");
                return redirect('/');
            }
        }

        if(Cookie::has('request_jam') && Cookie::has('request_hari'))
        {
            if (!empty($place)) {
                $category       = Category::find($place->category_id);
            } else {
                return abort(404);
            }

            $owners          = DB::table('owners')
                            ->where('verified', 1)
                            ->pluck('id')
                            ->all();

            $bookings       = DB::table('bookings')
                            ->where('subdistrict_id', $place->subdistrict_id)
                            ->where('playing_date', $hari_diminta->toDateString())
                            ->where('playing_time', $jam_diminta->toTimeString())
                            ->get();
                            
            $places         = DB::table('places')
                            ->where('category_id', $category->id)
                            ->where('regency_id', $regency->id)
                            ->whereNotIn('id', $bookings->pluck('place_id')->all())
                            ->whereIn('owner_id', $owners)
                            ->where('id','!=',$place->id)
                            ->get()
                            ->random(5);

            $booked         = DB::table('booked_fields')
                            ->where('playing_date', $hari_diminta->toDateString())
                            ->where('playing_time', $jam_diminta->toTimeString())
                            ->get();
            
            $fields         = DB::table('fields')
                            ->where('place_id', $place->id)
                            ->whereNotIn('id', $booked->pluck('field_id')->all())
                            ->get();

            $booked_fields  = DB::table('fields')
                            ->where('place_id', $place->id)
                            ->whereIn('id', $booked->pluck('field_id')->all())
                            ->get();
    
            return response(
                    view('searching.place', compact('booked_fields','booked','bookings','request_hari','request_jam',
                    'category','stock_fields','place','places','fields','jam_diminta','hari_diminta','request_regency'
                )
            ));
            
        }else{
            return redirect()->route('searching.landing');
        }
        
    }
}

<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Place;
use App\Models\Booking;
use App\Models\Regency;
use App\Models\Subdistrict;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ActivityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('as:player');
    }

    public function index(Request $request, $id)
    {
        $user           = Auth::user()->profile;

        if (!empty($user)) 
        {    
            $player_area    = ucwords($user->area);
            $player_subd    = Subdistrict::find($user->subdistrict_id)->display_name;
            $player_regency = Regency::find($user->regency_id)->display_name;
            $bookings       = Booking::where('player_id', Auth::id())->get()->sortByDesc('updated_at');
            
            return view('player.activities.index', compact(
                'bookings','user','player_area','player_subd','player_regency'
            ));
        }

        return redirect()->route('home');
    }

    public function invoice(Request $request, $slug)
    {
        $place = Place::where('slug', $slug)->first();
        $booking = Booking::where('player_id', Auth::id())->get()->sortByDesc('created_at')->first();
        $inv = Carbon::createFromFormat('Y-m-d H:i:s', $booking->created_at)->format('l, d M Y');
        
        return view('activities.invoice', compact('place','booking','inv'));
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     $user = Auth::user()->profile;
    //     $player_area = $user->area;
    //     $player_subd    = Subdistrict::find($user->subdistrict_id)->display_name;
    //     $player_regency = Regency::find($user->regency_id)->display_name;
    //     $bookings       = Booking::where('player_id', Auth::id())->get()->sortByDesc('updated_at');
        
    //     return view('activities.index', compact(
    //         'bookings','user','player_area','player_subd','player_regency'
    //     ));
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

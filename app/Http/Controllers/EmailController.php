<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class EmailController extends Controller
{
    public function sendEmail(Request $request)
    {
        try{
            Mail::send('email', ['nama' => $request->nama, 'pesan' => $request->pesan], function ($message) use ($request)
            {
                $message->subject($request->judul);
                $message->from('ragacorner@gmail.com', 'Ragacorner Team');
                $message->to($request->email);
            });

            alert()->success("Link Verifikasi telah dikirim ke email ".$request->email, 'Berhasil')->autoclose(5000);
            return redirect()->back();
        }
        catch (Exception $e){
            return response (['status' => false,'errors' => $e->getMessage()]);
        }

    }
}

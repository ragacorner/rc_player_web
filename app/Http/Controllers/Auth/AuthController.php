<?php

namespace App\Http\Controllers\Auth;

use App\Models\Player;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller
{

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();
        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser, true);
        return redirect('/');
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  Player
     */
    public function findOrCreateUser($player, $provider)
    {
        $authUser = Player::where('provider_id', $player->id)->first();
        if ($authUser) {
            return $authUser;
        }
        else{
            $data = Player::create([
                'name'     => $player->name,
                'email'    => !empty($player->email)? $player->email : '' ,
                'password' => Hash::make('secret'),
                'verified' => Player::VERIFIED_USER,
                'verification_token' => Player::generateVerificationCode(),
                'provider' => $provider,
                'provider_id' => $player->id
            ]);

            alert()->warning("Kamu baru saja berhasil daftar melalui ".ucwords($provider).", silahkan login kembali", 'Selamat!')->autoclose(5000);
            return $data;
        }
    }
}
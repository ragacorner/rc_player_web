<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer([
            'landing',
            'searching.regency',
            'searching.place',
            'booking.create',
            'settings.setup.create',
            'player.profiles.edit',
            'booklogs.index',
            'places.show',
            'searching.locale',
        ], 'App\Http\ViewComposers\LocationsComposer');

        View::composer([
            'landing',
            'searching.regency',
            'searching.place',
            'booking.create',
            'booklogs.index',
            'settings.setup.create',
            'player.profiles.edit',
        ], 'App\Http\ViewComposers\CategoriesComposer');

        View::composer([
            'landing',
            'searching.regency',
            'searching.place',
            'booking.create',
            'booklogs.index',
            'player.profiles.edit',
            'places.show',
            'booking.change_time.show',
            'player.games.index'
        ], 'App\Http\ViewComposers\TimesComposer');

        View::composer([
            'owner.layouts.app',
            'admin.layouts.app',
        ], 'App\Http\ViewComposers\NotificationsComposer');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

<?php

namespace App\Models;

use App\Models\Field;
use App\Models\Booking;
use App\Models\Subdistrict;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookedField extends Model
{
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
    protected $table = 'booked_fields';
    protected $fillable = ['subdistrict_id','booking_id','field_id','playing_date','playing_time'];

    public function subdistrict()
    {
        return $this->belongsTo(Subdistrict::class);
    }
    
    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }

    public function field()
    {
        return $this->belongsTo(Field::class);
    }
}

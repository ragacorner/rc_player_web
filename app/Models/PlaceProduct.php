<?php

namespace App\Models;

use App\Models\Place;
use App\Models\PlaceMasterProduct;
use Illuminate\Database\Eloquent\Model;

class PlaceProduct extends Model
{
    protected $table = 'place_products';
    protected $fillable = ['place_id','master_id','qty'];

    public function place()
    {
        return $this->belongsTo(Place::class);
    }

    public function master()
    {
        return $this->belongsTo(PlaceMasterProduct::class);
    }
}

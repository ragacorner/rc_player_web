<?php

namespace App\Models;

use App\Player;
use App\Models\User;
use App\Models\Place;
use App\Models\Regency;
use App\Models\Category;
use App\Models\Province;
use App\Models\BookedField;
use App\Models\Subdistrict;
use App\Models\BookingPayment;
use App\Models\BookingProduct;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use SoftDeletes;

    const PAYED = 'paid';
    const UNPAY = 'unpaid';
    
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'code','player_id','category_id','place_id','field_id','pricebook','subdistrict_id',
        'playing_date','playing_time','duration','paid_half','paid_full','played','book_at_place',
    ];

    public static function boot() {
        parent::boot();

        static::deleting(function($booking) { // before delete() method call this
             $booking->booked_fields()->delete();
             $booking->payment()->delete();
             // do the rest of the cleanup...
        });
    }

    public function isPaid()
    {
        return $this->status == Booking::PAYED;
    }

    // HAS ================================================

    public function payment()
    {
        return $this->hasOne(BookingPayment::class);
    }

    public function products()
    {
        return $this->hasMany(BookingProduct::class);
    }

    public function booked_fields()
    {
        return $this->hasMany(BookedField::class);
    }

    // BELONGS TO =========================================

    public function player()
    {
        return $this->belongsTo(Player::class);
    }

    public function place()
    {
        return $this->belongsTo(Place::class);
    }

    public function field()
    {
        return $this->belongsTo(Field::class);
    }

    public function subdistrict()
    {
        return $this->belongsTo(Subdistrict::class);
    }

    public function regency()
    {
        return $this->belongsTo(Regency::class);
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}

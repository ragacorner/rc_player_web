<?php

namespace App\Models;

use App\Models\Booking;
use App\Player;
use Illuminate\Database\Eloquent\Model;

class BookingPayment extends Model
{
    public $timestamps = false;
    protected $table = 'booking_payments';
    protected $fillable = [
        'player_id','booking_id','method','amount','toFutsal','toCompany','status'
    ];

    public function players()
    {
        return $this->belongsTo(Player::class);
    }

    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }
}

<?php

namespace App\Models;

use App\Models\Bank;
use App\Models\Place;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'place_id','bank_id','no_atm',
    ];

    public function place()
    {
        return $this->belongsTo('App\Place');
    }

    public function bank()
    {
        return $this->belongsTo('App\Bank');
    }
}

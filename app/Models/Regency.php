<?php

namespace App\Models;

use RajaOngkir;
use App\Models\Place;
use App\Models\Profile;
use App\Models\Province;
use App\Models\StockField;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Regency extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['id', 'province_id', 'name','display_name'];

    // public static function populate() {
    //     foreach (RajaOngkir::Kota()->all() as $kota) {
    //         $model = static::firstOrNew(['id' => $kota['city_id']]);
    //         $model->province_id = $kota['province_id'];
    //         $model->name = $kota['type'] . ' ' . $kota['city_name'];
    //         $model->save();
    //     }
    // }
    //
    public function places()
    {
        return $this->hasMany(Place::class);
    }

    public function profile()
    {
        return $this->hasMany(Profile::class);
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }
    public function stock_fields()
    {
        return $this->hasMany(StockField::class);
    }
}

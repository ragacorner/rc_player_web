<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = ['user_id','name','display_name','url','status'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

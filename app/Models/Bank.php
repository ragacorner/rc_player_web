<?php

namespace App\Models;

use App\Models\Account;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bank extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['name', 'display_name'];

    public function accounts()
    {
        return $this->hasMany('App\Models\Account');
    }

    public function places()
    {
        return $this->belongsToMany('App\Models\Place');
    }
}

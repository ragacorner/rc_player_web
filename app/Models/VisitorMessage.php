<?php

namespace App\Models;

use App\Models\Visitor;
use Illuminate\Database\Eloquent\Model;

class VisitorMessage extends Model
{
    protected $fillable = ['visitor_id','message'];

    public function visitor()
    {
        return $this->belongsTo(Visitor::class);
    }
}

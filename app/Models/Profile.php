<?php

namespace App\Models;

use App\Player;
use App\Models\Regency;
use App\Models\Category;
use App\Models\Province;
use App\Models\Subdistrict;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'player_id','gender','username','description','photo','birthday','category_id','occupation','area','subdistrict_id','regency_id',
    ];

    public function setUsernameAttribute($slug)
    {
        $string = str_replace(' ', '-', $slug);
        $this->attributes['username'] = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $string));
    }

    public function getDescriptionAttribute($value)
    {
        return ucwords($value);
    }

    public function getAreaAttribute($value)
    {
        return ucwords($value);
    }

    public function player()
    {
        return $this->belongsTo(Player::class);
    }
    
    public function subdistrict()
    {
        return $this->belongsTo(Subdistrict::class);
    }

    public function regency()
    {
        return $this->belongsTo(Regency::class);
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}

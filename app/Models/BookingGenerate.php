<?php

namespace App\Models;

use App\Models\Field;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookingGenerate extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = "booking_generates";
    protected $fillable = ['place_id','field_id','player_email','code','playing_time','playing_date','duration'];

    public function field()
    {
        return $this->belongsTo(Field::class);
    }
}

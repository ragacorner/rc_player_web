<?php

namespace App\Models;

use App\Player;
use App\Models\Booking;
use App\Models\PlaceFacility;
use Illuminate\Database\Eloquent\Model;

class BookingFacility extends Model
{
    protected $table = 'booking_facilities';
    protected $fillable = ['player_id','booking_id','facility_id','qty','amount','paid'];

    public function player()
    {
        return $this->belongsTo(User::class);
    }

    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }

    public function facility()
    {
        return $this->belongsTo(PlaceFacility::class);
    }
}

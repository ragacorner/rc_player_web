<?php

namespace App\Models;

use App\Models\Owner;
use App\Models\Player;
use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    protected $table = ['sender_id','receiver_id','title','content'];

    public function sender()
    {
        return $this->belongsTo(Player::class);
    }
    public function receiver()
    {
        return $this->belongsTo(Owner::class);
    }
}

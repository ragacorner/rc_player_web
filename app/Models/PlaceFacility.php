<?php

namespace App\Models;

use App\Models\Place;
use Illuminate\Database\Eloquent\Model;

class PlaceFacility extends Model
{
    protected $table = 'place_facilities';
    protected $fillable = ['place_id','item','price','qty'];

    public function getItemAttribute($value)
    {
        return ucwords($value);
    }

    //==========================================================

    public function place()
    {
        return $this->belongsTo(Place::class);
    }
}

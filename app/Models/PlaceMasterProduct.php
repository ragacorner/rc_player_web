<?php

namespace App\Models;

use App\Models\Place;
use Illuminate\Database\Eloquent\Model;

class PlaceMasterProduct extends Model
{
    protected $table = 'place_master_products';
    protected $fillable = ['place_id','name','price'];

    public function place()
    {
        return $this->belongsTo(Place::class);
    }
}

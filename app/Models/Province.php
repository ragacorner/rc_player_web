<?php

namespace App\Models;

use RajaOngkir;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Province extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['id', 'name','display_name'];

    // public static function populate() {
    //     foreach (RajaOngkir::Provinsi()->all() as $province) {
    //         $model = static::firstOrNew(['id' => $province['province_id']]);
    //         $model->name = $province['province'];
    //         $model->save();
    //     }
    // }
    //
    public function profile()
    {
        return $this->hasMany('App\Models\Profile');
    }

    public function places()
    {
        return $this->hasMany('App\Models\Place');
    }

    public function regencies()
    {
        return $this->hasMany('App\Models\Regency');
    }
}

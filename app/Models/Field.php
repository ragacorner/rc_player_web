<?php

namespace App\Models;

use App\Models\Owner;
use App\Models\Place;
use App\Models\Booking;
use App\Models\StockField;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Field extends Model
{
    use SoftDeletes;

    protected $table = 'fields';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'place_id',
        'name',
        'grass_type',
        'priceDayLight', 
        'priceNight',
        'width',
        'length', 
    ];

    public function getNameAttribute($value)
    {
        return ucwords($value);
    }

    public function setPriceDayLightAttribute($siang)
    {
        $harga = str_replace('.', '', $siang);
        $this->attributes['priceDayLight'] = $harga;
    }

    public function setPriceNightAttribute($malam)
    {
        $harga = str_replace('.', '', $malam);
        $this->attributes['priceNight'] = $harga;
    }

    public function stock_fields()
    {
        return $this->hasMany(StockField::class);
    }

    public function place()
    {
        return $this->belongsTo(Place::class);
    }

    public function owner()
    {
        return $this->belongsTo(Owner::class);
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

}

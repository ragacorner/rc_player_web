<?php

namespace App;

use App\Models\Place;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    // use SoftDeletes;

    // protected $dates = ['deleted_at'];
    protected $fillable = ['id', 'name'];

    public function places()
    {
        return $this->hasMany(Place::class);
    }
}

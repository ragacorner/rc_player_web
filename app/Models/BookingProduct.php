<?php

namespace App\Models;

use App\Models\User;
use App\Models\Booking;
use App\Models\PlaceProduct;
use Illuminate\Database\Eloquent\Model;

class BookingProduct extends Model
{
    protected $table = 'booking_products';
    protected $fillable = ['booking_id','product_id','qty','amount','paid'];

    public function player()
    {
        return $this->belongsTo(User::class);
    }

    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }

    public function product()
    {
        return $this->belongsTo(PlaceProduct::class);
    }
}

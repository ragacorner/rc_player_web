<?php

namespace App\Models;

use App\Models\Regency;
use App\Models\Province;
use App\Models\Place;
use Illuminate\Database\Eloquent\Model;

class Subdistrict extends Model
{
    protected $table = "subdistricts";
    protected $fillable = ['id','province_id','regency_id','name','display_name'];
    public $timestamps = false;

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function regency()
    {
        return $this->belongsTo(Regency::class);
    }

    public function places()
    {
        return $this->hasMany(Place::class);
    }
}

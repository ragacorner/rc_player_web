<?php

namespace App\Models;

use App\Models\VisitorMessage;
use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    protected $table = "visitors";
    protected $fillable = ['ip','message','last_visited','hits'];

    public function visitor_messages()
    {
        return $this->belongsTo(VisitorMessage::class);
    }
}

<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\Bank;
use App\Player;
// use App\Models\Owner;
use App\Models\Field;
use App\Models\Account;
use App\Models\Booking;
use App\Models\Regency;
use App\Models\Category;
use App\Models\Province;
use App\Models\StockField;
use App\Models\Subdistrict;
use App\Models\PlaceFacility;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Place extends Model
{
    use SoftDeletes;

    const VERIFIED_PLACE = '1';
    const UNVERIFIED_PLACE = '0';
    
    const OPEN = 'open';
    const CLOSE = 'close';
    
    protected $table = 'places';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'slug','name','photo', 'contact','open','close','amount_field',
        'address','lat','lng','owner_id','subdistrict_id','regency_id',
        'province_id','category_id','verified','is_open'
    ];

    public function setNameAttribute($name)
    {
        $this->attributes['name'] = strtolower($name);
    }
    
    public function getNameAttribute($value)
    {
        return ucwords($value);
    }

    public function setSlugAttribute($slug)
    {
        $string = str_replace(' ', '-', $slug);
        $this->attributes['slug'] = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $string));
    }

    public function setOpenAttribute($open)
    {
        $this->attributes['open'] = Carbon::createFromFormat('H:i:s', $open)->format('H:i');
    }

    public function setCloseAttribute($close)
    {
        $this->attributes['close'] = Carbon::createFromFormat('H:i:s', $close)->format('H:i');
    }

    public function setAddressAttribute($address)
    {
        $this->attributes['address'] = strtolower($address);
    }

    public function getAddressAttribute($address)
    {
        return ucwords($address);
    }

    //====================================================================================

    public function isOpen()
    {
        return $this->status == Place::OPEN;
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function fields()
    {
        return $this->hasMany(Field::class);
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    public function banks()
    {
        return $this->belongsToMany(Bank::class);
    }

    public function accounts()
    {
        return $this->hasMany(Account::class);
    }

    public function subdistrict()
    {
        return $this->belongsTo(Subdistrict::class);
    }

    public function regency()
    {
        return $this->belongsTo(Regency::class);
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function stock_fields()
    {
        return $this->hasMany(StockField::class);
    }

    public function facilities()
    {
        return $this->hasMany(PlaceFacility::class);
    }
}

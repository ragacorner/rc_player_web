<?php

namespace App\Models;

use App\Models\Place;
use App\Models\Profile;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['id','name','display_name','avatar'];

    public function places()
    {
        return $this->hasMany(Place::class);
    }

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }
}

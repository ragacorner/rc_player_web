$(document).ready(function() {
    
    $('select[name="regency_id"]').on('change', function(){
        var regency_id = $(this).val();
        if(regency_id) {
            $.ajax({
                url: '/get/regencies/'+regency_id,
                type:"GET",
                dataType:"json",
                beforeSend: function(){
                    $('#loader').css("visibility", "visible");
                },

                success:function(data) {

                    $('select[name="subdistrict_id"]').empty();
                    // $('select[name="subdistrict_id"]').append('<option value="0">SEMUA TEMPAT</option>');
                    $.each(data, function(key, value){

                        $('select[name="subdistrict_id"]').append('<option value="'+ key +'">' + value + '</option>');

                    });
                },
                complete: function(){
                    $('#loader').css("visibility", "hidden");
                }
            });
        } else {
            $('select[name="subdistrict_id"]').empty();
        }

    });

    // $('select[name="regency_id"]').on('change', function(){
    //     var regency_id = $(this).val();
    //     if(regency_id) {
    //         $.ajax({
    //             url: 'http://dapur.ragacorner.dvlp/api/categories',
    //             type:"GET",
    //             dataType:"json",
    //             beforeSend: function(){
    //                 $('#loader').css("visibility", "visible");
    //             },
        
    //             success:function(data) {
        
    //                 // $('select[name="category_id"]').empty();
    //                 $('select[name="category_id"]').append('<option value="0">SEMUA TEMPAT</option>');
    //                 $.each(data, function(key, value){
        
    //                     $('select[name="category_id"]').append('<option value="'+ key +'">' + value + '</option>');
        
    //                 });
    //             },
    //             complete: function(){
    //                 $('#loader').css("visibility", "hidden");
    //             }
    //         });
    //     } else {
    //         $('select[name="place_id"]').empty();
    //     }

    // });

});